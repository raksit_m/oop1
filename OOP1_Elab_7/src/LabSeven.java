//elab-source: LabSeven.java
import java.util.Arrays;
import java.util.Scanner;
public class LabSeven {
	public static void main (String [] args) {
		Scanner scanner = new Scanner (System.in);
		int size = scanner.nextInt();
		if(size >= 1 && size <= 10) {
			int [][] mountain = new int [size][size];
			for(int i = 0 ; i < size ; i++) {
				for(int j = 0 ; j < size ; j++) {
					mountain [i][j] = scanner.nextInt();
				}
			}
			int m = 0;
			int n = 0;
			for(int i = size-1 ; i >= 0 ; i--) {
				for(int j = size-1 ; j >= 0 ; j--) {
					if(mountain [i][j] == 1) {
						m = i;
						n = j;
					}
				}
			}
			int oldlvl = 0;
			int newlvl = 1;
			while(oldlvl != newlvl && newlvl <= 99 ) {
				oldlvl++;
				if(n < size-1) {
					if(mountain[m][n+1] == newlvl+1) {
						newlvl++;
						n++;
					}
				}
				if(n>0) {
					if(mountain[m][n-1] == newlvl+1) {
						newlvl++;
						n--;
					}
				}
				if(m < size-1) {
					if(mountain[m+1][n] == newlvl+1) {
						newlvl++;
						m++;
					}
				}
				if(m>0) {
					if(mountain[m-1][n] == newlvl+1) {
						newlvl++;
						m--;
					}
				}
			}
			System.out.print(newlvl);
		}
	}
}
