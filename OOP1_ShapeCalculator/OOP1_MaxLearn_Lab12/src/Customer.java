import java.util.ArrayList;
public class Customer extends Person {
	private String customerID;
	private int memberClass;
	private double discountRate;
	ArrayList <Ticket> ticketList;
	ArrayList <Bill> billList;
	static private int numberOfCustomers = 1;
	
	public Customer (String ID,String name,String lastname,String gender,int memberClass) {
		super(ID,name,lastname,gender);
		this.memberClass = memberClass;
		ticketList = new ArrayList <Ticket> ();
		billList = new ArrayList <Bill> ();
		this.customerID = "Sky" + numberOfCustomers;
		numberOfCustomers++;
	}
	
	public void addTicket (Ticket ticket) {
		ticketList.add(ticket);
	}
	public int searchTicket (FlightInfo key) {
		int index = -1; 
		for(int i = 0; i < ticketList.size(); i++) {
			if(ticketList.get(i).getFlightInfo() == key) {
				index = i;
				break;
			}
		}
		return index;
	}
	public void addBill (Bill bill) {
		billList.add(bill);
	}
	public void setDiscountRate (int memberClass) {
		if(memberClass == 0) {
			this.discountRate = 0.00;
		}
		else if(memberClass == 1) {
			this.discountRate = 0.10;
		}
		else if(memberClass == 2) {
			this.discountRate = 0.20;
		}
	}
	public void setMemberClass (int memberClass) {
		this.memberClass = memberClass;
		setDiscountRate(this.memberClass);
	}
	public boolean equals (Customer key) {
		boolean isEquals = false;
		if(this.customerID == key.customerID && super.equals(key) == true) {
			isEquals = true;
		}
		return isEquals;
	}
	public String toString () {
		return String.format("%s, %s, %d, %.2f", super.toString(), this.customerID, this.memberClass, this.discountRate);
	}
	public String getTicketListToString () {
		String list = "";
		for(int i = 0; i < ticketList.size(); i++) {
			list += (ticketList.get(i).toString()) + "\n";
		}
		return list;
	}
	public String getBillListToString () {
		String list = "";
		for(int i = 0; i < billList.size(); i++) {
			list += (billList.get(i).toString()) + "\n";
		}
		return list;
	}

	public String getCustomerID() {
		return customerID;
	}

	public int getMemberClass() {
		return memberClass;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public int getNumberOfCustomers() {
		return numberOfCustomers;
	}
	
}
