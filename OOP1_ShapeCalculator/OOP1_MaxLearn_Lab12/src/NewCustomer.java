import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class NewCustomer extends JFrame {
	private ShoppingMenu menu;
	public NewCustomer(ShoppingMenu menu) {
		this.menu = menu;
	}

	private JLabel title2,ID,name,lastname,gender,memberClass,travelClass;
	private JTextField txt1,txt2,txt3,txt4,txt5,txt6;
	private JButton button5;
	private JTextArea area1;
	private JFrame new2 = new JFrame();
	String output = "";

	public void initComponets1() {
		title2 = new JLabel("New Customer Information");
		title2.setAlignmentX(CENTER_ALIGNMENT);
		ID = new JLabel("ID:");
		name = new JLabel("First Name:");
		lastname = new JLabel("Last Name:");
		gender = new JLabel("Gender:");
		memberClass = new JLabel("Member Class:");
		travelClass = new JLabel("Travel Class:");

		txt1 = new JTextField(15);
		txt2 = new JTextField(15);
		txt3 = new JTextField(15);
		txt4 = new JTextField(15);
		txt5 = new JTextField(15);
		txt6 = new JTextField(15);

		button5 = new JButton("Enter");
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(menu.flight1.getNumBookings()[0] < menu.flight1.getNumSeats()[0] || menu.flight1.getNumBookings()[1] < menu.flight1.getNumSeats()[1] || menu.flight1.getNumBookings()[2] < menu.flight1.getNumSeats()[2]) {
					flight1();
				}
				else {
					JOptionPane.showMessageDialog(null,"Error: No Seats Available");
				}
			}
		});

		area1 = new JTextArea(20, 50);
		area1.setEditable(false);

		new2.setTitle("Booking");
		JPanel pane2 = new JPanel();
		pane2.setLayout(new BoxLayout(pane2,BoxLayout.Y_AXIS));
		pane2.add(title2);
		pane2.add(new JLabel(" "));

		JPanel body = new JPanel();
		body.setLayout(new BoxLayout(body,BoxLayout.X_AXIS));
		JPanel info = new JPanel();
		info.setLayout(new BoxLayout(info,BoxLayout.Y_AXIS));
		JPanel info1 = new JPanel();
		info1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info1.add(ID);
		info1.add(txt1);
		info.add(info1);

		JPanel info2 = new JPanel();
		info2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info2.add(name);
		info2.add(txt2);
		info.add(info2);

		JPanel info3 = new JPanel();
		info3.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info3.add(lastname);
		info3.add(txt3);
		info.add(info3);

		JPanel info4 = new JPanel();
		info4.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info4.add(gender);
		info4.add(txt4);
		info.add(info4);

		JPanel info5 = new JPanel();
		info5.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info5.add(memberClass);
		info5.add(txt5);
		info.add(info5);

		JPanel info6 = new JPanel();
		info6.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info6.add(travelClass);
		info6.add(txt6);
		info.add(info6);

		JPanel btn = new JPanel();
		btn.setLayout(new FlowLayout(FlowLayout.CENTER));
		btn.add(button5);
		info.add(btn);

		body.add(info);
		body.add(area1);
		pane2.add(body);
		new2.add(pane2);
		new2.pack();
		new2.setVisible(true);
		new2.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}
	public void setTitle(String newTitle) {
		new2.setTitle(newTitle);
	}
	public void flight1 () {
		Customer passenger = new Customer(txt1.getText(),txt2.getText(),txt3.getText(),txt4.getText(),Integer.parseInt(txt5.getText()));
		Ticket ticket = new Ticket(passenger,menu.flight1.getFlightInfo(),txt6.getText(),0);

		if(txt6.getText().equals("Y")) {
			ticket = new Ticket(passenger,menu.flight1.getFlightInfo(),txt6.getText(),menu.flight1.getTravelClassPrice()[0]);
			if(menu.flight1.getNumBookings()[0] < menu.flight1.getNumSeats()[0]) {
				menu.flight1.getNumBookings()[0]++;
				menu.flight1.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Economy Class Available");
			}
		}
		else if(txt6.getText().equals("J")) {
			ticket = new Ticket(passenger,menu.flight1.getFlightInfo(),txt6.getText(),menu.flight1.getTravelClassPrice()[1]);
			if(menu.flight1.getNumBookings()[1] < menu.flight1.getNumSeats()[1]) {
				menu.flight1.getNumBookings()[1]++;
				menu.flight1.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Business Class Available");
			}
		}
		else if(txt6.getText().equals("F")) {
			ticket = new Ticket(passenger,menu.flight1.getFlightInfo(),txt6.getText(),menu.flight1.getTravelClassPrice()[2]);
			if(menu.flight1.getNumBookings()[2] < menu.flight1.getNumSeats()[2]) {
				menu.flight1.getNumBookings()[2]++;
				menu.flight1.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in First Class Available");
			}
		}
		
		menu.calculateDiff();
		menu.updateLabel();
	}
	
	public void initComponets2() {
		title2 = new JLabel("New Customer Information");
		title2.setAlignmentX(CENTER_ALIGNMENT);
		ID = new JLabel("ID:");
		name = new JLabel("First Name:");
		lastname = new JLabel("Last Name:");
		gender = new JLabel("Gender:");
		memberClass = new JLabel("Member Class:");
		travelClass = new JLabel("Travel Class:");

		txt1 = new JTextField(15);
		txt2 = new JTextField(15);
		txt3 = new JTextField(15);
		txt4 = new JTextField(15);
		txt5 = new JTextField(15);
		txt6 = new JTextField(15);

		button5 = new JButton("Enter");
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(menu.flight2.getNumBookings()[0] < menu.flight2.getNumSeats()[0] || menu.flight2.getNumBookings()[1] < menu.flight2.getNumSeats()[1] || menu.flight2.getNumBookings()[2] < menu.flight2.getNumSeats()[2]) {
					flight2();
				}
				else {
					JOptionPane.showMessageDialog(null,"Error: No Seats Available");
				}
			}
		});

		area1 = new JTextArea(20, 50);
		area1.setEditable(false);

		new2.setTitle("Booking");
		JPanel pane2 = new JPanel();
		pane2.setLayout(new BoxLayout(pane2,BoxLayout.Y_AXIS));
		pane2.add(title2);
		pane2.add(new JLabel(" "));

		JPanel body = new JPanel();
		body.setLayout(new BoxLayout(body,BoxLayout.X_AXIS));
		JPanel info = new JPanel();
		info.setLayout(new BoxLayout(info,BoxLayout.Y_AXIS));
		JPanel info1 = new JPanel();
		info1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info1.add(ID);
		info1.add(txt1);
		info.add(info1);

		JPanel info2 = new JPanel();
		info2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info2.add(name);
		info2.add(txt2);
		info.add(info2);

		JPanel info3 = new JPanel();
		info3.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info3.add(lastname);
		info3.add(txt3);
		info.add(info3);

		JPanel info4 = new JPanel();
		info4.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info4.add(gender);
		info4.add(txt4);
		info.add(info4);

		JPanel info5 = new JPanel();
		info5.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info5.add(memberClass);
		info5.add(txt5);
		info.add(info5);

		JPanel info6 = new JPanel();
		info6.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info6.add(travelClass);
		info6.add(txt6);
		info.add(info6);

		JPanel btn = new JPanel();
		btn.setLayout(new FlowLayout(FlowLayout.CENTER));
		btn.add(button5);
		info.add(btn);

		body.add(info);
		body.add(area1);
		pane2.add(body);
		new2.add(pane2);
		new2.pack();
		new2.setVisible(true);
		new2.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}
	public void flight2() {
		Customer passenger = new Customer(txt1.getText(),txt2.getText(),txt3.getText(),txt4.getText(),Integer.parseInt(txt5.getText()));
		Ticket ticket = new Ticket(passenger,menu.flight2.getFlightInfo(),txt6.getText(),0);

		if(txt6.getText().equals("Y")) {
			ticket = new Ticket(passenger,menu.flight2.getFlightInfo(),txt6.getText(),menu.flight2.getTravelClassPrice()[0]);
			if(menu.flight2.getNumBookings()[0] < menu.flight2.getNumSeats()[0]) {
				menu.flight2.getNumBookings()[0]++;
				menu.flight2.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Economy Class Available");
			}
		}
		else if(txt6.getText().equals("J")) {
			ticket = new Ticket(passenger,menu.flight2.getFlightInfo(),txt6.getText(),menu.flight2.getTravelClassPrice()[1]);
			if(menu.flight2.getNumBookings()[1] < menu.flight2.getNumSeats()[1]) {
				menu.flight2.getNumBookings()[1]++;
				menu.flight2.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Business Class Available");
			}
		}
		else if(txt6.getText().equals("F")) {
			ticket = new Ticket(passenger,menu.flight2.getFlightInfo(),txt6.getText(),menu.flight2.getTravelClassPrice()[2]);
			if(menu.flight2.getNumBookings()[2] < menu.flight2.getNumSeats()[2]) {
				menu.flight2.getNumBookings()[2]++;
				menu.flight2.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in First Class Available");
			}
		}
		menu.calculateDiff();
		menu.updateLabel();
	}
	
	public void initComponets3() {
		title2 = new JLabel("New Customer Information");
		title2.setAlignmentX(CENTER_ALIGNMENT);
		ID = new JLabel("ID:");
		name = new JLabel("First Name:");
		lastname = new JLabel("Last Name:");
		gender = new JLabel("Gender:");
		memberClass = new JLabel("Member Class:");
		travelClass = new JLabel("Travel Class:");

		txt1 = new JTextField(15);
		txt2 = new JTextField(15);
		txt3 = new JTextField(15);
		txt4 = new JTextField(15);
		txt5 = new JTextField(15);
		txt6 = new JTextField(15);

		button5 = new JButton("Enter");
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(menu.flight3.getNumBookings()[0] < menu.flight3.getNumSeats()[0] || menu.flight3.getNumBookings()[1] < menu.flight3.getNumSeats()[1] || menu.flight3.getNumBookings()[2] < menu.flight3.getNumSeats()[2]) {
					flight3();
				}
				else {
					JOptionPane.showMessageDialog(null,"Error: No Seats Available");
				}
			}
		});

		area1 = new JTextArea(20, 50);
		area1.setEditable(false);

		new2.setTitle("Booking");
		JPanel pane2 = new JPanel();
		pane2.setLayout(new BoxLayout(pane2,BoxLayout.Y_AXIS));
		pane2.add(title2);
		pane2.add(new JLabel(" "));

		JPanel body = new JPanel();
		body.setLayout(new BoxLayout(body,BoxLayout.X_AXIS));
		JPanel info = new JPanel();
		info.setLayout(new BoxLayout(info,BoxLayout.Y_AXIS));
		JPanel info1 = new JPanel();
		info1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info1.add(ID);
		info1.add(txt1);
		info.add(info1);

		JPanel info2 = new JPanel();
		info2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info2.add(name);
		info2.add(txt2);
		info.add(info2);

		JPanel info3 = new JPanel();
		info3.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info3.add(lastname);
		info3.add(txt3);
		info.add(info3);

		JPanel info4 = new JPanel();
		info4.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info4.add(gender);
		info4.add(txt4);
		info.add(info4);

		JPanel info5 = new JPanel();
		info5.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info5.add(memberClass);
		info5.add(txt5);
		info.add(info5);

		JPanel info6 = new JPanel();
		info6.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info6.add(travelClass);
		info6.add(txt6);
		info.add(info6);

		JPanel btn = new JPanel();
		btn.setLayout(new FlowLayout(FlowLayout.CENTER));
		btn.add(button5);
		info.add(btn);

		body.add(info);
		body.add(area1);
		pane2.add(body);
		new2.add(pane2);
		new2.pack();
		new2.setVisible(true);
		new2.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}
	public void flight3 () {
		Customer passenger = new Customer(txt1.getText(),txt2.getText(),txt3.getText(),txt4.getText(),Integer.parseInt(txt5.getText()));
		Ticket ticket = new Ticket(passenger,menu.flight3.getFlightInfo(),txt6.getText(),0);

		if(txt6.getText().equals("Y")) {
			ticket = new Ticket(passenger,menu.flight3.getFlightInfo(),txt6.getText(),menu.flight3.getTravelClassPrice()[0]);
			if(menu.flight3.getNumBookings()[0] < menu.flight3.getNumSeats()[0]) {
				menu.flight3.getNumBookings()[0]++;
				menu.flight3.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Economy Class Available");
			}
		}
		else if(txt6.getText().equals("J")) {
			ticket = new Ticket(passenger,menu.flight3.getFlightInfo(),txt6.getText(),menu.flight3.getTravelClassPrice()[1]);
			if(menu.flight3.getNumBookings()[1] < menu.flight3.getNumSeats()[1]) {
				menu.flight3.getNumBookings()[1]++;
				menu.flight3.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Business Class Available");
			}
		}
		else if(txt6.getText().equals("F")) {
			ticket = new Ticket(passenger,menu.flight3.getFlightInfo(),txt6.getText(),menu.flight3.getTravelClassPrice()[2]);
			if(menu.flight3.getNumBookings()[2] < menu.flight3.getNumSeats()[2]) {
				menu.flight3.getNumBookings()[2]++;
				menu.flight3.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in First Class Available");
			}
		}
		menu.calculateDiff();
		menu.updateLabel();
	}
	
	public void initComponets4() {
		title2 = new JLabel("New Customer Information");
		title2.setAlignmentX(CENTER_ALIGNMENT);
		ID = new JLabel("ID:");
		name = new JLabel("First Name:");
		lastname = new JLabel("Last Name:");
		gender = new JLabel("Gender:");
		memberClass = new JLabel("Member Class:");
		travelClass = new JLabel("Travel Class:");

		txt1 = new JTextField(15);
		txt2 = new JTextField(15);
		txt3 = new JTextField(15);
		txt4 = new JTextField(15);
		txt5 = new JTextField(15);
		txt6 = new JTextField(15);

		button5 = new JButton("Enter");
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(menu.flight4.getNumBookings()[0] < menu.flight4.getNumSeats()[0] || menu.flight4.getNumBookings()[1] < menu.flight4.getNumSeats()[1] || menu.flight4.getNumBookings()[2] < menu.flight4.getNumSeats()[2]) {
					flight4();
				}
				else {
					JOptionPane.showMessageDialog(null,"Error: No Seats Available");
				}
			}
		});

		area1 = new JTextArea(20, 50);
		area1.setEditable(false);

		new2.setTitle("Booking");
		JPanel pane2 = new JPanel();
		pane2.setLayout(new BoxLayout(pane2,BoxLayout.Y_AXIS));
		pane2.add(title2);
		pane2.add(new JLabel(" "));

		JPanel body = new JPanel();
		body.setLayout(new BoxLayout(body,BoxLayout.X_AXIS));
		JPanel info = new JPanel();
		info.setLayout(new BoxLayout(info,BoxLayout.Y_AXIS));
		JPanel info1 = new JPanel();
		info1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info1.add(ID);
		info1.add(txt1);
		info.add(info1);

		JPanel info2 = new JPanel();
		info2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info2.add(name);
		info2.add(txt2);
		info.add(info2);

		JPanel info3 = new JPanel();
		info3.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info3.add(lastname);
		info3.add(txt3);
		info.add(info3);

		JPanel info4 = new JPanel();
		info4.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info4.add(gender);
		info4.add(txt4);
		info.add(info4);

		JPanel info5 = new JPanel();
		info5.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info5.add(memberClass);
		info5.add(txt5);
		info.add(info5);

		JPanel info6 = new JPanel();
		info6.setLayout(new FlowLayout(FlowLayout.RIGHT));
		info6.add(travelClass);
		info6.add(txt6);
		info.add(info6);

		JPanel btn = new JPanel();
		btn.setLayout(new FlowLayout(FlowLayout.CENTER));
		btn.add(button5);
		info.add(btn);

		body.add(info);
		body.add(area1);
		pane2.add(body);
		new2.add(pane2);
		new2.pack();
		new2.setVisible(true);
		new2.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}
	public void flight4 () {
		Customer passenger = new Customer(txt1.getText(),txt2.getText(),txt3.getText(),txt4.getText(),Integer.parseInt(txt5.getText()));
		Ticket ticket = new Ticket(passenger,menu.flight4.getFlightInfo(),txt6.getText(),0);

		if(txt6.getText().equals("Y")) {
			ticket = new Ticket(passenger,menu.flight4.getFlightInfo(),txt6.getText(),menu.flight4.getTravelClassPrice()[0]);
			if(menu.flight4.getNumBookings()[0] < menu.flight4.getNumSeats()[0]) {
				menu.flight4.getNumBookings()[0]++;
				menu.flight4.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Economy Class Available");
			}
		}
		else if(txt6.getText().equals("J")) {
			ticket = new Ticket(passenger,menu.flight4.getFlightInfo(),txt6.getText(),menu.flight4.getTravelClassPrice()[1]);
			if(menu.flight4.getNumBookings()[1] < menu.flight4.getNumSeats()[1]) {
				menu.flight4.getNumBookings()[1]++;
				menu.flight4.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in Business Class Available");
			}
		}
		else if(txt6.getText().equals("F")) {
			ticket = new Ticket(passenger,menu.flight4.getFlightInfo(),txt6.getText(),menu.flight4.getTravelClassPrice()[2]);
			if(menu.flight4.getNumBookings()[2] < menu.flight4.getNumSeats()[2]) {
				menu.flight4.getNumBookings()[2]++;
				menu.flight4.getPassengerList().add(passenger);
				passenger.ticketList.add(ticket);
				output += passenger.toString() +"\n" + ticket.toString() + "\n";
				area1.setText(output);
			}
			else {
				JOptionPane.showMessageDialog(null,"Error: No Seats in First Class Available");
			}
		}
		menu.calculateDiff();
		menu.updateLabel();
	}
	public void run() {
		
	}
}
