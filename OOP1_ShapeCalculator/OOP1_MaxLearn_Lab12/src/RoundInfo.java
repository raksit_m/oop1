
public class RoundInfo {
	private String info;
	private Time fromTime;
	private Time toTime;
	
	public RoundInfo () {
		this.info = "";
		fromTime = new Time (0,0,0);
		toTime = new Time (0,0,0);
	}
	public RoundInfo (String info, Time fromTime, Time toTime) {
		this.info = info;
		this.fromTime = fromTime;
		this.toTime = toTime;
	}
	public String getInfo () {
		return this.info;
	}
	public Time getFromTime () {
		return this.fromTime;
	}
	public Time getToTime () {
		return this.toTime;
	}
	public boolean equals (RoundInfo key) {
		boolean check = false;
		if(this.info.equals(key.info) && this.fromTime.getHour() == key.fromTime.getHour() && this.fromTime.getMinute() == key.fromTime.getMinute() 
				&& this.fromTime.getSecond() == key.fromTime.getSecond() && this.toTime.getHour() == key.toTime.getHour() 
				&& this.toTime.getMinute() == key.toTime.getMinute() && this.toTime.getSecond() == key.toTime.getSecond()) {
			check = true;
		}
		return check;
	}
	public String toString () {
		return info + ", " + fromTime.toString() + ", " + toTime.toString();
	}
}
