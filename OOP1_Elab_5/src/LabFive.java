//elab-source: LabFive.java
import java.util.Scanner;
public class LabFive {
	public static void main(String [] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please Enter a number : ");
		long number = scanner.nextLong();
		if(isPalindrome(number) == true) {
			System.out.printf("%d is palindrome number.",number);
		}
		else {
			System.out.printf("%d is not palindrome number.",number);
		}
	}
	public static boolean isPalindrome (long number) {
		boolean palidrome = true;
		String str = "";
		long check = 0; 
		long input = number;
		while(number != 0) {
			str += number%10;
			number/=10;
		}
		check = Long.parseLong(str);
		if(check == input) {
			palidrome = true;
		}
		else {
			palidrome = false;
		}
		return palidrome;
	}
}