import java.util.Scanner;
public class LabEight {
	private static int[][] submatrix(int[][] matrix,int x,int y,int subSize){
		int [][] submatrix = new int [subSize][subSize];
		for(int i = 0 ; i < subSize ; i++ ) {
			for(int j = 0 ; j < subSize ; j++) {
				submatrix [i][j] = matrix[x][y];
				y++;
			}
			y -= subSize;
			x++;
		}
		return submatrix;
	}
	private static void printMatrix(int [][] arr) {
		for(int i = 0 ; i < arr.length ; i++ ) {
			for(int j = 0 ; j < arr[0].length ; j++) {
				System.out.printf("%4d",arr[i][j]);
			}
			System.out.println();
		}
	}
	public static void main(String[] args){
		Scanner scanner = new Scanner (System.in);
		System.out.print("Input size of matrix : ");
		int size = scanner.nextInt();
		System.out.println("Input matrix :");
		int [][] matrix = new int [size][size];

		for(int i = 0 ; i < matrix.length ; i++) {
			for(int j = 0 ; j < matrix[0].length ; j++) {
				matrix [i][j] = scanner.nextInt();
			}
		}
		System.out.println("Matrix is :");
		printMatrix(matrix);
		System.out.print("Input x: ");
		int x = scanner.nextInt();
		System.out.print("Input y: ");
		int y = scanner.nextInt();
		System.out.print("Input size of submatrix : ");
		int subSize = scanner.nextInt();
		System.out.println("Submatrix is :");
		int[][] sub = submatrix(matrix,x,y,subSize);
		printMatrix(sub);
	}
}
