import java.util.Scanner;
import java.util.Arrays;
public class Snake {
	public static int[] getNextPos(int [] currPos, char direction) {
		int [] nextPos = currPos.clone();
		if(direction == 'u') {
			nextPos[0]--;
		}
		else if(direction == 'd') {
			nextPos[0]++;
		}
		else if(direction == 'l') {
			nextPos[1]--;
		}
		else if(direction == 'r') {
			nextPos[1]++;
		}
		return nextPos;	
	}
	public static boolean isInBoardPosition(int[][] board, int[] nextPos) {
		boolean isBoard = true;
		if(nextPos[0] < 0 || nextPos[1] < 0 || nextPos[0] >= board.length || nextPos[1] >= board.length){
			isBoard = false;
		}
		return isBoard;
	}
	public static void updateBoard(int[][] board, int [] currPos, int [] nextPos) {
		board[currPos[0]][currPos[1]] = -1;
		board[nextPos[0]][nextPos[1]] = 8;
	}
	public static int getNextPosStatus(int[][] board, int [] nextPos) {
		int status = 0;
		if(board[nextPos[0]][nextPos[1]] == 0) {
			status = 0;
		}
		else if(board[nextPos[0]][nextPos[1]] == 1) {
			status = 1;
		}
		else if(board[nextPos[0]][nextPos[1]] == 2) {
			status = -1;
		}
		else if(board[nextPos[0]][nextPos[1]] == -1){
			status = -2;
		}
		return status;
	}
	public static void printBoard(int[][] board) {
		for(int i = 0 ; i < board.length ; i++) {
			System.out.print("|");
			for (int j = 0 ; j < board[0].length ; j++) {
				System.out.printf("%2d|",board[i][j]);
			}
			System.out.println();
		}
	}
	public static void main (String []  args) {                   
		Scanner scanner = new Scanner (System.in);           
		 int[][] board = { { 0, 0, 0, 1 },                             
				 			{ 1, 0, 0, 1 },                             
				 			{ 1, 1, 1, 1 },                
				 			{ 1, 2, 2, 1 } 
				 			}; 
		int x = 1;
		int y = 1;
		int [] currPos = new int[2];
		while(1 + 1 == 2) {
			System.out.print("Enter position X of snake : ");
			x = scanner.nextInt();
			currPos [0] = x;
			System.out.print("Enter position Y of snake : ");
			y = scanner.nextInt();
			currPos [1] = y;
			if(x > board.length-1 || x < 0 || y > board[0].length-1 || y < 0) {
				System.out.println("Invalid snake position.");
				continue;
			}
			if(board[currPos[0]][currPos[1]] != 0) {
				System.out.println("Invalid snake position.");
			}
			else {
				break;
			}
		}
		board[currPos[0]][currPos[1]] = 8;
		System.out.println("Board is");
		printBoard(board);
		int reward = 0;
		String tmp = scanner.nextLine();
		for(int i = 1 ; i <= 10 ; i++) {
			System.out.printf("Quest%d: Enter direction (u/d/l/r) : ",i);
			char direction = scanner.nextLine().charAt(0);
			int [] nextPos = getNextPos(currPos,direction);
			if(isInBoardPosition(board,nextPos) == false) {
				System.out.println("Invalid direction.  Please select new direction!");
				continue;
			}
			else {
				if(getNextPosStatus(board,nextPos) == -1) {
					System.out.println("You hit the obstacle.  You DIE!!!!");
					break;
				}
				else if(getNextPosStatus(board,nextPos) == -2) {
					System.out.println("Invalid direction.  Please select new direction!");
					continue;
				} 
				else if(getNextPosStatus(board,nextPos) == 1) {
					reward++;
				} 
				updateBoard(board,currPos,nextPos);
				currPos = nextPos.clone();
				printBoard(board);
				System.out.println("Current rewards : " + reward);
			}
		}
		System.out.println("Your total rewards : " + reward);
		System.out.print("GAME ENDS!");
	}	
}
