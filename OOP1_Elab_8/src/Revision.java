// elab-source: Revision.java
import java.util.Scanner;

public class Revision {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input A = ");
        int a = scanner.nextInt();
        System.out.print("Input B = ");
        int b = scanner.nextInt();
        
        int gcd = 1;
        for(int i = a ; i >= 1 ; i--) {
        	if(a % i == 0 && b % i == 0) {
        		gcd = i;
        		break;
        	}
        }
        System.out.println("Output : " + gcd);
    }
}
