//elab-source: LabSix.java
import java.util.Scanner;
public class LabSix {
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Amount to shift : ");
		int amount = Integer.parseInt(scanner.nextLine());
		System.out.print("Shift left or right (l/r)? : ");
		String side = scanner.nextLine();

		if(side.equals("l")) {

			System.out.print("Input array size: ");
			int size = Integer.parseInt(scanner.nextLine());      
			int [] array = new int [size];

			for(int i = 0 ; i < size ; i++) {
				System.out.printf("Input #%d: ",i+1);
				array [i] = Integer.parseInt(scanner.nextLine());
			}
			int shift = amount%size; 
			System.out.print("The shifted array : ");

			for(int j = shift ; j < size ; j++) {
				System.out.print(array[j] + " ");
			}
			for(int k = 0 ; k < shift ; k++) {
				System.out.print(array[k] + " ");
			}
		}
		else if(side.equals("r")) {

			System.out.print("Input array size: ");
			int size = Integer.parseInt(scanner.nextLine());      
			int [] array = new int [size];

			for(int i = 0 ; i < size ; i++) {
				System.out.printf("Input #%d: ",i+1);
				array [i] = Integer.parseInt(scanner.nextLine());
			}
			int shift = amount%size; 
			System.out.print("The shifted array : ");

			for(int j = size-shift ; j < size ; j++) {
				System.out.print(array[j] + " ");
			}
			for(int k = 0 ; k < size-shift ; k++) {
				System.out.print(array[k] + " ");
			}
		}
	}
}