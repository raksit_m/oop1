import java.util.Scanner;
public class SelectionSort {
	public static void main(String [] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int [] array1 = new int [n];
		for(int i = 0 ; i < n ; i++) {
			array1 [i] = scanner.nextInt();
		}
		int [] array2 = array1.clone();
		for(int step = 0 ; step <= n-2 ; step++) {
			for(int j = step ; j <= n-2 ; j++) {
				for(int k = 0; k < n ; k++) {
					System.out.print(array1[k] + " ");
				}
					if(array1[step] > array1 [j+1]) {
						array1[step] = array2 [j+1];
						array1[j+1] = array2 [step];
						array2 = array1.clone();
				}
				System.out.println();
			}
		}
		for(int k = 0 ; k < n ; k++) {
			System.out.print(array1[k] + " ");
		}
	}
}

