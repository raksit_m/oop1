import java.util.Scanner;
public class TripSwitch {
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to SKE trip-planner\n");
		System.out.println("Destination: ");
		System.out.println("  1. Kuala Lumpur (10000 Baht)");
		System.out.println("  2. Hanoi (15000 Baht)");
		System.out.println("  3. Jakata (20000 Baht)");
		System.out.println("  4. Manila (25000 Baht)");
		System.out.println("  5. Singapore (30000 Baht)");
		System.out.print("What�s your destination choice? ");
		int d = scanner.nextInt();
		int price1 = 0;
		String destination = "";
		
		switch(d) {
		case 1 :
			price1 = 10000;
			destination = "Kuala Lumpur";
			break;
		
		case 2 :
			price1 = 15000;
			destination = "Hanoi";
			break;
		
		case 3 :
			price1 = 20000;
			destination = "Jakata";
			break;
		
		case 4 : 
			price1 = 25000;
			destination = "Manila";
			break;
			
		case 5 : 
			price1 = 30000;
			destination = "Singapore";
			break;
		}
		
		System.out.println();
		
		System.out.println("Special food: ");
		System.out.println("  1. Sandwich (200 Baht)");
		System.out.println("  2. Pizza (500 Baht)");
		System.out.println("  3. Steak (1000 Baht)");
		System.out.println("  4. Oyster (2000 Baht)");
		System.out.println("  5. Foie Gras (5000 Baht)");
		System.out.print("What�s your special food choice? ");
		int f = scanner.nextInt();
		int price2 = 0;
		String food = "";
		
		switch(f) {
		case 1 : 
			price2 = 200;
			food = "Sandwich";
			break;
			
		case 2 : 
			price2 = 500;
			food = "Pizza";
			break;
		
		case 3 :
			price2 = 1000;
			food = "Steak";
			break;
		
		case 4 :
			price2 = 2000;
			food = "Oyster";
			break;
		
		case 5 :
			price2 = 5000;
			food = "Foie Gras";
			break;
		
		case -1 :
			price2 = 0;
			food = "None";
			break;
		}
		
		System.out.println();
		
		System.out.println("Activity: ");
		System.out.println("  1. Massage (500 Baht)");
		System.out.println("  2. Dancing (1000 Baht)");
		System.out.println("  3. Diving (1500 Baht)");
		System.out.println("  4. Golf (2000 Baht)");
		System.out.println("  5. Concert (3000 Baht)");
		System.out.print("What�s your activity choice? ");
		int a = scanner.nextInt();
		int price3 = 0;
		String activity = "";
		
		switch(a) {
		case 1 :
			price3 = 500;
			activity = "Massage";
			break;
		
		case 2 :
			price3 = 1000;
			activity = "Dancing";
			break;
		
		case 3 :
			price3 = 1500;
			activity = "Diving";
			break;
		
		case 4 :
			price3 = 2000;
			activity = "Golf";
			break;
		
		case 5 :
			price3 = 3000;
			activity = "Concert";
			break;

		case -1 :
			price3 = 0;
			activity = "None";
			break;
		}
		
		System.out.println();
		
		switch(d) {
		case -1 :
			System.out.println("Error!  No selected destination. ");
			break;
		
		default :
		System.out.println("Your order and total price: ");
		System.out.println(destination);
		System.out.println(food);
		System.out.println(activity);
		System.out.println(price1+price2+price3 + " Baht \n");
		System.out.print("Good luck, have a nice trip :)");
		break;
		}
	}
}