import java.util.Scanner;
public class Trip {
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to SKE trip-planner\n");
		System.out.println("Destination: ");
		System.out.println("  1. Kuala Lumpur (10000 Baht)");
		System.out.println("  2. Hanoi (15000 Baht)");
		System.out.println("  3. Jakata (20000 Baht)");
		System.out.println("  4. Manila (25000 Baht)");
		System.out.println("  5. Singapore (30000 Baht)");
		System.out.print("What�s your destination choice? ");
		int d = scanner.nextInt();
		int price1 = 0;
		String destination = "";
		
		if(d==1) {
			price1 = 10000;
			destination = "Kuala Lumpur";
		}
		else if(d==2) {
			price1 = 15000;
			destination = "Hanoi";
		}
		else if(d==3) {
			price1 = 20000;
			destination = "Jakata";
		}
		else if(d==4) {
			price1 = 25000;
			destination = "Manila";
		}
		else if(d==5) {
			price1 = 30000;
			destination = "Singapore";
		}
		
		System.out.println();
		
		System.out.println("Special food: ");
		System.out.println("  1. Sandwich (200 Baht)");
		System.out.println("  2. Pizza (500 Baht)");
		System.out.println("  3. Steak (1000 Baht)");
		System.out.println("  4. Oyster (2000 Baht)");
		System.out.println("  5. Foie Gras (5000 Baht)");
		System.out.print("What�s your special food choice? ");
		int f = scanner.nextInt();
		int price2 = 0;
		String food = "";
		
		if(f==1) {
			price2 = 200;
			food = "Sandwich";
		}
		else if(f==2) {
			price2 = 500;
			food = "Pizza";
		}
		else if(f==3) {
			price2 = 1000;
			food = "Steak";
		}
		else if(f==4) {
			price2 = 2000;
			food = "Oyster";
		}
		else if(f==5) {
			price2 = 5000;
			food = "Foie Gras";
		}
		else if(f==-1) {
			price2 = 0;
			food = "None";
		}
		
		System.out.println();
		
		System.out.println("Activity: ");
		System.out.println("  1. Massage (500 Baht)");
		System.out.println("  2. Dancing (1000 Baht)");
		System.out.println("  3. Diving (1500 Baht)");
		System.out.println("  4. Golf (2000 Baht)");
		System.out.println("  5. Concert (3000 Baht)");
		System.out.print("What�s your activity choice? ");
		int a = scanner.nextInt();
		int price3 = 0;
		String activity = "";
		
		if(a==1) {
			price3 = 500;
			activity = "Massage";
		}
		else if(a==2) {
			price3 = 1000;
			activity = "Dancing";
		}
		else if(a==3) {
			price3 = 1500;
			activity = "Diving";
		}
		else if(a==4) {
			price3 = 2000;
			activity = "Golf";
		}
		else if(a==5) {
			price3 = 3000;
			activity = "Concert";
		}
		else if(a==-1) {
			price3 = 0;
			activity = "None";
		}
		
		System.out.println();
		
		if(d==-1) {
			System.out.println("Error!  No selected destination. ");
		}
		else if(d!=-1) {
		System.out.println("Your order and total price: ");
		System.out.println(destination);
		System.out.println(food);
		System.out.println(activity);
		System.out.println(price1+price2+price3 + " Baht \n");
		System.out.print("Good luck, have a nice trip :)");
		}
	}
}