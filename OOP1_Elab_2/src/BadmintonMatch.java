import java.util.Scanner;
public class BadmintonMatch {
	public static void main(String [] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter name of first player: ");
		String p1 = scanner.nextLine();
		System.out.print("Enter name of second player: ");
		String p2 = scanner.nextLine();
		int total1 = 0;
		int win1 = 0;
		int total2 = 0;
		int win2 = 0;
		int serve = 0;
		int match = 1;
			while(win1 != 2 && win2 != 2) {
				System.out.printf("Game %d:\n",match);
				while(total1<20 && total2<20) {
					if(serve == 0) {
						serve = (int)Math.round(5*Math.random());
						if(serve == 0 || serve == 2 || serve == 4) {
							total1 ++;
							serve = 0;
						}
						else if(serve == 1 || serve == 3 || serve == 5) {
							total2 ++;
							serve = 1;
						}
						System.out.printf("%s served: %d vs. %d\n",p1,total1,total2);
					}
					else if(serve == 1) {
						serve = (int)Math.round(5*Math.random());
						if(serve == 0 || serve == 2 || serve == 4) {
							total1 ++;
							serve = 0;
						}
						else if(serve == 1 || serve == 3 || serve == 5) {
							total2 ++;
							serve = 1;
						}
						System.out.printf("%s served: %d vs. %d\n",p2,total1,total2);
					}
				}

				while((total1 == 20 || total2 == 20) && (total1 != total2)) {
					if(serve == 0) {
						serve = (int)Math.round(5*Math.random());
						if(serve == 0 || serve == 2 || serve == 4) {
							total1 ++;
							serve = 0;
						}
						else if(serve == 1 || serve == 3 || serve == 5) {
							total2 ++;
							serve = 1;
						}
						System.out.printf("%s served: %d vs. %d\n",p1,total1,total2);
					}
					else if(serve == 1) {
						serve = (int)Math.round(Math.random());
						if(serve == 0 || serve == 2 || serve == 4) {
							total1 ++;
							serve = 0;
						}
						else if(serve == 1 || serve == 3 || serve == 5) {
							total2 ++;
							serve = 1;
						}
						System.out.printf("%s served: %d vs. %d\n",p2,total1,total2);
					}
				}

				if(total1 == 20 && total2 == 20) {
					while((total1-total2 != 2 && total2-total1 != 2) && (total1<29 && total2<29)) {
						if(serve == 0) {
							serve = (int)Math.round(Math.random());
							if(serve == 0) {
								total1 ++;
								serve = 0;
							}
							else if(serve == 1 || serve == 3 || serve == 5) {
								total2 ++;
								serve = 1;
							}
							System.out.printf("%s served: %d vs. %d\n",p1,total1,total2);
						}
						else if(serve == 1) {
							serve = (int)Math.round(Math.random());
							if(serve == 0 || serve == 2 || serve == 4) {
								total1 ++;
								serve = 0;
							}
							else if(serve == 1 || serve == 3 || serve == 5) {
								total2 ++;
								serve = 1;
							}
							System.out.printf("%s served: %d vs. %d\n",p2,total1,total2);
						}
					}
				}
				while((total1 == 29 || total2 == 28) || (total1 == 28 || total2 == 29)) {
					if(serve == 0) {
						serve = (int)Math.round(5*Math.random());
						if(serve == 0 || serve == 2 || serve == 4) {
							total1 ++;
							serve = 0;
						}
						else if(serve == 1 || serve == 3 || serve == 5) {
							total2 ++;
							serve = 1;
						}
						System.out.printf("%s served: %d vs. %d\n",p1,total1,total2);
					}
					else if(serve == 1) {
						serve = (int)Math.round(Math.random());
						if(serve == 0 || serve == 2 || serve == 4) {
							total1 ++;
							serve = 0;
						}
						else if(serve == 1 || serve == 3 || serve == 5) {
							total2 ++;
							serve = 1;
						}
						System.out.printf("%s served: %d vs. %d\n",p2,total1,total2);
					}
				}

				while(total1 == 29 && total2 == 29) {
					serve = (int)Math.round(Math.random());
					if(serve == 0 || serve == 2 || serve == 4) {
						total1 ++;
						serve = 0;
					}
					else if(serve == 1 || serve == 3 || serve == 5) {
						total2 ++;
						serve = 1;
					}
				}
				if(total1>total2) {
					System.out.printf("%s wins.\n",p1);
					win1++;
					serve = 0;
				}
				else if(total2>total1) {
					System.out.printf("%s wins.\n",p2);
					win2++;
					serve = 1;
				}
				match++;
				total1 = 0;
				total2 = 0;
			}
		if(win1>win2) {
			System.out.printf("Match winner = %s",p1);
		}
		else if(win2>win1) {
			System.out.printf("Match winner = %s",p2);
		}
	}
}