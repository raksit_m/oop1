// elab-source: LabThree.java
import java.util.Scanner;

public class LabThree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = Integer.parseInt(scanner.nextLine());
        String s1 = "";
        
        if(num%2==0) {
        	s1 += "" + "0";
        }
        else if(num%2!=0) {
        	s1 += "" + "1";
        }
        while(num/2!=0) {
        	num/=2;
        	s1 += num%2;      	
        }
        int before = Integer.parseInt(s1);
        String s2 = "";
        while(before!=0) {
        	s2 += before%10;
        	before/=10;
        }
        System.out.println(s2);
    }
}