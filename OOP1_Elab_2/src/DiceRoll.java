import java.util.Scanner;
public class DiceRoll {
	public static void main(String []args) {
		double new1 = 0;
		double new2 = 0;
		int total1 = 0;
		int total2 = 0;
		int next = 1;
		
		while(total1 < 21 && total2 < 21) {
			if(next == 1) {
				new1 = Math.ceil(6*(Math.random()));
				total1 += new1;
				System.out.println("Player1's turn: ");
				System.out.printf("New = %d, Total = %d\n",(int)(new1),total1);
				next = 2;
			}
			else if(next == 2) {
				new2 = Math.ceil(6*(Math.random()));
				total2 += new2;
				System.out.println("Player2's turn: ");
				System.out.printf("New = %d, Total = %d\n",(int)(new2),total2);
				next = 1;		
			}
		}
		if(total1 > total2) {
			System.out.println("Player1 wins");
		}
		else if(total1 < total2) {
			System.out.println("Player2 wins");
		}
	}
}
