import java.util.Scanner;
public class LabTwo {
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to BET trip-planner\n");
		System.out.println("Destination: ");
		System.out.println("  1. Bangkok (10000 Baht)");
		System.out.println("  2. Havana (20000 Baht)");
		System.out.println("  3. Moscow (30000 Baht)");
		System.out.println("  4. Berlin (40000 Baht)");
		System.out.println("  5. London (50000 Baht)");
		System.out.print("What�s your destination choice? ");
		int d = scanner.nextInt();
		int price1 = 0;
		String destination = "";
		
		if(d==1) {
			price1 = 10000;
			destination = "Bangkok";
		}
		else if(d==2) {
			price1 = 20000;
			destination = "Havana";
		}
		else if(d==3) {
			price1 = 30000;
			destination = "Moscow";
		}
		else if(d==4) {
			price1 = 40000;
			destination = "Berlin";
		}
		else if(d==5) {
			price1 = 50000;
			destination = "London";
		}
		
		System.out.println();
		
		System.out.println("Special drinks: ");
		System.out.println("  1. Mojito (700 Baht)");
		System.out.println("  2. Gin (800 Baht)");
		System.out.println("  3. Schnapps (900 Baht)");
		System.out.println("  4. Mekhong (1000 Baht)");
		System.out.println("  5. Vodka (2000 Baht)");
		System.out.print("What�s your special drinks choice? ");
		int f = scanner.nextInt();
		int price2 = 0;
		String food = "";
		
		if(f==1) {
			price2 = 700;
			food = "Mojito";
		}
		else if(f==2) {
			price2 = 800;
			food = "Gin";
		}
		else if(f==3) {
			price2 = 900;
			food = "Schnapps";
		}
		else if(f==4) {
			price2 = 1000;
			food = "Mekhong";
		}
		else if(f==5) {
			price2 = 2000;
			food = "Vodka";
		}
		else if(f==-1) {
			price2 = 0;
		}
		
		System.out.println();
		
		System.out.println("Gambling: ");
		System.out.println("  1. Slave (5000 Baht)");
		System.out.println("  2. Bingo (10000 Baht)");
		System.out.println("  3. Slot Machines (15000 Baht)");
		System.out.println("  4. Poker (20000 Baht)");
		System.out.println("  5. Roulette (30000 Baht)");
		System.out.print("What�s your special gambling choice? ");
		int a = scanner.nextInt();
		int price3 = 0;
		String activity = "";
		
		if(a==1) {
			price3 = 5000;
			activity = "Slave";
		}
		else if(a==2) {
			price3 = 10000;
			activity = "Bingo";
		}
		else if(a==3) {
			price3 = 15000;
			activity = "Slot Machines";
		}
		else if(a==4) {
			price3 = 20000;
			activity = "Poker";
		}
		else if(a==5) {
			price3 = 30000;
			activity = "Roulette";
		}
		else if(a==-1) {
			price3 = 0;
		}
		
		System.out.println();
		
		if(d==-1) {
			System.out.println("Error!  No selected destination. ");
		}
		else if(d!=-1) {
		System.out.println("Your order and total price: ");
		System.out.println(destination);
		System.out.println(food);
		System.out.println(activity);
		System.out.println(price1+price2+price3 + " Baht \n");
		System.out.print("Good luck, have a nice BET :)");
		}
	}
}