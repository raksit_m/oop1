//elab-source: Student.java
public class Student extends Person {
	private double GPA;
	private int classYear;
	

public Student(String id,String name,String lastname,String gender,double gpa,int classYear) {
	super(id,name,lastname,gender);
	this.GPA = gpa;
	this.classYear = classYear;
}
	public void setGPA(double gPA) {
		GPA = gPA;
	}
	public void setClassYear(int classYear) {
		this.classYear = classYear;
	}
	public double getGPA() {
		return GPA;
	}
	public int getClassYear() {
		return classYear;
	}
	public String toString () {
		return String.format("%s,%s %s,%s,%d,%.2f",this.getID(),this.getName(),this.getLastname(),this.getGender(),this.getClassYear(),this.getGPA());
	}
	
}
