//elab-source: Queue.java
import java.util.ArrayList;
public class Queue {
	ArrayList <Integer> queue;
	
	public Queue () {
		queue = new ArrayList <Integer> ();
	}
	public void enqueue(int x) {
		queue.add(x);
	}
	public int dequeue() {
		int dequeue = 0;
		if(this.isEmpty() == true) {
			dequeue = -1;
		}
		else {
			dequeue = queue.get(0);
			queue.remove(0);
		}
		return dequeue;
	}
	public int peek() {
		int peek = 0;
		if(this.isEmpty() == true) {
			peek = -1;
		}
		else {
			peek = queue.get(0);
		}
		return peek;
	}
	public int size () {
		return queue.size();
	}
	public boolean isEmpty() {
		boolean isEmpty = false;
		if(queue.size() == 0) {
			isEmpty = true;
		}
		return isEmpty;
	}
}
