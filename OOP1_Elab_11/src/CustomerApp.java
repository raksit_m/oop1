public class CustomerApp {  
	public static void main (String[] args) {  
		Time departure1 = new Time (12,0,0);   
		Time arrival1 = new Time (13,0,0);   
		FlightInfo sk101FlightInfo = new FlightInfo ("SK101", departure1, arrival1, "Bangkok", "Chiangmai"); 
		Time departure2 = new Time (14,0,0);  
		Time arrival2 = new Time (15,0,0);   
		FlightInfo sk102FlightInfo = new FlightInfo ("SK102", departure2, arrival2, "Bangkok", "Phuket"); 
		Customer adam = new Customer ("1001", "Adam", "Smith", "M", 0);   
		Customer beth = new Customer ("1002", "Beth", "Woods", "F", 1);      
		System.out.println("Ticket: Adam");
		Ticket ticketAdam1 = new Ticket(adam, sk101FlightInfo, "Y", 100); 
		Ticket ticketAdam2 = new Ticket(adam, sk102FlightInfo, "Y", 90);
		System.out.println(ticketAdam1.toString());   
		System.out.println(ticketAdam2.toString());   
		System.out.println(ticketAdam1.equals(ticketAdam1)); 
		System.out.println(ticketAdam1.equals(ticketAdam2));  
		System.out.println("Ticket: Beth");   
		Ticket ticketBeth1 = new Ticket(beth, sk101FlightInfo, "J", 200); 
		System.out.println(ticketBeth1.toString());  
		ticketBeth1.applyDiscount();   
		System.out.println(ticketBeth1.toString());  
		ticketBeth1.setPaymentStatus("Paid");   
		System.out.println(ticketBeth1.toString());        
		System.out.println("Customer: Adam");  
		System.out.println(adam.toString());  
		adam.addTicket(ticketAdam1);   
		adam.addTicket(ticketAdam2);  
		System.out.println(adam.getTicketListToString());  
		System.out.println(adam.searchTicket(sk101FlightInfo));
		System.out.println(adam.searchTicket(sk102FlightInfo));
		System.out.println(adam.equals(adam));  
		System.out.println(adam.equals(beth));   
		adam.setMemberClass(2);   
		System.out.println(adam.toString());    
		System.out.println("Bill: Adam");   
		Bill billAdam1 = new Bill (adam, ticketAdam1);   
		Bill billAdam2 = new Bill (adam, ticketAdam2);   
		adam.addBill(billAdam1);   
		adam.addBill(billAdam2);  
		System.out.println(adam.getBillListToString());    
		} 
	} 