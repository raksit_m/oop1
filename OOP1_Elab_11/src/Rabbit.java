//elab-source: Rabbit.java
import java.util.ArrayList;
public class Rabbit {
	private double x;
	private double y;
	private double speed = 2;
	private double degree = 90;
	ArrayList <Integer> food = new ArrayList <Integer> ();
	
	public Rabbit () {
		this.x = 0;
		this.y = 0;
	}
	public Rabbit(double x , double y ) {
		this.x = x;
		this.y = y;
	}
	public void walk() {
		this.x += Math.cos(degree*Math.PI/180)*speed;
		this.y += Math.sin(degree*Math.PI/180)*speed;
	}
	public void run() {
		this.x += Math.cos(degree*Math.PI/180)*speed*2;
		this.y += Math.sin(degree*Math.PI/180)*speed*2;
	}
	public String getPosition() {
		return String.format("The rabbit is at x = %.3f, y = %.3f, direction = %.3f",this.x,this.y,this.degree);
	}
	public void turnLeft() {
		this.degree += 90;
	}
	public void turnRight() {
		this.degree -= 90;
	}
	public void eat(int nutrient) {
		this.food.add(nutrient);
	}
	
	public String getFoodInformation() {
		int sum = 0;
		String list = "";
		for(int i = 0 ; i < food.size() ; i++) {
			sum += food.get(i);
			list += food.get(i) + " ";
		}
		return String.format("Eaten food: %s, Sum of nutrients: %d",list,sum);
	}
	public ArrayList<Integer> getEaten_food() {
		return food;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getDegree() {
		return degree;
	}
	public void setDegree(double degree) {
		this.degree = degree;
	}
}
