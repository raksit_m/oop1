//elab-source: Purse.java
import java.util.ArrayList;
public class Purse {
	ArrayList <Coin> purse;
	public Purse () {
		purse = new ArrayList <Coin>();
	}
	public void add(Coin coin) {
		purse.add(coin);
	}
	public int getTotal() {
		int sum = 0;
		for(int i = 0; i < purse.size(); i++) {
			int value = purse.get(i).getValue();
			sum += value;
		}
		return sum;
	}
	public void clear () {
		purse = new ArrayList <Coin>();
	}
} 
