import java.util.ArrayList;
public class Flight {
	private int [] numSeats;
	private FlightInfo flightInfo;
	private double flightExpense;
	private ArrayList <Customer> passengerList;
	private int [] numBookings;
	private double [] travelClassPrice;

	public Flight (int [] numSeats, FlightInfo flightInfo, double flightExpense, double [] travelClassPrice) {
		this.passengerList = new ArrayList <Customer> ();
		numBookings = new int [3];
		this.numSeats = numSeats;
		this.flightInfo = flightInfo;
		this.flightExpense = flightExpense;
		this.travelClassPrice = travelClassPrice;
	}
	public int booking (Customer passenger, String travelClass) {
		int booking = -1;
		if(travelClass.equals("Y") && numBookings[0] < numSeats[0]) {
			passengerList.add(passenger);
			numBookings[0]++;
			passenger.setDiscountRate(passenger.getMemberClass());
			passenger.addTicket(new Ticket(passenger,flightInfo,travelClass,travelClassPrice[0]));
			booking = 1;
		}
		else if(travelClass.equals("J") && numBookings[1] < numSeats[1]) {
			passengerList.add(passenger);
			numBookings[1]++;
			passenger.setDiscountRate(passenger.getMemberClass());
			passenger.addTicket(new Ticket(passenger,flightInfo,travelClass,travelClassPrice[1]));
			booking = 1;
		}
		else if(travelClass.equals("F") && numBookings[2] < numSeats[2]) {
			passengerList.add(passenger);
			numBookings[2]++;
			passenger.setDiscountRate(passenger.getMemberClass());
			passenger.addTicket(new Ticket(passenger,flightInfo,travelClass,travelClassPrice[2]));
			booking = 1;
		}
		return booking;
	}
	public int searchPassenger (Customer passenger) {
		int index = -1;
		for(int i = 0; i < passengerList.size(); i++) {
			if(passengerList.get(i) == passenger) {
				index = i;
				break;
			}
		}
		return index;
	}
	public int cancel (Customer passenger) {
		int cancel = -1;
		int num = -1;
		for(int i = 0; i < passengerList.size(); i++) {
			if(passengerList.get(i) == passenger) {
				cancel = 1;
				passengerList.remove(i);
				num = i;
				break;
			}
		}
		if(cancel == 1 && passenger.ticketList.size() != 0) {
			for(int i = 0; i < passenger.ticketList.size(); i++) {
				int index = passengerList.get(i).searchTicket(this.flightInfo);
				if(index != -1) {
					cancel = 1;
					if(passengerList.get(i).ticketList.get(index).getTravelClass().equals("Y")) {
						numBookings[0]--;
					}
					else if(passengerList.get(i).ticketList.get(index).getTravelClass().equals("J")) {
						numBookings[1]--;
					}
					else if(passengerList.get(i).ticketList.get(index).getTravelClass().equals("F")) {
						numBookings[2]--;
					}
					passengerList.get(i).ticketList.remove(index);		
					break;
				}
				else {
					cancel = -1;
					passengerList.add(num,passenger);
				}
			}
		}
		else {			
			cancel = -1;
		}
		return cancel;
	}
	public void clear () {
		for(int i = 0; i < passengerList.size(); i++) {
			for(int j = 0; j < passengerList.get(i).ticketList.size(); j++) {
				int index = passengerList.get(i).searchTicket(this.flightInfo);
				if(index != -1) {
					passengerList.get(i).ticketList.remove(index);
				}
			}
		}
		for(int i = 0; i < numBookings.length; i++) {
			numBookings[i] = 0;
		}
		passengerList.clear();
	}
	public String toString () {
		String temp = "";
		temp += flightInfo.toString() + "\n";
		if (numBookings[0] > 0 || numBookings[1] > 0 ||numBookings[2] > 0) {  
			for ( int i = 0; i < passengerList.size(); i++ ) {    
				temp += String.format("%d,  %s\n", i+1, passengerList.get(i).toString());
			}    
		} 
		else {  
			temp += "No booking\n"; 
		}    
		return temp; 
	}
	public int[] getNumSeats() {
		return numSeats;
	}
	public FlightInfo getFlightInfo() {
		return flightInfo;
	}
	public double getFlightExpense() {
		return flightExpense;
	}
	public ArrayList<Customer> getPassengerList() {
		return passengerList;
	}
	public int[] getNumBookings() {
		return numBookings;
	}
	public double[] getTravelClassPrice() {
		return travelClassPrice;
	}

}
