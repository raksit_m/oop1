//elab-source: Fraction.java

public class Fraction {
	protected int numerator;
	protected int denominator;
	static Fraction ONE = new Fraction(1,1);
	static Fraction ZERO = new Fraction(0,1);
	public Fraction(int numerator, int denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}
	public double evaluate() {
		return (this.numerator*1.0 / this.denominator);
	}
	public Fraction add(int n) {
		int sum1 = (this.numerator)+(this.denominator*n);
		int sum2 = (this.denominator);
		Fraction sum = new Fraction(sum1,sum2);
		return sum;
	}
	public Fraction add(Fraction other) {
		int sum1 = (this.numerator*other.denominator)+(this.denominator*other.numerator);
		int sum2 = (this.denominator*other.denominator);
		Fraction sum = new Fraction(sum1,sum2);
		return sum;
	}
	public Fraction multiply(int n) {
		int sum1 = (this.numerator*n);
		int sum2 = this.denominator;
		Fraction sum = new Fraction(sum1,sum2);
		return sum;
	}
	public Fraction multiply(Fraction other) {
		int sum1 = (this.numerator*other.numerator);
		int sum2 = (this.denominator*other.denominator);
		Fraction sum = new Fraction(sum1,sum2);
		return sum;
	}
	public String toString() {
		return this.numerator + "/" + this.denominator;
	}
}
