import java.util.ArrayList;
public class Course {
	private String courseID;
	private String courseName;
	private Teacher teacher;
	ArrayList <Student> student = new ArrayList <Student> ();
	
	public Course(String courseID,String courseName, String teacherID, String name, String lastname, String gender) {
		this.courseID = courseID;
		this.courseName = courseName;
		teacher = new Teacher (teacherID,name,lastname,gender);
		
	}
	public void register (String ID,String name,String lastname,String gender,double gpa,int classYear) {
		student.add(new Student(ID,name,lastname,gender,gpa,classYear));
	}
	public void withdraw (int studentIndex) {
		student.remove(studentIndex);
	}
	public double getAveGPA() {
		double sum = 0;
		for(int i = 0 ; i < student.size() ; i++) {
			sum += student.get(i).getGPA();
		}
		return sum/student.size();
	}
	public String getCourseID() {
		return this.courseID;
	}
	public String getCourseName() {
		return this.courseName;
	}
	public Teacher getTeacher() {
		return this.teacher;
	}
	public int findingStudentIndex(String keyID) {
		int index = -1;
		for(int i = 0 ; i < student.size() ; i++) {
			if(student.get(i).getID().equals(keyID)) {
				index = i;
				break;
			}
		}
		return index;
	}
	public void printAllStudent() {
		for(int i = 0 ; i < student.size() ; i++) {
			System.out.println(student.get(i).toString());
		}
	}
	public int getTotalStudentNumber() {
		return student.size();
	}
}
