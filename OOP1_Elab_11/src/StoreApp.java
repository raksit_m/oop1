
public class StoreApp {

	public static void main (String[] args) {

		Customer adam = new Customer ("1001", "Adam", "Smith", "M", 0);
		Customer beth = new Customer ("1002", "Beth", "Woods", "F", 1);
		Customer cara = new Customer ("1003", "Cara", "Johnson", "F", 2);
		Customer david = new Customer ("1004", "David", "Brown", "M", 0);
		Customer eric = new Customer ("1004", "Eric", "Martin", "M", 0);
		
		Time departure1 = new Time (12,0,0);
		Time arrival1 = new Time (13,0,0);
		FlightInfo ske101FlightInfo = new FlightInfo ("SK101", departure1, arrival1, "Bangkok", "Chiangmai");
		
		Time departure2 = new Time (14,0,0);
		Time arrival2 = new Time (15,0,0);
		FlightInfo ske102FlightInfo = new FlightInfo ("SK102", departure2, arrival2, "Bangkok", "Phuket");
		
		Flight sk101 = new Flight (new int[] {2,2,1}, ske101FlightInfo, 500, new double []{100, 200, 300});
		
		Store agency = new Store ();

		System.out.println("--------------------------");
		System.out.println("Agency: SK101 Booking");
		if (agency.booking(sk101, adam, "Y") == -1)   
			System.out.println("Adam: Unsuccessful booking");
		if (agency.booking(sk101, beth, "J") == -1)   
			System.out.println("Beth: Unsuccessful booking");
		if (agency.booking(sk101, cara, "F") == -1)   
			System.out.println("Cara: Unsuccessful booking");
		System.out.println("SK101: Flight");
		System.out.print(sk101.toString());
		System.out.println();
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());
		System.out.println("Beth: Customer");
		System.out.println(beth.toString());
		System.out.println(beth.getTicketListToString());

		System.out.println("Agency: Information");
		System.out.println(agency.toString());
		
		System.out.println("\n--------------------------");
		System.out.println("Agency: SK101 Receive Payment\n");
		
		if (agency.receivePayment(sk101, adam) == -1)   
			System.out.println("Adam: Ticket is not found");
		if (agency.receivePayment(sk101, beth) == -1)   
			System.out.println("Beth: Ticket is not found");
		if (agency.receivePayment(sk101, david) == -1)   
			System.out.println("David: Ticket is not found");
		
		System.out.println();		
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());
		System.out.println("Beth: Customer");
		System.out.println(beth.toString());
		System.out.println(beth.getTicketListToString());
		
		System.out.println(agency.toString());
		System.out.println();		

		System.out.println("--------------------------");
		System.out.println("Agency: SK102 Booking");
		System.out.println();		
		Flight sk102 = new Flight (new int[] {2,1,0}, ske102FlightInfo, 250, new double []{90, 150, 0});
		if (agency.booking(sk102, adam, "Y") == -1)   
			System.out.println("Adam: Unsuccessful booking");
		if (agency.booking(sk102, cara, "J") == -1)   
			System.out.println("Cara: Unsuccessful booking");
		if (agency.booking(sk102, david, "J") == -1)   
			System.out.println("David: Unsuccessful booking");
		if (agency.booking(sk102, eric, "Y") == -1)   
			System.out.println("Eric: Unsuccessful booking");
		
		System.out.println("\nSK102: Flight");
		System.out.print(sk102.toString());
		System.out.println();
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());
		System.out.println("Cara: Customer");
		System.out.println(cara.toString());
		System.out.println(cara.getTicketListToString());

		System.out.println("--------------------------");
		System.out.println("Agency: SK101 Canceling");
		System.out.println();		
		if (agency.cancel(sk101, beth) == -1)   
			System.out.println("Beth: Unsuccessful canceling");
		if (agency.cancel(sk101, cara) == -1)   
			System.out.println("Cara: Unsuccessful canceling");
		if (agency.cancel(sk101, david) == -1)   
			System.out.println("David: Unsuccessful canceling");
		
		System.out.println();		
		System.out.println("SK101: Flight");
		System.out.print(sk101.toString());
		System.out.println();
		System.out.println("Beth: Customer");
		System.out.println(beth.toString());
		System.out.println(beth.getTicketListToString());
		System.out.println("Cara: Customer");
		System.out.println(cara.toString());
		System.out.println(cara.getTicketListToString());
	
		System.out.println("--------------------------");
		System.out.println("Agency: SK101 Clear");
		System.out.println();
		agency.clearFlight(sk101);
		System.out.println(agency.toString());
		System.out.println("\nSK101: Flight");
		System.out.print(sk101.toString());
		System.out.println();
		
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());

		System.out.println("--------------------------");
		System.out.println("Agency: SK102 Receive Payment\n");
		
		if (agency.receivePayment(sk102, adam) == -1)   
			System.out.println("Adam: Ticket is not found");
		if (agency.receivePayment(sk102, cara) == -1)   
			System.out.println("Cara: Ticket is not found");
		if (agency.receivePayment(sk102, eric) == -1)   
			System.out.println("Eric: Ticket is not found");
		
		System.out.println(agency.toString());
		System.out.println();		
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());
		System.out.println("Cara: Customer");
		System.out.println(cara.toString());
		System.out.println(cara.getTicketListToString());
		System.out.println("Eric: Customer");
		System.out.println(eric.toString());
		System.out.println(eric.getTicketListToString());
		
		System.out.println("--------------------------");
		System.out.println("Agency: SK102 Clear");
		System.out.println();
		agency.clearFlight(sk102);
		System.out.println(agency.toString());
		System.out.println("\nSK102: Flight");
		System.out.print(sk102.toString());
		System.out.println();
		
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());
		System.out.println(adam.getBillListToString());
		System.out.println("Cara: Customer");
		System.out.println(cara.toString());
		System.out.println(cara.getTicketListToString());
		System.out.println(cara.getBillListToString());
		System.out.println("Eric: Customer");
		System.out.println(eric.toString());
		System.out.println(eric.getTicketListToString());
		System.out.println(eric.getBillListToString());
		
		System.out.println("--------------------------");

		
	}
}
