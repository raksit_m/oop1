
public class CourseApp {
	public static void main (String [] args) {
		Course course = new Course("1","Test","11","AA","BB","M");
		
		course.register("1","A","BBBB","M",4.0,1);
		course.register("2","B","BBBB","F",3.8,1);
		course.register("3","C","BBBB","M",2.5,1);
		course.register("4","D","BBBB","F",3.6,1);
		course.register("5","E","BBBB","F",3.5,1);
		course.register("6","F","BBBB","M",2.7,1);
		
		System.out.println(course.getAveGPA());
		System.out.println();
		course.printAllStudent();
		System.out.println();
		System.out.println(course.findingStudentIndex("1"));
		System.out.println();
		course.withdraw(0);
		
		course.printAllStudent();
		System.out.println();
		course.withdraw(course.findingStudentIndex("3"));
		
		course.printAllStudent();
		System.out.println();
		System.out.println(course.getCourseID()+"");
		System.out.println();
		System.out.println(course.getCourseName());
		System.out.println();
		course.withdraw(course.findingStudentIndex("3"));
		
		System.out.println(course.getAveGPA());
		System.out.println();
		course.printAllStudent();
		System.out.println();
		course.register("7","G","BBBB","M",3.5,1);
		
		System.out.println(course.getAveGPA());
		System.out.println();
		course.printAllStudent();
		System.out.println();
		course.withdraw(0);
		
		course.withdraw(0);
		
		course.withdraw(0);
		
		System.out.println(course.getAveGPA());
		System.out.println();
		course.printAllStudent();
		System.out.println();
		Teacher T = course.getTeacher();
		
		System.out.println(course.getTeacher().toString());
	}
}
