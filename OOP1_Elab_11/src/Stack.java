//elab-source: Stack.java
import java.util.ArrayList;
public class Stack {
	ArrayList <Integer> stack;
	
	public Stack () {
		stack = new ArrayList <Integer> ();
	}
	public void push(int x) {
		stack.add(0,x);
	}
	public int pop() {
		int pop = 0;
		if(this.isEmpty() == true) {
			pop = -1;
		}
		else {
			pop = stack.get(0);
			stack.remove(0);
		}
		return pop;
	}
	public int peek() {
		int peek = 0;
		if(this.isEmpty() == true) {
			peek = -1;
		}
		else {
			peek = stack.get(0);
		}
		return peek;
	}
	public int size () {
		return stack.size();
	}
	public boolean isEmpty() {
		boolean isEmpty = false;
		if(stack.size() == 0) {
			isEmpty = true;
		}
		return isEmpty;
	}
}
