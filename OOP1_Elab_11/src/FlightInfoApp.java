public class FlightInfoApp {  
	public static void main (String[] args) {   
		Time departure1 = new Time (12,0,0);   
		Time arrival1 = new Time (13,0,0);   
		FlightInfo flightInfo1 = new FlightInfo ("SK101", departure1, arrival1, "Bangkok", "Chiangmai");   
		System.out.println(flightInfo1);     
		Time departure2 = new Time (14,0,0);  
		Time arrival2 = new Time (15,0,0);   
		FlightInfo flightInfo2 = new FlightInfo ("SK102", departure2, arrival2, "Bangkok", "Phuket");  
		System.out.println(flightInfo2);   
		System.out.println(flightInfo2.equals(flightInfo2));   
		System.out.println(flightInfo2.equals(flightInfo1));              
	}    
} 