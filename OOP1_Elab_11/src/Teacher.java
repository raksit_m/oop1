
public class Teacher extends Person {
	private String faculty;

	public Teacher(String id, String name, String lastname, String gender) {
		super(id,name,lastname,gender);
	}
	public Teacher(String id, String name, String lastname, String gender, String faculty) {
		super(id,name,lastname,gender);
		this.faculty = faculty;
	}
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getFaculty() {
		return faculty;
	}
	public String toString() {
		return String.format("%s, %s %s, %s, %s",this.getID(),this.getName(),this.getLastname(),this.getGender(),this.getFaculty());
	}
}
