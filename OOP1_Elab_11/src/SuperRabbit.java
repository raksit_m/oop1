//elab-source: SuperRabbit.java
import java.util.ArrayList;
public class SuperRabbit extends Rabbit {
	private ArrayList <Integer> uneaten;
	private int max = -1;
	public SuperRabbit() {
		super();
		uneaten = new ArrayList <Integer> ();
	}
	public SuperRabbit(double x, double y) {
		super(x,y);
		uneaten = new ArrayList <Integer> ();
	}
	public void rush() {
		doubleSpeed();
		run();
	}
	public void run() {
		if(this.getX() > 0 && this.getY() > 0) {
			this.setX(-this.getX());
			this.setY(-this.getY());
		}
		else if(this.getX() < 0 && this.getY() > 0) {
			this.setX(-this.getX());
		}
		else if(this.getX() < 0 && this.getY() < 0) {
			this.setX(0);
			this.setY(0);
		}
		else if(this.getX() > 0 && this.getY() < 0) {
			rush();
		}
		else {
			halfSpeed();
			walk();
		}
	}
	public String getPosition() {
		return String.format("Super rabbit is at x = %.3f , y = %.3f, degree = %.3f",this.getX(),this.getY(),this.getDegree());
	}
	public void turnLeft(double degree) {
		this.setDegree(getDegree()+degree);
	}
	public void turnRight(double degree) {
		this.setDegree(getDegree()-degree);
	}
	public void eat(int nutrients) {
		if(nutrients > max) {
			max = nutrients;
			getEaten_food().add(nutrients);
		}
		else {
			uneaten.add(nutrients);
		}
	}
	public String getFoodInformation() {
		int sum1 = 1;
		String list1 = "";
		String output = "";
		for(int i = 0 ; i < getEaten_food().size() ; i++) {
			sum1 *= getEaten_food().get(i);
			list1 += getEaten_food().get(i) + " ";
		}
		output += String.format("Eaten food: %s, Sum of nutrients: %d\n",list1,sum1);
		int sum2 = 0;
		String list2 = "";
		for(int i = 0 ; i < uneaten.size() ; i++) {
			sum2 += uneaten.get(i);
			list2 += uneaten.get(i) + " ";
		}
		output += String.format("Uneaten: %s, Sum of uneaten: %d\n",list2,sum2);
		output += String.format("Different of nutrients: %d",Math.abs(sum1-sum2));
		return output;
	}
	
	public void doubleSpeed() {
		super.setSpeed(super.getSpeed()*2);
	}
	public void halfSpeed() {
	super.setSpeed(super.getSpeed()/2);
	}
}
