//elab-source: SimplifyingFraction.java
public class SimplifyingFraction extends Fraction {
	
	public SimplifyingFraction(int numerator, int denominator) {
		super(numerator,denominator);
		simplify();
	}
	public int gcd(int a, int b) {
		int gcd = 1;
		for(int i = Math.abs(a); i > 0 ; i--) {
			if(Math.abs(a)%i == 0 && Math.abs(b)%i == 0) {
				gcd = i;
				break;
			}
		}
		return gcd;
	}
	public void simplify () {
		int gcd = gcd(this.numerator,this.denominator);
		this.numerator /= gcd;
		this.denominator /= gcd;
	}
	public double evaluate() {
		return (this.numerator*1.0 / this.denominator);
	}
	public SimplifyingFraction add(int n) {
		Fraction sum = super.add(n);
		SimplifyingFraction result = new SimplifyingFraction (sum.numerator,sum.denominator);
		return result;
	}
	public SimplifyingFraction add(Fraction other) {
		Fraction sum = super.add(other);
		SimplifyingFraction result = new SimplifyingFraction (sum.numerator,sum.denominator);
		return result;
	}
	public SimplifyingFraction multiply(int n) {
		Fraction sum = super.multiply(n);
		SimplifyingFraction result = new SimplifyingFraction (sum.numerator,sum.denominator);
		return result;
	}
	public SimplifyingFraction multiply(Fraction other) {
		Fraction sum = super.multiply(other);
		SimplifyingFraction result = new SimplifyingFraction (sum.numerator,sum.denominator);
		return result;
	}
	public String toString () {
		return super.toString();
	}
}
