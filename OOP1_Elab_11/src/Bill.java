
public class Bill {
	private Customer payer;
	private Ticket ticket;
	private int billID;
	static private int numberOfBills = 1;
	
	public Bill (Customer payer, Ticket ticket) {
		this.payer = payer;
		this.ticket = ticket;
		this.billID = numberOfBills;
		numberOfBills++;
	}
	public String toString () {
		return String.format("Bill#%d: %s %s, %.2f", this.billID, payer.getName(), payer.getLastname(), ticket.getPrice());
	}
}
