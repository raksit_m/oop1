
public class FlightApp {
	public static void main (String[] args) {
		Time departure1 = new Time (12,0,0);
		Time arrival1 = new Time (13,0,0);
		FlightInfo sk101FlightInfo = new FlightInfo ("SK101", departure1, arrival1, "Bangkok", "Chiangmai");
		
		Customer adam = new Customer ("1001", "Adam", "Smith", "M", 0);
		Customer beth = new Customer ("1002", "Beth", "Woods", "F", 1);
		Customer cara = new Customer ("1003", "Cara", "Johnson", "F", 2);
		Customer david = new Customer ("1004", "David", "Brown", "M", 0);
		
		Flight flight1 = new Flight (new int[] {2,2,1}, sk101FlightInfo, 500, new double []{100, 200, 300});
		System.out.println(flight1.toString());
		
		System.out.println("Flight1: Booking");
		if (flight1.booking(adam, "Y") == -1)   
			System.out.println("Adam: Unsuccessful booking");
		if (flight1.booking(beth, "J")  == -1)   
			System.out.println("Beth: Unsuccessful booking");
		if (flight1.booking(cara, "F")  == -1)   
			System.out.println("Cara: Unsuccessful booking");
		if (flight1.booking(david, "F")  == -1)   
			System.out.println("David: Unsuccessful booking");
		System.out.println(flight1.toString());
		
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());
		System.out.println("Beth: Customer");
		System.out.println(beth.toString());
		System.out.println(beth.getTicketListToString());
				
		System.out.println("Flight1: Cancel");
		flight1.cancel(beth);
		System.out.println(flight1.toString());
		System.out.println("Beth: Customer");
		System.out.println(beth.toString());
		System.out.println(beth.getTicketListToString());
		
		System.out.println(flight1.searchPassenger(adam));
		System.out.println(flight1.searchPassenger(beth));
		
		System.out.println("Flight1: Clear");
		flight1.clear();
		System.out.println(flight1.toString());
		System.out.println("Adam: Customer");
		System.out.println(adam.toString());
		System.out.println(adam.getTicketListToString());
		System.out.println("Cara: Customer");
		System.out.println(cara.toString());
		System.out.println(cara.getTicketListToString());

	}
}
