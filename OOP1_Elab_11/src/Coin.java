//elab-source: Coin.java
public class Coin {
	private int value;
	public Coin (int value) {
		this.value = value;
	}
	public int getValue() {
		return this.value;
	}
	public String toString () {
		return this.value + " Baht Coin";
	}
}