public class BankNote implements Valuable {
	private int value;
	public BankNote(int value) {
		this.value = value;
	}
	public int getValue() {
		return this.value;
	}
	public String toString () {
		return this.value + " Baht BankNote";
	}
}