//elab-source: Purse.java
import java.util.ArrayList;

public class Purse {

	private ArrayList <Valuable> purse;
	private int size;
	private Comparator compare;

	public Purse(int Size) {
		purse = new ArrayList <Valuable> ();
		this.size = Size;
		compare = new Comparator();
	}
	public boolean add(Valuable temp) {
		if(purse.size() == size) {
			return false;
		}
		else {
			purse.add(temp);
			return true;
		}
	}
	public int getTotal() {
		int total = 0;
		for(int i = 0; i < purse.size(); i++) {
			total += purse.get(i).getValue();
		}
		return total;
	}
	public boolean isFull() {
		if(purse.size() == size) {
			return true;
		}
		else {
			return false;
		}
	}
	public int getSize() {
		return size;
	}
	public int count() {
		return purse.size();
	}
	public ArrayList<Valuable> getPurse() {
		return purse;
	}
	public void clear() {
		purse.clear();
	}
	public ArrayList <Valuable> withdraw(int amount) {
		ArrayList <Valuable> temp = new ArrayList <Valuable>();
		purse = compare.sort(purse);
		for(int i = purse.size()-1 ; i >= 0; i--) {
			if(amount >= purse.get(i).getValue()) {
				temp.add(purse.get(i));
				amount -= purse.get(i).getValue();
			}
		}
		if(purse.size() == 0) {
			return null;
		}
		else {
			return temp;
		}
	}
}
