//elab-source: Comparator.java
import java.util.ArrayList;

public class Comparator {

	public Comparator() {	

	}
	public ArrayList<Valuable> sort(ArrayList<Valuable> list) {
		ArrayList <Valuable> temp = new ArrayList <Valuable>();
		for(int i = 0 ; i <= list.size()-2 ; i++) {
			for(int j = 0 ; j < list.size()-(i+1) ; j++) {
				if(compareTo(list.get(j), list.get(j+1)) == -1) {
					Valuable swap = list.get(j);
					list.set(j, list.get(j+1));
					list.set(j+1, swap);
				}
			}
		}
		for(int i = 0; i < list.size(); i++) {
			temp.add(list.get(i));
		}
		return temp;
	}
	public int compareTo(Valuable a , Valuable b) {
		int compare = 0;
		if(a.getValue() < b.getValue()) {
			compare = 1;
		}
		else if(a.getValue() == b.getValue()) {
			compare = 0;
		}
		else if(a.getValue() > b.getValue()) {
			compare = -1;
		}
		return compare;
	}
}
