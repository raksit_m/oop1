public class Coin implements Valuable {
	private int value;
	public Coin (int value) {
		this.value = value;
	}
	public int getValue() {
		return this.value;
	}
	public String toString () {
		return this.value + " Baht Coin";
	}
}