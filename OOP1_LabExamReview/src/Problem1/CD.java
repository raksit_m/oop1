package Problem1;

public class CD extends Product {
	
	private String cdTitle;
	
	public CD(String productID, String description, double price, String cdTitle) {
		super(productID, description, price);
		this.cdTitle = cdTitle;
	}
	
	public String getCDTitle() {
		return cdTitle;
	}
	
	public String toString() {
		return String.format("%s, %s, %.2f, %s", super.getProductID(), super.getDescription(), super.getPrice(), getCDTitle());
	}
}
