package Problem1;

public class Book extends Product {

	private String bookTitle;
	private String ISBN;
	
	public Book(String productID, String description, double price, String bookTitle, String ISBN) {
		super(productID, description, price);
		this.bookTitle = bookTitle;
		this.ISBN = ISBN;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public String getISBN() {
		return ISBN;
	}
	
	public String toString() {
		return String.format("%s, %s, %.2f, %s, %s", super.getProductID(), super.getDescription(), super.getPrice(), getBookTitle(), getISBN());
	}
}
