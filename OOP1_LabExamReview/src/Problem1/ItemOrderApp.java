package Problem1;

public class ItemOrderApp {    
	public static void main (String[] args) {  
    CD cd1 = new CD("1111", "CD", 250, "Ultimate Mozart");
    CD cd2 = new CD("2222", "CD", 275, "Chopin Collection");       
    Book book1 = new Book("3333", "Book", 300, "The Hunger Games", "0439023521");   
    Book book2 = new Book("4444", "Book", 350, "Catching Fire", "0545586178");  
    ItemOrder[] orderArr =  new ItemOrder[4];  
    orderArr[0] = new ItemOrder(cd1,10);     
    System.out.println(orderArr[0]);  // print "1, 1111, CD, 250.00, Ultimate Mozart, 10, 15.00, 2125.00"  
    orderArr[1] = new ItemOrder(cd2,2);     
    System.out.println(orderArr[1]);  // print "2, 2222, CD, 275.00, Chopin Collection, 2, 0.00, 550.00"  
    orderArr[2] = new ItemOrder(book1,4);      
    System.out.println(orderArr[2]);  // print "3, 3333, Book, 300.00, The Hunger Games, 0439023521, 4, 10.00, 1080.00"  
    orderArr[3] = new ItemOrder(book2,20);      
    System.out.println(orderArr[3]);  // print "4, 4444, Book, 350.00, Catching Fire, 05455861781, 20, 20.00, 5600.00" 
	}
}
