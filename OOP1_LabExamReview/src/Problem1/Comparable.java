package Problem1;

public interface Comparable <T> {
	int compareTo(T obj);
	boolean equals(Product obj);
}
