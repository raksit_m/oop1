package Problem1;

public class ItemOrder {    

	private int itemOrderID; 
	private Product product;
	private int itemQty;
	private double itemDiscRate;
	private double itemSubtotal;	
	static int numItemOrder = 1;  

	public ItemOrder(CD aCD, int aQty) {
		product = (Product) aCD;
		this.itemOrderID = numItemOrder;
		this.itemQty = aQty;
		this.itemDiscRate = 0;
		this.itemSubtotal = aCD.getPrice();
		numItemOrder++; 
		setItemDiscRate();
	}

	public ItemOrder(Book aBook, int aQty) {
		product = (Product) aBook;
		this.itemOrderID = numItemOrder;
		this.itemQty = aQty;
		this.itemDiscRate = 0;
		this.itemSubtotal = aBook.getPrice();
		numItemOrder++;
		setItemDiscRate();
	}

	public void setItemDiscRate() {
		
		if(getItemSubtotal()*getItemQty() >= 1000 && getItemSubtotal()*getItemQty() < 2500) {
			itemDiscRate = 0.10; 
		}

		else if(getItemSubtotal()*getItemQty() >= 2500 && getItemSubtotal()*getItemQty() < 5000) {
			itemDiscRate = 0.15; 
		}

		else if(getItemSubtotal()*getItemQty() >= 5000) {
			itemDiscRate = 0.20; 
		}
	}

	public int getItemOrderID() {
		return itemOrderID;
	}

	public Product getProduct() {
		return product;
	}

	public int getItemQty() {
		return itemQty;
	}

	public double getItemDiscRate() {
		return itemDiscRate;
	}

	public double getItemSubtotal() {
		return itemSubtotal;
	}

	public String toString() {
		return String.format("%d, %s, %d, %.2f, %.2f", getItemOrderID(), getProduct().toString(), getItemQty(), getItemDiscRate()*100, getItemSubtotal()*getItemQty()*(1-getItemDiscRate()));
	}
} 

