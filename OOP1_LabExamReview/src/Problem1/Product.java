package Problem1;

public class Product implements Comparable {

	private String productID;
	private String description;
	private double price;
	
	public Product(String productID, String description, double price) {
		this.productID = productID;
		this.description = description;
		this.price = price;
	}

	public String getProductID() {
		return productID;
	}

	public String getDescription() {
		return description;
	}

	public double getPrice() {
		return price;
	}
	
	public int compareTo(Object temp) {
		Product temp2 = (Product) temp;
		int compare = this.getProductID().compareTo(temp2.getProductID());
		return compare;
	}
	
	public boolean equals(Product temp) {
		if( this.getProductID().equals(temp.getProductID())) {
			return true;
		}
		else {
			return false;
		}
	}	
}
