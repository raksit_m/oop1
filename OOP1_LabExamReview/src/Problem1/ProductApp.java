package Problem1;

public class ProductApp {    
	public static void main (String[] args) {
    CD cd1 = new CD("1111", "CD", 250, "Ultimate Mozart");  // display "1111, CD, 250.00, Ultimate Mozart"
    CD cd2 = new CD("2222", "CD", 275, "Chopin Collection");
    Book book1 = new Book("3333", "Book", 300, "The Hunger Games", "0439023521");
    Book book2 = new Book("4444", "Book", 350, "Catching Fire", "0545586178"); 
   System.out.println(cd1);    
   System.out.println(book2);   // display "4444, Book, 350.00, Catching Fire, 0545586178"
   System.out.println(cd1.compareTo(cd1));   // print zero      
   System.out.println(cd1.compareTo(cd2));   // print -1      
   System.out.println(book1.equals(book2));   // return false      
   System.out.println(book2.equals(book2));   // return true  
 } } 
