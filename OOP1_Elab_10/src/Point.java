
public class Point {
	private double x;
	private double y;
	public Point (double x, double y) {
	       this.x = x;
	       this.y = y;
	    }
	public boolean equals(double x, double y) {
		boolean isEquals = false;
		if(this.x == x && this.y == y) {
			isEquals = true;
		}
		return isEquals;
	}
	public boolean isOnXaxis() {
		boolean isEquals = false;
		if(this.y == 0) {
			isEquals = true;
		}
		return isEquals;
	}
	public double getX() {
		return this.x;
	}
	public double getY() {
		return this.y;
	}
	public boolean isOnYaxis() {
		boolean isEquals = false;
		if(this.x == 0) {
			isEquals = true;
		}
		return isEquals;
	}
	public void translate(double distX, double distY) {
		this.x += distX;
		this.y += distY;	
	}
	public String toString () {
		return "(" + this.x + "," + this.y + ")";
	}


}
