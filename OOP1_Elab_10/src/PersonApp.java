public class PersonApp {  
	public static void main (String[] args) {   
		Person adam = new Person ("1001", "Adam", "Smith", "M");   
		Person beth = new Person ("1002", "Beth", "Woods", "F");   
		Person cara = new Person ("1003", "Cara", "Johnson", "F");  
		Person david = new Person ("1004", "David", "Brown", "M");     
		System.out.println(adam.toString());      
		System.out.println(beth.toString());   
		System.out.println(cara.equals(cara));   
		System.out.println(cara.equals(david));        
		}    
	} 