
public class Flight {
	private int numSeats;
	private RoundInfo flightInfo;
	private double flightExpense;
	private Person [] passengerList;
	private int numBookings;
	private double ticketPrice;

	public Flight (int numSeats, RoundInfo flightInfo, double flightExpense, double ticketPrice) {
		this.numBookings = 0;
		this.numSeats = numSeats;
		this.flightInfo = flightInfo;
		this.flightExpense = flightExpense;
		this.ticketPrice = ticketPrice;
		passengerList = new Person [this.numSeats];
	}
	public int booking (Person passenger) {
		int check = 0;
		if(this.numBookings < this.numSeats) {
			passengerList[numBookings] = passenger;
			numBookings++;
			check = 1;
		}
		else {
			check = -1;
		}
		return check;
	}
	public void clear () {
		passengerList = new Person [this.numSeats];
		numBookings = 0;
	}
	public String toString() {   
		String temp = "";   
		temp += flightInfo.toString() + "\n";  
		if (numBookings > 0) {       
			for ( int i = 0; i < numBookings; i++ ) {               
				temp += String.format("%d,  %s\n", i+1, passengerList[i].toString());    
			} 
		} 
		else {    
			temp += "No booking\n";  
		}   
		return temp; 
	}  
	public int getNumSeats() { 
		return this.numSeats;
	}
	public RoundInfo getFlightInfo() {
		return this.flightInfo;
	}
	public double getFlightExpense() {
		return this.flightExpense;
	}
	public Person[] getPassengerList() {
		return this.passengerList;
	}
	public int getNumBookings() {
		return this.numBookings;
	}
	public double getTicketPrice() {
		return this.ticketPrice;
	}
}
