public class RoundInfoApp {  
	public static void main (String[] args) {   
		Time departure1 = new Time (12,0,0);   
		Time arrival1 = new Time (13,0,0);   
		RoundInfo flightInfo1 = new RoundInfo ("SK101", departure1, arrival1);      
		Time departure2 = new Time (14,0,0);   
		Time arrival2 = new Time (15,0,0);   
		RoundInfo flightInfo2 = new RoundInfo ("SK102", departure2, arrival2);  
		System.out.println(flightInfo1.toString());   
		System.out.println(flightInfo2.toString());     
		} 
	} 