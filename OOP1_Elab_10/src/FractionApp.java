import java.util.Scanner;
public class FractionApp {
	public static void main (String [] args) {
		Scanner scanner = new Scanner (System.in);
		System.out.print("Input numerator of Fraction #1 = ");
		int num1 = scanner.nextInt();
		System.out.print("Input denominator of Fraction #1 = ");
		int deno1 = scanner.nextInt();
		System.out.print("Input numerator of Fraction #2 = ");
		int num2 = scanner.nextInt();
		System.out.print("Input denominator of Fraction #2 = ");
		int deno2 = scanner.nextInt();
		
		Fraction frac1 = new Fraction (num1,deno1);
		Fraction frac2 = new Fraction (num2,deno2);
		System.out.println("Fraction #1 is " + frac1.toString());
		System.out.println("Fraction #2 is " + frac2.toString());
		System.out.println(frac1.toString() + " + " + frac2.toString() + " = " + frac1.add(frac2).toString());
		System.out.println(frac1.toString() + " * " + frac2.toString() + " = " + frac1.multiply(frac2).toString());
	}
}
