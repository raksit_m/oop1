public class Line {
	private double slope;
	private double yIntersept;
	private Point startPoint;
	private Point endPoint;
	public Line (double x1, double y1, double x2, double y2) {
	       /* Assign x1 and y1 to be start point. Call constructor of Point object. */
	       startPoint = new Point(x1,y1);
	       /* Assign x2 and y2 to be end point. Call constructor of Point object.    */
	       endPoint = new Point(x2,y2);
	       /* Use values of x1,y1,x2,y2 to find out the initialized values of slope and the yIntersept  */
	       slope = (y2-y1) / (x2-x1);
	       yIntersept = y1 - (slope*x1);
	    }
	public double getSlope () {
		return this.slope;
	}
	public double getYintersept () {
		return this.yIntersept;
	}
	public Point getStartPoint () {
		return this.startPoint;
	}
	public Point getEndPoint () {
		return this.endPoint;
	}
	public boolean contains(double x, double y) {
		boolean isContain = false;
			if((y == slope*x + yIntersept) && x >= startPoint.getX() && x <= endPoint.getX() && y >= startPoint.getY() && y <= endPoint.getY()) {
					isContain = true;
		}
		return isContain;
	}
	public double getX1() {
		return startPoint.getX();
	}
	public double getY1() {
		return startPoint.getY();
	}
	public double getX2() {
		return endPoint.getX();
	}
	public double getY2() {
		return endPoint.getY();
	}
	public double getDistance() {
		double distance = Math.sqrt(Math.pow(endPoint.getY()-startPoint.getY(),2) + Math.pow(endPoint.getX()-startPoint.getY(),2));
		return distance;
	}
	public double getY(double x) {
			double y = slope*x + yIntersept;
			if(contains(x,y) == false) {
				y = -999.999;
			}
		return y;
	}
}
