
public class StoreApp {
	public static void main (String[] args) {

		Person adam = new Person ("1001", "Adam", "Smith", "M");
		Person beth = new Person ("1002", "Beth", "Woods", "F");
		Person cara = new Person ("1003", "Cara", "Johnson", "F");
		Person david = new Person ("1004", "David", "Brown", "M");
	
		Time departure1 = new Time (12,0,0);
		Time arrival1 = new Time (13,0,0);
		RoundInfo flightInfo1 = new RoundInfo ("SK101", departure1, arrival1);
		
		Time departure2 = new Time (14,0,0);
		Time arrival2 = new Time (15,0,0);
		RoundInfo flightInfo2 = new RoundInfo ("SK102", departure2, arrival2);

		Flight flight1 = new Flight (5, flightInfo1, 250, 100);
		System.out.println("Flight 1 Information");
		System.out.print(flight1.toString());
		System.out.println();
		
		Store agency = new Store ();
		agency.booking(flight1, adam);
		agency.booking(flight1, beth);
		System.out.println("Flight 1 Information");
		System.out.print(flight1.toString());
		System.out.println();
		
		System.out.println("Agency Information");
		System.out.println(agency.toString());
		System.out.println();
		agency.receivePayment(flight1, adam);
		agency.receivePayment(flight1, beth);
		System.out.println("Agency Information");
		System.out.println(agency.toString());
		System.out.println();
		
		System.out.println("Clear Flight 1");
		agency.clearFlight(flight1);
		System.out.println("Flight 1 Information");
		System.out.print(flight1.toString());
		System.out.println();
		System.out.println("Agency Information");
		System.out.println(agency.toString());
		System.out.println();

		Flight flight2 = new Flight (3, flightInfo2, 220, 100);
		agency.booking(flight2, adam);
		agency.booking(flight2, cara);
		agency.booking(flight2, david);
		System.out.println("Flight 2 Information");
		System.out.print(flight2.toString());
		System.out.println();
		agency.receivePayment(flight2, adam);
		agency.receivePayment(flight2, cara);
		agency.receivePayment(flight2, david);
		System.out.println("Agency Information");
		System.out.println(agency.toString());
		System.out.println();


		System.out.println("Clear Flight 2");
		agency.clearFlight(flight2);
		System.out.println("Flight 2 Information");
		System.out.print(flight2.toString());
		System.out.println();
		System.out.println("Agency Information");
		System.out.println(agency.toString());
		System.out.println();
		System.out.println("Agency Profit");
		System.out.println(agency.getProfit());
				
	}
}


