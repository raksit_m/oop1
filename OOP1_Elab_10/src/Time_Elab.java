//elab-source: Time.java
public class Time_Elab {
	private int hour;
	private int minute;
	private int second;
	
	public Time_Elab(int hour, int minute, int second) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}
	public Time_Elab(int duration) {
		this.hour = duration/3600;
		duration %= 3600;
		this.minute = duration/60;
		duration %= 60;
		this.second = duration;
	}
	public void setHour (int newHour) {
		this.hour = newHour;
	}
	public void setMinute (int newMinute) {
		this.minute = newMinute;
	}
	public void setSecond (int newSecond) {
		this.second = newSecond;
	}
	public int getHour () {
		return this.hour;
	}
	public int getMinute () {
		return this.minute;
	}
	public int getSecond () {
		return this.second;
	}
	public int getDuration() {
		return (this.hour*3600) + (this.minute*60) + this.second;
	}
	public Time_Elab add(Time_Elab other) {
		Time_Elab sum = new Time_Elab (this.hour + other.hour,this.minute + other.minute,this.second + other.second);
		return sum;
	}
	public int subtract(Time_Elab other) {
		int time = 0;
		if(this.getDuration() < other.getDuration()) {
			time = this.getDuration() - other.getDuration() + 86400;
		}
		else {
			time = this.getDuration() - other.getDuration();
		}
		return time;
	}
	public boolean equals(Time_Elab other) {
		boolean isEquals = false;
		if(this.hour == other.hour && this.minute == other.minute && this.second == other.second) {
			isEquals = true;
		}
		return isEquals;
	}
	public String toString () {
		return String.format("%02d:%02d:%02d",this.hour,this.minute,this.second);
	}
}
