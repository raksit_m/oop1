
public class Store {
	private double revenue;
	private double expense;
	
	public Store () {
		this.revenue = 0;
		this.expense = 0;
	}
	public Store (double revenue, double expense) {
		this.revenue = revenue;
		this.expense = expense;
	}
	public int booking (Flight flight, Person passenger) {
		int check = flight.booking(passenger);
		return check;
	}
	public void receivePayment (Flight flight, Person passenger) {
		this.revenue += flight.getTicketPrice();
	}
	public void clearFlight (Flight flight) {
		this.expense += flight.getFlightExpense();
		flight.clear();
	}
	public double getRevenue() {
		return this.revenue;
	}
	public double getExpense() {
		return this.expense;
	}
	public double getProfit () {
		return this.revenue - this.expense;
	}
	public String toString () {
		return String.format("Revenue: %.2f, Expense: %.2f",this.revenue,this.expense);
	}
}
