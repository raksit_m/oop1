public class FlightApp {  
	public static void main (String[] args) {  
		Time departure1 = new Time (12,0,0);  
		Time arrival1 = new Time (13,0,0);   
		RoundInfo flightInfo1 = new RoundInfo ("SK101", departure1, arrival1);  
		Flight flight1 = new Flight (5, flightInfo1, 250, 100);
		Person adam = new Person ("1001", "Adam", "Smith", "M");   
		Person beth = new Person ("1002", "Beth", "Woods", "F");   
		Person cara = new Person ("1003", "Cara", "Johnson", "F"); 
		System.out.println(flight1.toString());  
		flight1.booking(adam);   
		flight1.booking(beth);  
		flight1.booking(cara);  
		System.out.println(flight1.toString());
		flight1.clear();   
		System.out.println(flight1.toString());   
	}
} 