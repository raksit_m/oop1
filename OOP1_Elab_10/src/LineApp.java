import java.util.Scanner;
public class LineApp {
	public static void main (String [] args) {
		Scanner scanner = new Scanner (System.in);
		System.out.print("Enter x1 : ");
		double x1 = scanner.nextDouble();
		System.out.print("Enter y1 : ");
		double y1 = scanner.nextDouble();
		System.out.print("Enter x2 : ");
		double x2 = scanner.nextDouble();
		System.out.print("Enter y2 : ");
		double y2 = scanner.nextDouble();

		Line point = new Line(x1,y1,x2,y2);
		System.out.printf("value of x1 on this line is %.3f\n",point.getX1());
		System.out.printf("value of x2 on this line is %.3f\n",point.getX2());
		System.out.printf("value of y1 on this line is %.3f\n",point.getY1());
		System.out.printf("value of y2 on this line is %.3f\n",point.getY2());
		System.out.println("==========");

		System.out.println("Check x and y are on this line ?");
		System.out.print("Enter x : ");
		double xx = scanner.nextDouble();
		System.out.print("Enter y : ");
		double yy = scanner.nextDouble();
		if(point.contains(xx,yy) == true) {
			System.out.printf("x = %.3f and y = %.3f are on this line\n",xx,yy);
		}
		else {
			System.out.printf("x = %.3f and y = %.3f are not on this line\n",xx,yy);
		}
		System.out.printf("Distance between startPoint and endPoint is %.3f\n",point.getDistance());
		System.out.println("==========");

		System.out.println("Find value of y that gives( x , y ) on this line ");
		System.out.print("Enter x : ");
		double x = scanner.nextDouble();
		System.out.printf("value of y is %.3f\n",point.getY(x));
		if(point.contains(x,point.getY(x)) == true) {
			System.out.printf("( x , y ) = ( %.3f , %.3f ) on this line",x,point.getY(x));
		}
		else {
			System.out.printf("( x , y ) = ( %.3f , %.3f ) is not on this line",x,point.getY(x));
		}
	}
}
