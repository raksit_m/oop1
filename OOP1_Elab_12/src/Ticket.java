
public class Ticket {
	private Customer passenger;
	private FlightInfo flightInfo;
	private double price;
	private String paymentStatus;
	private String travelClass;
	
	public Ticket (Customer passenger, FlightInfo flightInfo, String travelClass, double price) {
		this.passenger = passenger;
		this.flightInfo = flightInfo;
		this.travelClass = travelClass;
		this.price = price;
		setPaymentStatus("Charged");
		applyDiscount();
	}
	public void applyDiscount () {
		passenger.setDiscountRate(passenger.getMemberClass());
		this.price -= this.price*passenger.getDiscountRate();
	}
	
	public Customer getPassenger() {
		return passenger;
	}
	public FlightInfo getFlightInfo() {
		return flightInfo;
	}
	public double getPrice() {
		return price;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public String getTravelClass() {
		return travelClass;
	}

	public void setPaymentStatus (String newStatus) {
		this.paymentStatus = newStatus;
	}
	public boolean equals(Ticket key) {
		boolean isEquals = false;
		if(this.passenger == key.passenger && this.flightInfo == key.flightInfo) {
			isEquals = true;
		}
		return isEquals;
	}
	public String toString () {
		return String.format("%s, %.2f, %s", flightInfo.toString(), price, paymentStatus);
	}
}
