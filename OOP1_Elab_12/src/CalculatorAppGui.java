import javax.swing.JFrame;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class CalculatorAppGui extends JFrame {
	private static final int WIDTH = 250;
	private static final int HEIGHT = 175;

	private JLabel num1Label, num2Label, resultLabel;
	private JTextField num1TextField, num2TextField, resultTextField;
	private JButton addB, subtractB, multiplyB, divideB;

	//private inner ActionListener class
	private class AddButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			double num1 = Double.parseDouble(num1Str);
			double num2 = Double.parseDouble(num2Str);
			double result = num1 + num2;
			String resultStr = Double.toString(result);
			resultTextField.setText(resultStr);
		}
	}

	//private inner ActionListener class
	private class SubtractButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			double num1 = Double.parseDouble(num1Str);
			double num2 = Double.parseDouble(num2Str);
			double result = num1 - num2;
			String resultStr = Double.toString(result);
			resultTextField.setText(resultStr);
		}
	}

	//private inner ActionListener class
	private class MultiplyButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String num2Str = num2TextField.getText();
			double num1 = Double.parseDouble(num1Str);
			double num2 = Double.parseDouble(num2Str);
			double result = num1 * num2;
			String resultStr = Double.toString(result);
			resultTextField.setText(resultStr);
		}
	}

	//Constructor
	public CalculatorAppGui () {
		setTitle("Calculator");
		setSize(WIDTH,HEIGHT);
		Container pane = getContentPane();
		GridLayout aGrid = new GridLayout(5,2);

		pane.setLayout(aGrid);
		num1Label = new JLabel("1st Number");
		num2Label = new JLabel("2nd Number");
		resultLabel = new JLabel("Result");

		num1TextField = new JTextField(10);
		num2TextField = new JTextField(10);
		resultTextField = new JTextField(10);

		addB = new JButton("+");
		subtractB = new JButton("-");
		multiplyB = new JButton("*");
		divideB = new JButton("/");

		pane.add(num1Label);
		pane.add(num1TextField);
		pane.add(num2Label);
		pane.add(num2TextField);

		pane.add(addB);
		pane.add(subtractB);
		pane.add(multiplyB);
		pane.add(divideB);
		pane.add(resultLabel);
		pane.add(resultTextField);

		AddButtonHandler addbHandler = new AddButtonHandler();
		addB.addActionListener(addbHandler);

		SubtractButtonHandler subtractbHandler = new SubtractButtonHandler();
		subtractB.addActionListener(subtractbHandler);

		MultiplyButtonHandler multiplybHandler = new MultiplyButtonHandler();
		multiplyB.addActionListener(multiplybHandler);

		//Anonymous ActionListener class
		divideB.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				String num1Str = num1TextField.getText();
				String num2Str = num2TextField.getText();
				double num1 = Double.parseDouble(num1Str);
				double num2 = Double.parseDouble(num2Str);
				if(num2 == 0) {
					JOptionPane.showMessageDialog(null, "Division by 0");
					System.exit(0);
				}
				double result = num1 / num2;		
				String resultStr = Double.toString(result);
				resultTextField.setText(resultStr);
			}
		});
	}// end of constructor
	
	//main method
	public static void main (String [] args) {
		JFrame aCalculatorGui = new CalculatorAppGui();
		aCalculatorGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		aCalculatorGui.setVisible(true);
	}
}
