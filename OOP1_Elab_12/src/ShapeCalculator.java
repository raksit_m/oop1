import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.ImageIcon;

public class ShapeCalculator extends JFrame implements Runnable {
	
	public ShapeCalculator() {
		super("Shape Calculator");
		this.initComponets();
	}
	private JLabel title1, select, shape1a, shape1b, shape2a, shape2b, shape3a, shape3b, list1, list2, list3;
	private JLabel width, length, base, height,radius;
	private JTextField input1, input2, input3, input4, input5, input6, output1, output2, output3, output4, output5, output6 ;
	private JPanel pane;
	
		private void initComponets() {
			title1 = new JLabel("Shape Calculator",SwingConstants.CENTER);
			select = new JLabel("Select shape",SwingConstants.CENTER);
			list1 = new JLabel("Rectangle",SwingConstants.CENTER);
			list2 = new JLabel("Triangle",SwingConstants.CENTER);
			list3 = new JLabel("Circle",SwingConstants.CENTER);
			
			ClassLoader loader = this.getClass().getClassLoader();
			URL url1 = loader.getResource("Pictures/Rectangle.png");
			shape1a = new JLabel(new ImageIcon(url1));
			shape1b = new JLabel(new ImageIcon(url1));
			shape1a.addMouseListener(new MouseListener() {
				public void mouseClicked (MouseEvent e) {
					Rectangle();
				}		
				public void mouseEntered(MouseEvent e) {	
					
				}
				public void mouseExited(MouseEvent e) {
					
				}
				public void mousePressed(MouseEvent e) {
					
				}
				public void mouseReleased(MouseEvent e) {
						
				}
			});
						
			URL url2 = loader.getResource("Pictures/Triangle.png");
			shape2a = new JLabel(new ImageIcon(url2));
			shape2b = new JLabel(new ImageIcon(url2));
			shape2a.addMouseListener(new MouseListener() {
				public void mouseClicked (MouseEvent e) {
					Triangle();
				}		
				public void mouseEntered(MouseEvent e) {	
					
				}
				public void mouseExited(MouseEvent e) {
					
				}
				public void mousePressed(MouseEvent e) {
					
				}
				public void mouseReleased(MouseEvent e) {
						
				}
			});
			
			URL url3 = loader.getResource("Pictures/Circle.png");
			shape3a = new JLabel(new ImageIcon(url3));
			shape3b = new JLabel(new ImageIcon(url3));
			shape3a.addMouseListener(new MouseListener() {
				public void mouseClicked (MouseEvent e) {
					Circle();
				}		
				public void mouseEntered(MouseEvent e) {	
					
				}
				public void mouseExited(MouseEvent e) {
					
				}
				public void mousePressed(MouseEvent e) {
					
				}
				public void mouseReleased(MouseEvent e) {
						
				}
			});
			
			pane = new JPanel();
			pane.setLayout(new BoxLayout(pane,BoxLayout.Y_AXIS));
			JPanel head = new JPanel();
			head.add(title1);
			JPanel second = new JPanel();
			second.add(select);
			
			pane.add(head);
			pane.add(second);
			
			JPanel flow1 = new JPanel();
			flow1.setLayout(new FlowLayout());
			flow1.add(shape1a);
			flow1.add(shape2a);
			flow1.add(shape3a);
			
			JPanel flow2 = new JPanel();
			flow2.setLayout(new GridLayout(1,3));
			flow2.add(list1);
			flow2.add(list2);
			flow2.add(list3);
			
			pane.add(flow1);
			pane.add(flow2);
			super.add(pane);
		}
		private void Rectangle() {
			JLabel title2 = new JLabel("Rectangle",SwingConstants.CENTER);
			JLabel textper = new JLabel("Perimeter is");
			JLabel textarea = new JLabel("Area is");
			width = new JLabel("Enter Width:");
			input1 = new JTextField(10);
			length = new JLabel("Enter Height:");
			input2 = new JTextField(10);
			
			output1 = new JTextField(10);
			output2 = new JTextField(10);
			
			JButton button = new JButton("Go!");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double perimeter = 2*(Double.parseDouble(input1.getText()) + Double.parseDouble(input2.getText()));
					String tmp1 = String.format("%.2f",perimeter);
					output1.setText(tmp1);
					double area = (Double.parseDouble(input1.getText())*(Double.parseDouble(input2.getText())));
					String tmp2 = String.format("%.2f",area);
					output2.setText(tmp2);
				}
			});
			
			JFrame new1 = new JFrame("Rectangle Calculator");
			JPanel pane2 = new JPanel();
			pane2.setLayout(new BoxLayout(pane2,BoxLayout.Y_AXIS));
			pane2.add(title2);
			pane2.add(shape1b);
			
			JPanel body = new JPanel();
			body.setLayout(new FlowLayout());
			body.add(width);
			body.add(input1);
			body.add(length);
			body.add(input2);
			pane2.add(body);
			
			pane2.add(button);
			
			JPanel result = new JPanel();
			result.setLayout(new FlowLayout());
			result.add(textper);
			result.add(output1);
			result.add(textarea);
			result.add(output2);
			pane2.add(result);
			
			new1.add(pane2);
			new1.pack();
			new1.setVisible(true);
			new1.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		}
		private void Triangle() {
			JLabel title2 = new JLabel("Triangle",SwingConstants.CENTER);
			JLabel textper = new JLabel("Perimeter is");
			JLabel textarea = new JLabel("Area is");
			base = new JLabel("Enter Base:");
			input3 = new JTextField(10);
			height = new JLabel("Enter Height:");
			input4 = new JTextField(10);
			
			output3 = new JTextField(10);
			output4 = new JTextField(10);
			
			JButton button = new JButton("Go!");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double perimeter = (2*Math.sqrt(Math.pow(Double.parseDouble(input3.getText()),2) + Math.pow(Double.parseDouble(input4.getText())/2,2))) + Double.parseDouble(input4.getText());
					String tmp1 = String.format("%.2f",perimeter);
					output3.setText(tmp1);
					double area = 0.5*(Double.parseDouble(input3.getText())*(Double.parseDouble(input4.getText())));
					String tmp2 = String.format("%.2f",area);
					output4.setText(tmp2);
				}
			});
			
			JFrame new1 = new JFrame("Triangle Calculator");
			JPanel pane2 = new JPanel();
			pane2.setLayout(new BoxLayout(pane2,BoxLayout.Y_AXIS));
			pane2.add(title2);
			pane2.add(shape2b);
			
			JPanel body = new JPanel();
			body.setLayout(new FlowLayout());
			body.add(base);
			body.add(input3);
			body.add(height);
			body.add(input4);
			pane2.add(body);
			
			pane2.add(button);
			
			JPanel result = new JPanel();
			result.setLayout(new FlowLayout());
			result.add(textper);
			result.add(output3);
			result.add(textarea);
			result.add(output4);
			pane2.add(result);
			
			new1.add(pane2);
			new1.pack();
			new1.setVisible(true);
			new1.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		}
		private void Circle() {
			JLabel title2 = new JLabel("Circle",SwingConstants.CENTER);
			JLabel textper = new JLabel("Perimeter is");
			JLabel textarea = new JLabel("Area is");
			radius = new JLabel("Enter Radius:");
			input5 = new JTextField(10);
	
			
			output5 = new JTextField(10);
			output6 = new JTextField(10);
			
			JButton button = new JButton("Go!");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double perimeter = 2 * Math.PI * Double.parseDouble(input5.getText());
					String tmp1 = String.format("%.2f",perimeter);
					output5.setText(tmp1);
					double area = Math.PI * Double.parseDouble(input5.getText()) * Double.parseDouble(input5.getText());
					String tmp2 = String.format("%.2f",area);
					output6.setText(tmp2);
				}
			});
			
			JFrame new1 = new JFrame("Rectangle Calculator");
			JPanel pane2 = new JPanel();
			pane2.setLayout(new BoxLayout(pane2,BoxLayout.Y_AXIS));
			pane2.add(title2);
			pane2.add(shape3b);
			
			JPanel body = new JPanel();
			body.setLayout(new FlowLayout());
			body.add(radius);
			body.add(input5);
			body.add(button);
			pane2.add(body);
			
			JPanel result = new JPanel();
			result.setLayout(new FlowLayout());
			result.add(textper);
			result.add(output5);
			result.add(textarea);
			result.add(output6);
			pane2.add(result);
			
			new1.add(pane2);
			new1.pack();
			new1.setVisible(true);
			new1.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		}
		public void run() {
			this.pack();
			this.setVisible(true);
			this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		}
		public static void main (String [] args) {
			ShapeCalculator main = new ShapeCalculator();
			main.run();
		}		
}
