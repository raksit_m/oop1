import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.net.URL;
import javax.swing.ImageIcon;

public class ExchangeMachine extends JFrame implements Runnable {
	public ExchangeMachine () {
		super("Money Exchange Machine");
		this.initComponets();
	}
	private JLabel label1,label2,title;
	private JTextField text;
	private JTextField [] textfield;
	private JButton button;

	private void initComponets() {		
		label1 = new JLabel("Enter amount:");
		label2 = new JLabel("You get:");
		title = new JLabel("Money Exchanging Machine");

		text = new JTextField(10);

		textfield = new JTextField [8];
		for(int i = 0; i < textfield.length; i++) {
			textfield[i] = new JTextField(3);
		}
		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("Pictures/bank1000.png");
		JLabel img1 = new JLabel(new ImageIcon(url1));	
		URL url2 = loader.getResource("Pictures/bank500.png");
		JLabel img2 = new JLabel(new ImageIcon(url2));	
		URL url3 = loader.getResource("Pictures/bank100.png");
		JLabel img3 = new JLabel(new ImageIcon(url3));	
		URL url4 = loader.getResource("Pictures/bank50.png");
		JLabel img4 = new JLabel(new ImageIcon(url4));	
		URL url5 = loader.getResource("Pictures/bank20.png");
		JLabel img5 = new JLabel(new ImageIcon(url5));	
		URL url6 = loader.getResource("Pictures/coin10.png");
		JLabel img6 = new JLabel(new ImageIcon(url6));	
		URL url7 = loader.getResource("Pictures/coin5.png");
		JLabel img7 = new JLabel(new ImageIcon(url7));	
		URL url8 = loader.getResource("Pictures/coin1.png");
		JLabel img8 = new JLabel(new ImageIcon(url8));	

		button = new JButton("Exchange");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exchange(Integer.parseInt(text.getText()));
			}
		});


		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane,BoxLayout.Y_AXIS));
		pane.add(title);

		JPanel flow1 = new JPanel();
		flow1.setLayout(new FlowLayout());
		flow1.add(label1);
		flow1.add(text);
		flow1.add(button);

		JPanel flow2 = new JPanel();
		flow2.setLayout(new FlowLayout());
		flow2.add(textfield[0]);
		flow2.add(img1);
		flow2.add(textfield[1]);
		flow2.add(img2);
		flow2.add(textfield[2]);
		flow2.add(img3);
		flow2.add(textfield[3]);
		flow2.add(img4);

		JPanel flow3 = new JPanel();
		flow3.setLayout(new FlowLayout());
		flow3.add(textfield[4]);
		flow3.add(img5);
		flow3.add(textfield[5]);
		flow3.add(img6);
		flow3.add(textfield[6]);
		flow3.add(img7);
		flow3.add(textfield[7]);
		flow3.add(img8);

		super.add(pane);
		pane.add(flow1);
		pane.add(flow2);
		pane.add(flow3);
	}
	public void exchange(int money) {

		textfield[0].setText(Integer.toString(money/1000));
		money %= 1000;

		textfield[1].setText(Integer.toString(money/500));
		money %= 500;

		textfield[2].setText(Integer.toString(money/100));
		money %= 100;

		textfield[3].setText(Integer.toString(money/50));
		money %= 50;

		textfield[4].setText(Integer.toString(money/20));
		money %= 20;

		textfield[5].setText(Integer.toString(money/10));
		money %= 10;

		textfield[6].setText(Integer.toString(money/5));
		money %= 5;

		textfield[7].setText(Integer.toString(money));
	}
	public void run() {
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public static void main (String [] args) {
		ExchangeMachine money = new ExchangeMachine();
		money.run();
	}
}
