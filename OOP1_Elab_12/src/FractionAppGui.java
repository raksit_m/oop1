//import JFrame,Container,GridLayout,JLabel,JButton,JTextField
import javax.swing.JFrame;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

//Extend
public class FractionAppGui extends JFrame {
	//set WIDTH and HEIGHT
	private static final int WIDTH = 500;
	private static final int HEIGHT = 350;

	//Declare JLabel,JTextField,JButton
	private JLabel num1Label,deno1Label,num2Label,deno2Label,resultLabel;
	private JTextField num1TextField,deno1TextField,num2TextField,deno2TextField,resultTextField;
	private JButton sum1,sum2;

	//private inner ActionListener class
	private class SumButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String deno1Str = deno1TextField.getText();
			String num2Str = num2TextField.getText();
			String deno2Str = deno2TextField.getText();
			int num1 = Integer.parseInt(num1Str);
			int deno1 = Integer.parseInt(deno1Str);
			int num2 = Integer.parseInt(num2Str);
			int deno2 = Integer.parseInt(deno2Str);
			
			int sum1 = (num1*deno2)+(deno1*num2);
			int sum2 = (deno1*deno2);
			String resultStr = sum1 + "/" + sum2;
			resultTextField.setText(resultStr);
			if(deno1 == 0 || deno2 == 0) {
				JOptionPane.showMessageDialog(null,"Error: Denominator(s) cannot be Zero.");
				resultTextField.setText("Error");
			}
			else if(sum1 == sum2 && sum1 !=0) {
				resultTextField.setText("1");
			}
			else if(sum1 == 0 && sum2 != 0) {
				resultTextField.setText("0");
			}
		}
	}
	//private inner ActionListener class
	private class SimSumButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String num1Str = num1TextField.getText();
			String deno1Str = deno1TextField.getText();
			String num2Str = num2TextField.getText();
			String deno2Str = deno2TextField.getText();
			int num1 = Integer.parseInt(num1Str);
			int deno1 = Integer.parseInt(deno1Str);
			int num2 = Integer.parseInt(num2Str);
			int deno2 = Integer.parseInt(deno2Str);
			
			int sum1 = (num1*deno2)+(deno1*num2);
			int sum2 = (deno1*deno2);
			int gcd = 1;
			for(int i = Math.abs(sum1); i > 0; i--) {
				if(Math.abs(sum1)%i==0 && Math.abs(sum2)%i==0) {
					gcd = i;
					break;
				}
			}
			sum1 /= gcd;
			sum2 /= gcd;
			String resultStr = sum1 + "/" + sum2;
			resultTextField.setText(resultStr);

			if(sum2 == 0) {
				JOptionPane.showMessageDialog(null,"Error: Denominator(s) cannot be Zero.");
				resultTextField.setText("Error");
			}
			else if(sum1 == sum2 && sum1 !=0) {
				resultTextField.setText("1");
			}
			else if(sum1 == 0 && sum2 != 0) {
				resultTextField.setText("0");
			}
		}
	}
	
	public FractionAppGui () {
		setTitle("Fraction");
		setSize(WIDTH,HEIGHT);
		Container pane = getContentPane();
		GridLayout aGrid = new GridLayout(6,2);

		pane.setLayout(aGrid);
		num1Label = new JLabel("1st Numerator");
		deno1Label = new JLabel("1st Denominator");
		num2Label = new JLabel("2nd Numerator");
		deno2Label = new JLabel("2nd Denominator");
		resultLabel = new JLabel("Result");

		num1TextField = new JTextField(10);
		deno1TextField = new JTextField(10);
		num2TextField = new JTextField(10);
		deno2TextField = new JTextField(10);
		resultTextField = new JTextField(10);

		sum1 = new JButton("Sum");
		sum2 = new JButton("Sum & Sim");

		pane.add(num1Label);
		pane.add(num1TextField);
		pane.add(deno1Label);
		pane.add(deno1TextField);
		pane.add(num2Label);
		pane.add(num2TextField);
		pane.add(deno2Label);
		pane.add(deno2TextField);

		pane.add(sum1);
		pane.add(sum2);
		pane.add(resultLabel);
		pane.add(resultTextField);
		
		SumButtonHandler sumHandler = new SumButtonHandler();
		sum1.addActionListener(sumHandler);
		
		SimSumButtonHandler simsumHandler = new SimSumButtonHandler();
		sum2.addActionListener(simsumHandler);
	}
	public static void main (String [] args) {
		JFrame aFractionAppGui = new FractionAppGui();
		aFractionAppGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		aFractionAppGui.setVisible(true);
	}
}
