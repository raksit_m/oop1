import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class ShoppingMenu extends JFrame implements Runnable {
	public ShoppingMenu() {
		this.initComponets();
	}
	JFrame new1;
	JLabel title, select, choice1, choice2, img1, img2, time1, time2, time3, time4;
	JLabel seat1 = new JLabel();
	JLabel seat2 = new JLabel();
	JLabel seat3 = new JLabel();
	JLabel seat4 = new JLabel();
	JButton button1, button2, button3, button4;

	Time ft1 = new Time(12,0,0);
	Time tt1 = new Time(13,0,0);
	Time ft2 = new Time(13,0,0);
	Time tt2 = new Time(14,0,0);
	Time ft3 = new Time(10,0,0);
	Time tt3 = new Time(11,15,0);
	Time ft4 = new Time(14,0,0);
	Time tt4 = new Time(15,0,0);

	FlightInfo fInfo1 = new FlightInfo("SK101", ft1, tt1, "Bangkok", "Chiangmai");
	FlightInfo fInfo2 = new FlightInfo("SK102", ft2, tt2, "Bangkok", "Chiangmai");
	FlightInfo fInfo3 = new FlightInfo("SK103", ft3, tt3, "Bangkok", "Phuket");
	FlightInfo fInfo4 = new FlightInfo("SK104", ft4, tt4, "Bangkok", "Phuket");

	Flight flight1 = new Flight (new int[] {4,2,3}, fInfo1, 500, new double []{100, 200, 300});
	Flight flight2 = new Flight (new int[] {2,5,1}, fInfo2, 500, new double []{100, 200, 300});
	Flight flight3 = new Flight (new int[] {3,2,2}, fInfo3, 800, new double []{200, 400, 600});
	Flight flight4 = new Flight (new int[] {0,1,1}, fInfo4, 800, new double []{200, 400, 600});

	int diff1 = 0;
	int diff2 = 0;
	int diff3 = 0;
	int diff4 = 0;

	public void initComponets() {

		title = new JLabel("SKE Airline");
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		select = new JLabel("Select your flight");
		select.setAlignmentX(Component.CENTER_ALIGNMENT);
		choice1 = new JLabel("Chiang Mai");
		choice1.setAlignmentX(Component.CENTER_ALIGNMENT);
		choice2 = new JLabel("Phu Ket");
		choice2.setAlignmentX(Component.CENTER_ALIGNMENT);
		time1 = new JLabel("12:00-13:00");
		time2 = new JLabel("13:00-14:00");
		time3 = new JLabel("10:00-11:00");
		time4 = new JLabel("14:00-15:00");

		calculateDiff();
		updateLabel();

		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("Pictures/Chiang Mai.jpg");
		img1 = new JLabel(new ImageIcon(url1));
		URL url2 = loader.getResource("Pictures/Phu Ket.jpg");
		img2 = new JLabel(new ImageIcon(url2));

		button1 = new JButton("Go!");
		
			button1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(diff1 > 0) {
					NewCustomer add = new NewCustomer(getThis());
					add.setTitle("Chiang Mai : Flight 1");
					add.run();
					add.initComponets1();
					}
					else {
						JOptionPane.showMessageDialog(null,"Error: No Seats Available");
					}
				}
			});
		
		button2 = new JButton("Go!");
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(diff2 > 0) {
				NewCustomer add = new NewCustomer(getThis());
				add.setTitle("Chiang Mai : Flight 2");
				add.run();
				add.initComponets2();
				}
				else {
					JOptionPane.showMessageDialog(null,"Error: No Seats Available");
				}
			}
		});
		button3 = new JButton("Go!");
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(diff3 > 0) {
				NewCustomer add = new NewCustomer(getThis());
				add.setTitle("Phu Ket : Flight 1");
				add.run();
				add.initComponets3();
				}
				else {
					JOptionPane.showMessageDialog(null,"Error: No Seats Available");
				}
			}
		});
		button4 = new JButton("Go!");
		button4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(diff4 > 0) {
				NewCustomer add = new NewCustomer(getThis());
				add.setTitle("Phu Ket : Flight 2");
				add.run();
				add.initComponets4();
				}
				else {
					JOptionPane.showMessageDialog(null,"Error: No Seats Available");
				}
			}
		});

		new1 = new JFrame();
		new1.setTitle("Welcome to SKE Airline");
		JPanel pane1 = new JPanel();
		pane1.setLayout(new BoxLayout(pane1,BoxLayout.Y_AXIS));
		pane1.add(title);
		pane1.add(select);

		JPanel choice = new JPanel();
		choice.setLayout(new BoxLayout(choice,BoxLayout.X_AXIS));

		JPanel c1 = new JPanel();
		c1.setLayout(new BoxLayout(c1,BoxLayout.Y_AXIS));
		JPanel topic1 = new JPanel();
		topic1.setLayout(new FlowLayout(FlowLayout.CENTER));
		c1.add(choice1);
		topic1.add(img1);
		c1.add(topic1);

		JPanel f1 = new JPanel();
		f1.setLayout(new FlowLayout());	
		JPanel f1a = new JPanel();
		f1a.setLayout(new BoxLayout(f1a,BoxLayout.Y_AXIS));
		f1a.add(time1);
		f1a.add(seat1);
		f1a.add(button1);

		JPanel f1b = new JPanel();
		f1b.setLayout(new BoxLayout(f1b,BoxLayout.Y_AXIS));
		f1b.add(time2);
		f1b.add(seat2);
		f1b.add(button2);

		f1.add(f1a);
		f1.add(f1b);
		c1.add(f1);


		JPanel c2 = new JPanel();
		c2.setLayout(new BoxLayout(c2,BoxLayout.Y_AXIS));
		JPanel topic2 = new JPanel();
		topic2.setLayout(new FlowLayout(FlowLayout.CENTER));
		c2.add(choice2);
		topic2.add(img2);
		c2.add(topic2);

		JPanel f2 = new JPanel();
		f2.setLayout(new FlowLayout());	
		JPanel f2a = new JPanel();
		f2a.setLayout(new BoxLayout(f2a,BoxLayout.Y_AXIS));
		f2a.add(time3);
		f2a.add(seat3);
		f2a.add(button3);

		JPanel f2b = new JPanel();
		f2b.setLayout(new BoxLayout(f2b,BoxLayout.Y_AXIS));
		f2b.add(time4);
		f2b.add(seat4);
		f2b.add(button4);

		f2.add(f2a);
		f2.add(f2b);
		c2.add(f2);

		choice.add(c1);
		choice.add(c2);	
		pane1.add(choice);
		new1.add(pane1);
	}
	public void calculateDiff() {
		int sumSeats1 = 0;
		for(int i = 0; i < flight1.getNumSeats().length; i++) {
			sumSeats1 += flight1.getNumSeats()[i];
		}
		int sumSeats2 = 0;
		for(int i = 0; i < flight2.getNumSeats().length; i++) {
			sumSeats2 += flight2.getNumSeats()[i];
		}
		int sumSeats3 = 0;
		for(int i = 0; i < flight3.getNumSeats().length; i++) {
			sumSeats3 += flight3.getNumSeats()[i];
		}
		int sumSeats4 = 0;
		for(int i = 0; i < flight4.getNumSeats().length; i++) {
			sumSeats4 += flight4.getNumSeats()[i];
		}

		int sumBook1 = 0;
		for(int i = 0; i < flight1.getNumBookings().length; i++) {
			sumBook1 += flight1.getNumBookings()[i];
		}
		int sumBook2 = 0;
		for(int i = 0; i < flight2.getNumBookings().length; i++) {
			sumBook2 += flight2.getNumBookings()[i];
		}
		int sumBook3 = 0;
		for(int i = 0; i < flight3.getNumBookings().length; i++) {
			sumBook3 += flight3.getNumBookings()[i];
		}
		int sumBook4 = 0;
		for(int i = 0; i < flight4.getNumBookings().length; i++) {
			sumBook4 += flight4.getNumBookings()[i];
		}

		diff1 = sumSeats1 - sumBook1;
		diff2 = sumSeats2 - sumBook2;
		diff3 = sumSeats3 - sumBook3;
		diff4 = sumSeats4 - sumBook4;
	}
	public void run() {
		new1.pack();
		new1.setVisible(true);
		new1.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	public static void main (String [] args) {
		ShoppingMenu main = new ShoppingMenu();
		main.run();
	}
	public void updateLabel(){
		seat1.setText(String.format("%d seats left",diff1));
		seat2.setText(String.format("%d seats left",diff2));
		seat3.setText(String.format("%d seats left",diff3));
		seat4.setText(String.format("%d seats left",diff4));
	}
	public ShoppingMenu getThis() {
		return this;
	}
}
