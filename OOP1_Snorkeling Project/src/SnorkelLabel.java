import java.awt.Color;
import java.awt.Font;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class SnorkelLabel {

	protected static Database data;
	protected static JFrame new1;
	static JLabel lbBasic = new JLabel();
	static JLabel lbAverage = new JLabel();
	static JLabel lbPro = new JLabel();
	private static int index;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the application.
	 */
	public SnorkelLabel(Database data, final int index) {
		this.data = data;
		this.index = index;
		initialize();
	}

	/**
	 * Initialize the contents of the new1.
	 */
	private void initialize() {
		new1 = new JFrame();
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		new1.setBounds(600, 15, 600, 500);
		new1.getContentPane().setLayout(null);
		
		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/Snorkel Level.png");
		ImageIcon img1 = new ImageIcon(url1);
		
		lbBasic.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbBasic.setForeground(new Color(255, 255, 153));
		lbBasic.setText(Integer.toString(data.flightList[index].getNumSeats()[0] - data.flightList[index].getNumBookings()[0]) + " Left");
		lbBasic.setBounds(392, 99, 121, 14);

		lbAverage.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbAverage.setForeground(new Color(255, 255, 153));
		lbAverage.setText(Integer.toString(data.flightList[index].getNumSeats()[1] - data.flightList[index].getNumBookings()[1]) + " Left");
		lbAverage.setBounds(414, 219, 95, 14);

		lbPro.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbPro.setForeground(new Color(255, 255, 153));
		lbPro.setText(Integer.toString(data.flightList[index].getNumSeats()[2] - data.flightList[index].getNumBookings()[2]) + " Left");
		lbPro.setBounds(445, 340, 81, 14);
		
		new1.getContentPane().add(lbBasic);
		new1.getContentPane().add(lbAverage);
		new1.getContentPane().add(lbPro);
		
		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(-10, -10, 600, 500);
		new1.getContentPane().add(bg1);
	}
	public static void updateLabel() {
		lbBasic.setText(String.format("%d Left",data.flightList[index].getNumSeats()[0] - data.flightList[index].getNumBookings()[0]));
		lbAverage.setText(String.format("%d Left",data.flightList[index].getNumSeats()[1] - data.flightList[index].getNumBookings()[1]));
		lbPro.setText(String.format("%d Left",data.flightList[index].getNumSeats()[2] - data.flightList[index].getNumBookings()[2]));
		if(data.flightList[index].getNumSeats()[0] == data.flightList[index].getNumBookings()[0]) {
			lbBasic.setText("SOLD");
		}
		if(data.flightList[index].getNumSeats()[1] == data.flightList[index].getNumBookings()[1]) {
			lbAverage.setText("SOLD");
		}
		if(data.flightList[index].getNumSeats()[2] == data.flightList[index].getNumBookings()[2]) {
			lbPro.setText("SOLD");
		}
	}
}
