import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class NewCustomer extends JFrame {

	public static Database data;
	public static StoreAccount storeAccount;
	protected JFrame mainFrame;
	private JFrame new2;
	private JTextField tFFirstName;
	private JTextField tFLastName;
	private JComboBox cBMemberClass;
	private JLabel bg2 = new JLabel();

	public NewCustomer(Database data,StoreAccount storeAccount) {
		this.data = data;
		this.storeAccount = storeAccount;
		initialize();
	}
	public void initialize() {
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setBounds(100, 100, 900, 600);

		new2 = new JFrame();
		new2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		new2.setBounds(150, 100, 615, 285);

		new2.getContentPane().setLayout(null);
		mainFrame.getContentPane().setLayout(null);

		tFFirstName = new JTextField();
		tFFirstName.setBounds(286, 132, 170, 30);
		mainFrame.getContentPane().add(tFFirstName);
		tFFirstName.setColumns(10);

		tFLastName = new JTextField();
		tFLastName.setColumns(10);
		tFLastName.setBounds(286, 195, 170, 30);
		mainFrame.getContentPane().add(tFLastName);

		final JComboBox cBGender = new JComboBox();
		cBGender.setModel(new DefaultComboBoxModel(new String[] {"Male", "Female"}));
		cBGender.setBounds(286, 258, 140, 30);
		mainFrame.getContentPane().add(cBGender);

		cBMemberClass = new JComboBox();
		cBMemberClass.setModel(new DefaultComboBoxModel(new String[] {"Normal", "Gold", "Platinum"}));
		cBMemberClass.setBounds(286, 322, 140, 30);
		mainFrame.getContentPane().add(cBMemberClass);

		JButton btnAddCustomer = new JButton("Add Customer");
		btnAddCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String temp1 = tFFirstName.getText().replace(" ", "");
				tFFirstName.setText(temp1);
				String temp2 = tFLastName.getText().replace(" ", "");
				tFLastName.setText(temp2);
				boolean check = true;
				for(int i = 0; i < data.passengerList.size(); i++) {
					if(tFFirstName.getText().equals(data.passengerList.get(i).getName()) && tFLastName.getText().equals(data.passengerList.get(i).getLastname())) {
						JOptionPane.showMessageDialog(null,"Request Denied");
						check = false;
						break;
					}
					if(tFFirstName.getText().equals("") || tFLastName.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Please Input Valid Username or Password");
						check = false;
					}
				}
				int voucher = (int)(Math.random() * 2);
				if(voucher == 0) {
					data.temp = "Special";
					if(cBMemberClass.getSelectedItem().equals("Normal") && check == true) {
						bg2.setIcon(new ImageIcon(NewCustomer.class.getResource("/images/Voucher Normal.png")));
						bg2.setBounds(0, 0, 600, 250);
						new2.getContentPane().add(bg2);
						new2.setVisible(true);
					}
					else if(cBMemberClass.getSelectedItem().equals("Gold") && check == true) {
						bg2.setIcon(new ImageIcon(NewCustomer.class.getResource("/images/Voucher Gold.png")));
						bg2.setBounds(0, 0, 600, 250);
						new2.getContentPane().add(bg2);
						new2.setVisible(true);
					}
					else if(cBMemberClass.getSelectedItem().equals("Platinum") && check == true) {
						bg2.setIcon(new ImageIcon(NewCustomer.class.getResource("/images/Voucher Platinum.png")));
						bg2.setBounds(0, 0, 600, 250);
						new2.getContentPane().add(bg2);
						new2.setVisible(true);
					}
				}
				else if(voucher != 0) {
					data.temp = "None";
				}
				if(check) {
					Customer passenger = new Customer(tFFirstName.getText(), tFLastName.getText(), cBGender.getSelectedItem().toString(), (String)cBMemberClass.getSelectedItem(),data.temp);
					storeAccount.addCustomer(passenger);			
					JOptionPane.showMessageDialog(null, "Register Successful :\n" + passenger.toShowUp());
					data.writePassenger();
				}
				tFFirstName.setText("");
				tFLastName.setText("");
			}
		}
				);
		btnAddCustomer.setBounds(360, 427, 147, 30);
		mainFrame.getContentPane().add(btnAddCustomer);

		JLabel bg1 = new JLabel(new ImageIcon(NewCustomer.class.getResource("/images/NewCustomer.png")));
		bg1.setBounds(0, 0, 884, 561);
		mainFrame.getContentPane().add(bg1);

	}
}
