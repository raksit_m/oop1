import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Searching extends JFrame {

	public static Database data;
	private JPanel contentPane;
	protected JFrame new1;
	private JComboBox<String> comboBox_1;
	private JButton btnSearching;
	private int index;
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Searching(Database data, final int index) {
		this.data = data;
		this.index = index;
		initialize();
	}
	public void initialize() {
		new1 = new JFrame();
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		new1.setBounds(50, 50, 900, 600);

		ClassLoader loader1 = this.getClass().getClassLoader();
		URL urlC = loader1.getResource("images/Searching.png");
		ImageIcon img1 = new ImageIcon(urlC);
		new1.getContentPane().setLayout(null);

		comboBox_1 = new JComboBox <String> ();
		comboBox_1.setBounds(329, 173, 320, 30);
		for(int i = 0; i < 3; i++) {
			comboBox_1.addItem(data.flightList[index + i].getFlightInfo().toShowUp());
		}
		new1.getContentPane().add(comboBox_1);

		btnSearching = new JButton("Search");
		btnSearching.setBounds(376, 350, 100, 30);
		btnSearching.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String output = "Flight Found : \n" + data.flightList[index + comboBox_1.getSelectedIndex()].getFlightInfo().toShowUp() + "\n"; 
					for(int i = 0; i < data.passengerList.size(); i++) {
						for(int j = 0; j < data.passengerList.get(i).ticketList.size(); j++) {
							if(data.passengerList.get(i).ticketList.get(j).getFlightInfo().equals(data.flightList[index + comboBox_1.getSelectedIndex()].getFlightInfo())) {
								output += "Passenger : \n" + data.passengerList.get(i).toShowCB() + "\n";
								break;
							}
						}
					}
					JOptionPane.showMessageDialog(null, output);				
			}
		});
		new1.getContentPane().add(btnSearching);

		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 900, 600);
		new1.getContentPane().add(bg1);
	}

}
