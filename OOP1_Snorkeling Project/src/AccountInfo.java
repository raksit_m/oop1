import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AccountInfo extends JFrame {

	
	protected JFrame mainFrame;
	private JTextField tFRevenue;
	private JTextField tFExpenses;
	private JTextField tFProfit;
	
	public AccountInfo(StoreAccount storeAccount) {
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setBounds(100, 100, 900, 600);

		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/AccountInfo.png");
		ImageIcon img1 = new ImageIcon(url1);
		mainFrame.getContentPane().setLayout(null);

		tFRevenue = new JTextField("" + storeAccount.data.getRevenue());
		tFRevenue.setEditable(false);
		tFRevenue.setBounds(246, 160, 194, 34);
		tFRevenue.setColumns(10);
		mainFrame.getContentPane().add(tFRevenue);

		tFExpenses = new JTextField("" + storeAccount.data.getExpenses());
		tFExpenses.setEditable(false);
		tFExpenses.setBounds(246, 219, 194, 34);
		tFExpenses.setColumns(10);
		mainFrame.getContentPane().add(tFExpenses);

		tFProfit = new JTextField("" + storeAccount.data.getProfit());
		tFProfit.setEditable(false);
		tFProfit.setBounds(246, 281, 194, 34);
		tFProfit.setColumns(10);
		mainFrame.getContentPane().add(tFProfit);

		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 900, 600);
		mainFrame.getContentPane().add(bg1);

	}
}
