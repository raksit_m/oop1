import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class MainMenu {

	protected JFrame new1;
	private JButton btn1;
	private JButton btn2;
	public static Database data = new Database(); //Database is created just once.
	public static StoreAccount storeAccount =  new StoreAccount(data); //StoreAccount is created just once.
	public static SelectFlight select = new SelectFlight(data); //SelectFlight is created just once.

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu window = new MainMenu();
					window.new1.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainMenu() {
		initialize();
	}

	private void initialize() {
		new1 = new JFrame();
		new1.setBounds(100, 50, 900, 600);
		new1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		new1.getContentPane().setLayout(null);
		
		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/Main Menu.png");
		ImageIcon img1 = new ImageIcon(url1);
		
		btn1 = new JButton("Manage Trips");
		btn1.setBounds(375, 200, 150, 40);
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChooseIsland island = new ChooseIsland(data);
				island.new1.setVisible(true);
			}
		});
		
		btn2 = new JButton("My Account");
		btn2.setBounds(375, 315, 150, 40);
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login login = new Login(data,storeAccount.getStoreAccount()); //Passing both data and storeAccout
				login.mainFrame.setVisible(true);
			}
		});

		new1.getContentPane().add(btn1);
		new1.getContentPane().add(btn2);
		
		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 900, 600);
		new1.getContentPane().add(bg1);
	}

}
