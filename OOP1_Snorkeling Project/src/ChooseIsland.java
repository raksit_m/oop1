import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;


public class ChooseIsland {

	private ClassLoader loader;
	protected JFrame new1;
	private static Database data;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public ChooseIsland(Database data) {
		this.data = data;
		initialize();
	}

	/**
	 * Initialize the contents of the new1.
	 */
	private void initialize() {
		new1 = new JFrame();
		new1.setBounds(50, 10, 900, 700);
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/Choose Island.png");
		URL url2 = loader.getResource("images/Location_1.png");
		URL url3 = loader.getResource("images/Location_2.png");
		URL url4 = loader.getResource("images/Location_3.png");
		URL url5 = loader.getResource("images/Location_4.png");
		URL url6 = loader.getResource("images/Location_5.png");
		
		ImageIcon img1 = new ImageIcon(url1);
		ImageIcon img2 = new ImageIcon(url2);
		ImageIcon img3 = new ImageIcon(url3);
		ImageIcon img4 = new ImageIcon(url4);
		ImageIcon img5 = new ImageIcon(url5);
		ImageIcon img6 = new ImageIcon(url6);
		new1.getContentPane().setLayout(null);

		final JLabel lc1 = new JLabel(img2);
		lc1.setToolTipText("Hua Hin");
		lc1.setBounds(288, 263, 50, 50);
		lc1.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				data.indexSelect = 0;
				Submenu huahin = new Submenu(data);
				huahin.new1.setVisible(true);	
			}
			public void mouseEntered(MouseEvent e) {	
				lc1.setIcon(new ImageIcon(loader.getResource("images/Location_1_Glow.png")));
			}
			public void mouseExited(MouseEvent e) {
				lc1.setIcon(new ImageIcon(loader.getResource("images/Location_1.png")));
			}
			public void mousePressed(MouseEvent e) {
				
			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		
		JPopupMenu popup = new JPopupMenu();
		JMenuItem item = new JMenuItem("Information");
		popup.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Info");
			}
		});
		lc1.setComponentPopupMenu(popup);
		new1.getContentPane().add(lc1);

		final JLabel lc2 = new JLabel(img3);
		lc2.setToolTipText("Pattaya");
		lc2.setBounds(431, 187, 50, 50);
		lc2.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				data.indexSelect = 3;
				Submenu pattaya = new Submenu(data);
				pattaya.new1.setVisible(true);
			}
			public void mouseEntered(MouseEvent e) {	
				lc2.setIcon(new ImageIcon(loader.getResource("images/Location_2_Glow.png")));
			}
			public void mouseExited(MouseEvent e) {
				lc2.setIcon(new ImageIcon(loader.getResource("images/Location_2.png")));
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		new1.getContentPane().add(lc2);

		final JLabel lc3 = new JLabel(img4);
		lc3.setToolTipText("Phuket");
		lc3.setBounds(283, 445, 50, 50);
		lc3.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				data.indexSelect = 6;
				Submenu phuket = new Submenu(data);
				phuket.new1.setVisible(true);	
			}
			public void mouseEntered(MouseEvent e) {	
				lc3.setIcon(new ImageIcon(loader.getResource("images/Location_3_Glow.png")));
			}
			public void mouseExited(MouseEvent e) {
				lc3.setIcon(new ImageIcon(loader.getResource("images/Location_3.png")));
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		new1.getContentPane().add(lc3);
		
		final JLabel lc4 = new JLabel(img5);
		lc4.setToolTipText("Hat Yai");
		lc4.setBounds(441, 483, 50, 50);
		lc4.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				data.indexSelect = 9;
				Submenu hatyai = new Submenu(data);
				hatyai.new1.setVisible(true);	
			}
			public void mouseEntered(MouseEvent e) {	
				lc4.setIcon(new ImageIcon(loader.getResource("images/Location_4_Glow.png")));
			}
			public void mouseExited(MouseEvent e) {
				lc4.setIcon(new ImageIcon(loader.getResource("images/Location_4.png")));
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		new1.getContentPane().add(lc4);

		final JLabel lc5 = new JLabel(img6);
		lc5.setToolTipText("Koh Chang");
		lc5.setBounds(508, 237, 50, 50);
		lc5.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				data.indexSelect = 12;
				Submenu kohchang = new Submenu(data);
				kohchang.new1.setVisible(true);	
			}
			public void mouseEntered(MouseEvent e) {	
				lc5.setIcon(new ImageIcon(loader.getResource("images/Location_5_Glow.png")));
			}
			public void mouseExited(MouseEvent e) {
				lc5.setIcon(new ImageIcon(loader.getResource("images/Location_5.png")));
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		
		new1.getContentPane().add(lc5);
		
		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 900, 700);
		new1.getContentPane().add(bg1);
	}

}
