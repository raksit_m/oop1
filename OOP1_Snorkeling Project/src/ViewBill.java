import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ViewBill extends JFrame {

	public static StoreAccount storeAccount;
	protected JFrame mainFrame;
	private JTextField tFFirstName;
	private JTextField tFLastName;
	private JTextField textField;
	private JButton btnView;
	private JButton btnPrint;

	public ViewBill(StoreAccount storeAccount) {
		this.storeAccount = storeAccount;
		initialize();
	}
	public void initialize() {
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setBounds(100, 100, 900, 600);

		mainFrame.getContentPane().setLayout(null);

		tFFirstName = new JTextField();
		tFFirstName.setBounds(285, 165, 170, 30);
		mainFrame.getContentPane().add(tFFirstName);
		tFFirstName.setColumns(10);

		tFLastName = new JTextField();
		tFLastName.setColumns(10);
		tFLastName.setBounds(285, 227, 170, 30);
		mainFrame.getContentPane().add(tFLastName);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(285, 289, 170, 30);
		mainFrame.getContentPane().add(textField);

		btnView = new JButton("View");
		btnView.setBounds(285, 372, 112, 30);
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!tFFirstName.getText().equals("") && !tFLastName.getText().equals("") && textField.getText().equals("")) {
					String output = storeAccount.viewBill(tFFirstName.getText(), tFLastName.getText());
					JOptionPane.showMessageDialog(null, output);
				}
				else if(!tFFirstName.getText().equals("") && !tFLastName.getText().equals("") && !textField.getText().equals("")) {
					String output = storeAccount.viewBill(tFFirstName.getText(), tFLastName.getText(),Integer.parseInt(textField.getText()));
					JOptionPane.showMessageDialog(null, output);
				}
				else if(tFFirstName.getText().equals("") && tFLastName.getText().equals("") && !textField.getText().equals("")) {
					String output = storeAccount.viewBill(Integer.parseInt(textField.getText()));
					JOptionPane.showMessageDialog(null, output);
				}
				tFFirstName.setText("");
				tFLastName.setText("");
				textField.setText("");
			}			
		});
		mainFrame.getContentPane().add(btnView);

		//		btnPrint = new JButton("Print");
		//		btnPrint.setEnabled(false);
		//		btnPrint.setBounds(285, 438, 112, 30);
		//		mainFrame.getContentPane().add(btnPrint);

		JLabel bg1 = new JLabel(new ImageIcon(ViewBill.class.getResource("images/ViewBill.png")));
		bg1.setBounds(0, 0, 900, 600);
		mainFrame.getContentPane().add(bg1);

	}
}
