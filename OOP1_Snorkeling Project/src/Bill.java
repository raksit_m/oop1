
public class Bill {
	private Customer passenger;
	private Ticket ticket;
	private int billID;
	static private int numberOfBills = 1;
	
	public Bill (Customer payer, Ticket ticket) {
		this.passenger = payer;
		this.ticket = ticket;
		this.billID = numberOfBills;
		numberOfBills++;
	}
	public String toString () {
		return String.format("Bill#%d: %s,%s,%s,%s,%s,%s,%s,%s,%.2f",billID,passenger.getName(),passenger.getLastname(),ticket.getFlightInfo().getInfo(),ticket.getFlightInfo().getFromTime().toString(),ticket.getFlightInfo().getToTime().toString(),ticket.getFlightInfo().getFromCity(),ticket.getFlightInfo().getToCity(),ticket.getTravelClass(),ticket.getPrice());
	}
	public String toShowUp() {
		String [] product = ticket.getTravelClass().split(" - ");
		return String.format("Bill # %02d\nFirst Name : %s\nLast Name : %s\n%s , %s , %s , %s , %s\nProduct : %s\nPrice : %.2f Baht",billID,passenger.getName(),passenger.getLastname(),ticket.getFlightInfo().getInfo(),ticket.getFlightInfo().getFromTime().toString(),ticket.getFlightInfo().getToTime().toString(),ticket.getFlightInfo().getFromCity(),ticket.getFlightInfo().getToCity(),product[0],ticket.getPrice());
	}
	public int getBillID() {
		return billID;
	}
}
