
public class FlightInfo extends RoundInfo {
	private String fromCity;
	private String toCity;
	
	public FlightInfo () {
		this.fromCity = "";
		this.toCity = "";
	}
	public FlightInfo (String info, Time fromTime, Time toTime, String fromCity, String toCity) {
		super(info,fromTime,toTime);
		this.fromCity = fromCity;
		this.toCity = toCity;
	}
	public String getFromCity() {
		return fromCity;
	}
	public String getToCity() {
		return toCity;
	}
	public boolean equals(FlightInfo key) {
		boolean isEquals = false;
		if(this.getInfo().equals(key.getInfo()) && this.getFromTime().equals(key.getFromTime()) && this.getToTime().equals(key.getToTime()) && this.fromCity.equals(key.fromCity) && this.toCity.equals(key.toCity)) {
			isEquals = true;
		}
		return isEquals;
	}
	public String toString () {
//		return this.getInfo() + ", " + this.getFromTime().toString() + ", " + this.getToTime().toString() + ", " + this.fromCity + ", " + this.toCity;
		return String.format("%s,%s,%s,%s,%s",this.getInfo(),this.getFromTime().toString(),this.getToTime().toString(),this.fromCity,this.toCity);
	}
	public String toShowUp() {
		return String.format("%s , %s - %s , %s - %s\n",this.getInfo(),this.getFromTime().toString(),this.getToTime().toString(),this.fromCity,this.toCity);
	}
}
