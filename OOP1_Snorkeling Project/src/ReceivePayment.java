import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class ReceivePayment extends JFrame {

	public static Database data;
	public static StoreAccount storeAccount;
	protected JFrame mainFrame;
	private JTextField tFFirstName;
	private JTextField tFLastName;
	private JComboBox cBSelectFlight;
	private JButton btnSetToBePaid;

	public ReceivePayment(Database data, StoreAccount storeAccount) {
		this.data = data;
		this.storeAccount = storeAccount;
		initialize();
	}
	public void initialize() {
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setBounds(100, 100, 900, 600);

		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/AccountInfo.png");
		ImageIcon img1 = new ImageIcon(url1);
		mainFrame.getContentPane().setLayout(null);

		tFFirstName = new JTextField();
		tFFirstName.setBounds(277, 210, 194, 30);
		tFFirstName.setColumns(10);
		mainFrame.getContentPane().add(tFFirstName);

		tFLastName = new JTextField();
		tFLastName.setBounds(277, 271, 194, 30);
		tFLastName.setColumns(10);
		mainFrame.getContentPane().add(tFLastName);

		cBSelectFlight = new JComboBox();
		cBSelectFlight.setBounds(277, 151, 320, 30);
		for(int i = 0; i < data.flightList.length; i++) {
			cBSelectFlight.addItem(data.flightList[i].getFlightInfo().toShowUp());
		}
		mainFrame.getContentPane().add(cBSelectFlight);

		btnSetToBePaid = new JButton("OK");
		btnSetToBePaid.setBounds(400, 354, 90, 30);
		btnSetToBePaid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, storeAccount.receivePayment(cBSelectFlight.getSelectedIndex(), tFFirstName.getText(), tFLastName.getText()));
				data.writeAccountInfo();
			}
		}
				);
		mainFrame.getContentPane().add(btnSetToBePaid);

		JLabel bg1 = new JLabel(new ImageIcon(ReceivePayment.class.getResource("/images/ReceivePayment.png")));
		bg1.setBounds(0, 0, 900, 600);
		mainFrame.getContentPane().add(bg1);

	}
}
