import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Canceling extends JFrame {

	private static Database data;
	private static SelectFlight select;
	private JPanel contentPane;
	protected static JFrame new1;
	private JComboBox comboBox_1;
	private JComboBox comboBox_2;
	private JButton btnCanceling;
	private int index;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Canceling(Database data, SelectFlight select, final int index) {
		this.data = data;
		this.select = select;
		this.index = index;
		initialize();
	}
	public void initialize() {
		new1 = new JFrame();
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		new1.setBounds(50, 50, 900, 600);

		ClassLoader loader1 = this.getClass().getClassLoader();
		URL urlC = loader1.getResource("images/Canceling.png");
		ImageIcon img1 = new ImageIcon(urlC);
		new1.getContentPane().setLayout(null);

		comboBox_1 = new JComboBox <String> ();
		comboBox_1.setBounds(360, 152, 320, 30);
		for(int i = 0; i < data.passengerList.size(); i++) {
			comboBox_1.addItem(data.passengerList.get(i).toShowCB());
		}
		new1.getContentPane().add(comboBox_1);

		String [] flightChoice = new String [3];
		for(int i = 0 ; i <= 2 ; i++) {
			flightChoice[i] = data.flightList[i+index].getFlightInfo().toShowUp(); 
		}
		comboBox_2 = new JComboBox (flightChoice);
		comboBox_2.setBounds(360, 242, 320, 30);
		new1.getContentPane().add(comboBox_2);
		
		btnCanceling = new JButton("Cancel");
		btnCanceling.setBounds(400, 350, 100, 30);
		btnCanceling.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Customer passenger;
				int search = data.passengerList.get(comboBox_1.getSelectedIndex()).searchTicket(data.flightList[index+comboBox_2.getSelectedIndex()].getFlightInfo());
				if(search == -1) {
					JOptionPane.showMessageDialog(null, "Canceling Denied");
				}
				else {
					passenger = data.passengerList.get(comboBox_1.getSelectedIndex());
					if(data.passengerList.get(comboBox_1.getSelectedIndex()).ticketList.get(search).getTravelClass().equals("Basic Snorkel - 100 Baht")) {
						data.flightList[index + comboBox_2.getSelectedIndex()].getNumBookings()[0]--;
						data.numBookingsList.get(index + comboBox_2.getSelectedIndex())[0]--;
					}
					else if(data.passengerList.get(comboBox_1.getSelectedIndex()).ticketList.get(search).getTravelClass().equals("Average Snorkel - 200 Baht")) {
						data.flightList[index + comboBox_2.getSelectedIndex()].getNumBookings()[1]--;
						data.numBookingsList.get(index + comboBox_2.getSelectedIndex())[1]--;
					}
					else if(data.passengerList.get(comboBox_1.getSelectedIndex()).ticketList.get(search).getTravelClass().equals("Professional Snorkel - 300 Baht")) {
						data.flightList[index + comboBox_2.getSelectedIndex()].getNumBookings()[2]--;
						data.numBookingsList.get(index + comboBox_2.getSelectedIndex())[2]--;
					}
					
					String output = "Canceled :\n" + data.passengerList.get(comboBox_1.getSelectedIndex()).toShowUp() + "\n" + data.flightList[index].getFlightInfo().toShowUp() + "\nPrice : " + String.format("%.2f",data.passengerList.get(comboBox_1.getSelectedIndex()).ticketList.get(search).getPrice()) + " Baht" + "\nStatus : " + data.passengerList.get(comboBox_1.getSelectedIndex()).ticketList.get(search).getPaymentStatus() ;
					JOptionPane.showMessageDialog(null, output);
					data.passengerList.get(comboBox_1.getSelectedIndex()).ticketList.remove(search);
					
					data.writeBookings();
					data.writeTicketList(passenger);
					Canceling.this.data.calculateDiff();
					SelectFlight.updateLabel();
					SnorkelLabel.updateLabel();
					Canceling.this.select.updateThumbnail();
				}
			}
		});

		new1.getContentPane().add(btnCanceling);

		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 900, 600);
		new1.getContentPane().add(bg1);
	}

}
