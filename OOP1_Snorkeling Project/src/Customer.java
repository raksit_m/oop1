import java.util.ArrayList;
public class Customer extends Person {
	private String customerID;
	private String memberClass;
	private double discountRate;
	ArrayList <Ticket> ticketList;
	ArrayList <Bill> billList;
	static private int numberOfCustomers = 1;
	protected String voucher = "None";
	
	public Customer (String name,String lastname,String gender,String memberClass, String voucher) {
		super(name,lastname,gender);
		this.memberClass = memberClass;
		ticketList = new ArrayList <Ticket> ();
		billList = new ArrayList <Bill> ();
		this.customerID = "" + numberOfCustomers;
		this.voucher = voucher;
		setDiscountRate(this.memberClass);
		numberOfCustomers++;
	}
	
	public void addTicket (Ticket ticket) {
		ticketList.add(ticket);
	}
	public int searchTicket (FlightInfo key) {
		int index = -1; 
		for(int i = 0; i < ticketList.size(); i++) {
			if(ticketList.get(i).getFlightInfo().equals(key)) {
				index = i;
				break;
			}
		}
		return index;
	}
	public void addBill (Bill bill) {
		billList.add(bill);
	}
	public void setDiscountRate (String memberClass) {
		if(memberClass.equals("Normal")) {
			this.discountRate = 0.00;
		}
		else if(memberClass.equals("Gold")) {
			this.discountRate = 0.10;
		}
		else if(memberClass.equals("Platinum")) {
			this.discountRate = 0.20;
		}
	}
	public void setMemberClass (String memberClass) {
		this.memberClass = memberClass;
		setDiscountRate(this.memberClass);
	}
	public void setVoucher (String newVoucher) {
		this.voucher = newVoucher;
	}
	
	public boolean equals (Customer key) {
		boolean isEquals = false;
		if(this.customerID.equals(key.customerID) && super.equals(key) == true) {
			isEquals = true;
		}
		return isEquals;
	}
	public boolean equals (String name, String lastname) {
		boolean isEquals = false;
		if(super.getName().equals(name) && super.getLastname().equals(lastname)) {
			isEquals = true;
		}
		return isEquals;
	}
	public boolean equalsBill (String name, String lastname) {
		boolean isEquals = false;
		if(this.getName().equals(name) && this.getLastname().equals(lastname)) {
			isEquals = true;
		}
		return isEquals;
	}
	public String toString () {
		return String.format("%s,%s,%s,%.2f,%s", this.customerID, super.toString(), this.getMemberClass(), this.discountRate, this.voucher);
	}
	public String toShowUp() {
		return String.format("First Name : %s\nLast Name : %s\nGender : %s\nMember Class : %s\nDiscount Rate : %d%%\n", this.getName(),this.getLastname(),this.getGender(),this.getMemberClass(),(int)(this.getDiscountRate()*100));
	}
	public String toShowCB() {
		return String.format("%s %s , %s , %s , %d%% Discount , %s",this.getName(),this.getLastname(),this.getGender(),this.getMemberClass(),(int)(this.getDiscountRate()*100),this.getVoucher());
	}
	public String getTicketListToString () {
		String list = "";
		for(int i = 0; i < ticketList.size(); i++) {
			list += (ticketList.get(i).toString()) + "\n";
		}
		return list;
	}
	public String getBillListToString () {
		String list = "";
		for(int i = 0; i < billList.size(); i++) {
			list += (billList.get(i).toString()) + "\n";
		}
		return list;
	}

	public String getCustomerID() {
		return customerID;
	}
	
	public String getMemberClass() {
		return memberClass;
	}

	public double getDiscountRate() {
		return discountRate;
	}
	
	public String getVoucher() {
		return voucher;
	}

	public int getNumberOfCustomers() {
		return numberOfCustomers;
	}

	public ArrayList <Bill> getBillList() {
		return billList;
	}
	
}
