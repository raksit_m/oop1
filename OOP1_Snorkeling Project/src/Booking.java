import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Booking extends JFrame {

	protected static Database data;
	protected static SelectFlight select;
	protected static SnorkelLabel snorkel;
	protected static JFrame new1;
	private JFrame new2;
	ClassLoader loader;
	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JComboBox<String> comboBox_1;
	private JComboBox comboBox_2;
	private JButton btnBooking;
	private JLabel btnHelp;
	private JLabel bg1;
	private int index;
	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public Booking(Database data, SelectFlight select, final int index) {
		this.data = data;
		this.select = select;
		this.index = index;
		this.snorkel = new SnorkelLabel(data,index);
		initialize();
	}
	public void initialize() {
		new1 = new JFrame();
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		new1.setBounds(50, 50, 900, 600);

		loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/Booking.png");
		URL url2 = loader.getResource("images/Help.png");
		ImageIcon img1 = new ImageIcon(url1);
		ImageIcon img2 = new ImageIcon(url2);
		new1.getContentPane().setLayout(null);

		comboBox_1 = new JComboBox <String> ();
		comboBox_1.setBounds(300, 152, 320, 30);
		for(int i = 0; i < data.passengerList.size(); i++) {
			comboBox_1.addItem(data.passengerList.get(i).toShowCB());
		}
		new1.getContentPane().add(comboBox_1);


		comboBox_2 = new JComboBox();
		comboBox_2.setBounds(300, 235, 320, 30);
		comboBox_2.addItem("Basic Snorkel - 100 Baht");
		comboBox_2.addItem("Average Snorkel - 200 Baht");
		comboBox_2.addItem("Professional Snorkel - 300 Baht");
		new1.getContentPane().add(comboBox_2);

		btnBooking = new JButton("Book");
		btnBooking.setBounds(400, 350, 100, 30);
		btnBooking.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBox_2.getSelectedItem().equals("Basic Snorkel - 100 Baht")) {
					if(Booking.this.data.flightList[index].getNumBookings()[0] != Booking.this.data.flightList[index].getNumSeats()[0]) {
						Booking.this.data.flightList[index].getNumBookings()[0]++;
						Booking.this.data.numBookingsList.get(index)[0]++;
						Customer passenger = data.passengerList.get(comboBox_1.getSelectedIndex()); 
						Ticket ticket = new Ticket(passenger,data.flightList[index].getFlightInfo(),(String) comboBox_2.getSelectedItem(),data.flightList[index].getTravelClassPrice()[0]);
						String temp = "";
						if(passenger.getVoucher().equals("Special")) {
							ticket.setPrice(ticket.getPrice()*0.50);
							passenger.setVoucher("Used");
							temp = "Special Addition : Gift Voucher 50% Discount";
						}
						else if(passenger.getVoucher().equals("Used")) {
							passenger.setVoucher("None");
						}
						passenger.addTicket(ticket);
						data.writeTicketList(passenger);
						String output = "Booking Successful :\n" + data.passengerList.get(comboBox_1.getSelectedIndex()).toShowUp() + temp + "\n" + data.flightList[index].getFlightInfo().toShowUp() + "Price : " + String.format("%.2f",ticket.getPrice()) + " Baht";
						
						JOptionPane.showMessageDialog(null, output);
					}
					else {
						JOptionPane.showMessageDialog(null, "Out Of Stock !!");
					}
				}
				else if(comboBox_2.getSelectedItem().equals("Average Snorkel - 200 Baht")) {
					if(Booking.this.data.flightList[index].getNumBookings()[1] != Booking.this.data.flightList[index].getNumSeats()[1]) {
						Booking.this.data.flightList[index].getNumBookings()[1]++;
						Booking.this.data.numBookingsList.get(index)[1]++;
						Customer passenger = data.passengerList.get(comboBox_1.getSelectedIndex());
						Ticket ticket = new Ticket(passenger,data.flightList[index].getFlightInfo(),(String) comboBox_2.getSelectedItem(),data.flightList[index].getTravelClassPrice()[1]);
						String temp = "";
						if(passenger.getVoucher().equals("Special")) {
							ticket.setPrice(ticket.getPrice()*0.50);
							passenger.setVoucher("Used");
							temp = "Special Addition : Gift Voucher 50% Discount";
						}
						else if(passenger.getVoucher().equals("Used")) {
							passenger.setVoucher("None");
						}
						passenger.addTicket(ticket);
						data.writeTicketList(passenger);
						String output = "Booking Successful :\n" + data.passengerList.get(comboBox_1.getSelectedIndex()).toShowUp() + temp + "\n" + data.flightList[index].getFlightInfo().toShowUp() + "Price : " + String.format("%.2f",ticket.getPrice()) + " Baht";
						JOptionPane.showMessageDialog(null, output);
					}
					else {
						JOptionPane.showMessageDialog(null, "Out Of Stock !!");
					}
				}
				else if(comboBox_2.getSelectedItem().equals("Professional Snorkel - 300 Baht")) {
					if(Booking.this.data.flightList[index].getNumBookings()[2] != Booking.this.data.flightList[index].getNumSeats()[2]) {
						Booking.this.data.flightList[index].getNumBookings()[2]++;
						Booking.this.data.numBookingsList.get(index)[2]++;
						Customer passenger = data.passengerList.get(comboBox_1.getSelectedIndex());
						Ticket ticket = new Ticket(passenger,data.flightList[index].getFlightInfo(),(String) comboBox_2.getSelectedItem(),data.flightList[index].getTravelClassPrice()[2]);
						String temp = "";
						if(passenger.getVoucher().equals("Special")) {
							ticket.setPrice(ticket.getPrice()*0.50);
							passenger.setVoucher("Used");
							temp = "Special Addition : Gift Voucher 50% Discount";
						}
						else if(passenger.getVoucher().equals("Used")) {
							passenger.setVoucher("None");
						}
						passenger.addTicket(ticket);
						data.writeTicketList(passenger);
						String output = "Booking Successful :\n" + data.passengerList.get(comboBox_1.getSelectedIndex()).toShowUp() + temp + "\n" + data.flightList[index].getFlightInfo().toShowUp() + "Price : " + String.format("%.2f",ticket.getPrice()) + " Baht";
						JOptionPane.showMessageDialog(null, output);
					}
					else {
						JOptionPane.showMessageDialog(null, "Out Of Stock !!");
					}	
				}
				data.writePassenger();
				comboBox_1.removeAllItems();
				for(int i = 0; i < data.passengerList.size(); i++) {
					comboBox_1.addItem(data.passengerList.get(i).toShowCB());
				}
				data.writeBookings();
				Booking.this.data.calculateDiff();
				SelectFlight.updateLabel();
				SnorkelLabel.updateLabel();
				Booking.this.select.updateThumbnail();
			}
		});

		new1.getContentPane().add(btnBooking);

		btnHelp = new JLabel(img2);
		btnHelp.setBounds(632, 232, 35, 35);
		btnHelp.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				snorkel.new1.setVisible(true);
				data.opening = true;
			}
			public void mouseEntered(MouseEvent e) {	
				btnHelp.setIcon(new ImageIcon(loader.getResource("images/Help_Glow.png")));
			}
			public void mouseExited(MouseEvent e) {
				btnHelp.setIcon(new ImageIcon(loader.getResource("images/Help.png")));
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		new1.getContentPane().add(btnHelp);

		bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 900, 600);
		new1.getContentPane().add(bg1);
		if(index == data.indexSelect) {
			bg1.setIcon(new ImageIcon(loader.getResource("images/Booking1.png")));
		}
		else if(index == data.indexSelect + 1) {
			bg1.setIcon(new ImageIcon(loader.getResource("images/Booking2.png")));
		}
		else if(index == data.indexSelect + 2) {
			bg1.setIcon(new ImageIcon(loader.getResource("images/Booking3.png")));
		}
	}
}
