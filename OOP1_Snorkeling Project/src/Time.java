//elab-source: Time.java
public class Time {
	private int hour;
	private int minute;
	private int second;
	
	public Time(int hour, int minute) {
		this.hour = hour;
		this.minute = minute;
	}
	public Time(int duration) {
		this.hour = duration/3600;
		duration %= 3600;
		this.minute = duration/60;
		duration %= 60;
		this.second = duration;
	}
	public void setHour (int newHour) {
		this.hour = newHour;
	}
	public void setMinute (int newMinute) {
		this.minute = newMinute;
	}
	public void setSecond (int newSecond) {
		this.second = newSecond;
	}
	public int getHour () {
		return this.hour;
	}
	public int getMinute () {
		return this.minute;
	}
	public int getSecond () {
		return this.second;
	}
	public int getDuration() {
		return (this.hour*3600) + (this.minute*60) + this.second;
	}
	public Time add(Time other) {
		Time sum = new Time (this.hour + other.hour,this.minute);
		return sum;
	}
	public int subtract(Time other) {
		int time = 0;
		if(this.getDuration() < other.getDuration()) {
			time = this.getDuration() - other.getDuration() + 86400;
		}
		else {
			time = this.getDuration() - other.getDuration();
		}
		return time;
	}
	public boolean equals(Time key) {
		boolean isEquals = false;
		if(this.hour == key.hour && this.minute == key.minute) {
			isEquals = true;
		}
		return isEquals;
	}
	public String toString () {
		return String.format("%02d:%02d",this.hour,this.minute);
	}
}
