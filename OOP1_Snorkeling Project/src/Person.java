
public class Person {
	private String name;
	private String lastname;
	private String gender;
	
	public Person () {
		this.name = "";
		this.lastname = "";
		this.gender = "";
	}
	public Person (String name,String lastname,String gender) {
		this.name = name;
		this.lastname = lastname;
		this.gender = gender;
	}
	public String getName () {
		return this.name;
	}
	public String getLastname () {
		return this.lastname;
	}
	public String getGender () {
		return this.gender;
	}
	public boolean equals (Person key) {
		boolean check = false;
		if(this.name.equals(key.name) && this.lastname.equals(key.lastname) && this.gender.equals(key.gender)) {
			check = true;
		}
		return check;
	}
	public String toString () {
		return name + "," + lastname + "," + gender;
	}
}
