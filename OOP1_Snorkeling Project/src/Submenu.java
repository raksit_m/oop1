import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Submenu {

	private static Database data;
	protected JFrame new1;
	protected static SelectFlight select;
	protected static SnorkelLabel snorkel;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private URL url1;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public Submenu(Database data) {
		this.data = data;
		select = new SelectFlight(data);
		initialize();
	}

	/**
	 * Initialize the contents of the new1.
	 */
	private void initialize() {
		new1 = new JFrame();
		new1.setBounds(500, 100, 350, 550);
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		new1.getContentPane().setLayout(null);

		ClassLoader loader = this.getClass().getClassLoader();
		if(data.indexSelect == 0) {
			url1 = loader.getResource("images/Huahin.png");
		}
		else if(data.indexSelect == 3) {
			url1 = loader.getResource("images/Pattaya.png");
		}
		else if(data.indexSelect == 6) {
			url1 = loader.getResource("images/Phuket.png");
		}
		else if(data.indexSelect == 9) {
			url1 = loader.getResource("images/HatYai.png");
		}
		else if(data.indexSelect == 12) {
			url1 = loader.getResource("images/KohChang.png");
		}

		ImageIcon img1 = new ImageIcon(url1);

		btn1 = new JButton("Booking");
		btn1.setBounds(115, 115, 100, 30);
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectFlight.new1.setVisible(true);
				select.data.setChoice("Booking");
			}
		});

		new1.getContentPane().add(btn1);

		btn2 = new JButton("Canceling");
		btn2.setBounds(113, 200, 100, 30);
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectFlight.new1.setVisible(false);
				if(data.opening) {
				SnorkelLabel.new1.setVisible(false);
				}
				Canceling cancel = new Canceling(data,select,data.indexSelect);
				cancel.new1.setVisible(true);

			}
		});

		new1.getContentPane().add(btn2);

		btn3 = new JButton("Clearing");
		btn3.setBounds(113, 285, 100, 30);
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectFlight.new1.setVisible(false);
				if(data.opening) {
					SnorkelLabel.new1.setVisible(false);
					}
				Clearing clear = new Clearing(data,select,data.indexSelect);
				clear.new1.setVisible(true);
			}
		});

		new1.getContentPane().add(btn3);

		btn4 = new JButton("Searching");
		btn4.setBounds(113, 370, 100, 30);
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectFlight.new1.setVisible(false);
				if(data.opening) {
					SnorkelLabel.new1.setVisible(false);
					}
				Searching search = new Searching(data,data.indexSelect);
				search.new1.setVisible(true);
			}
		});

		new1.getContentPane().add(btn4);

		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 334, 511);
		new1.getContentPane().add(bg1);
	}
}
