import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class SearchCustomer extends JFrame {

	public static Database data;
	public static StoreAccount storeAccount;
	protected JFrame mainFrame;
	private JTextField tFFirstName;
	private JTextField tFLastName;
	private JButton btnSearch;

	public SearchCustomer(Database data, final StoreAccount storeAccount) {
		this.data = data;
		this.storeAccount = storeAccount;
		initialize();
	}
	public void initialize() {
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setBounds(100, 100, 900, 600);

		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/AccountInfo.png");
		ImageIcon img1 = new ImageIcon(url1);
		mainFrame.getContentPane().setLayout(null);

		tFFirstName = new JTextField();
		tFFirstName.setBounds(285, 167, 170, 30);
		mainFrame.getContentPane().add(tFFirstName);
		tFFirstName.setColumns(10);

		tFLastName = new JTextField();
		tFLastName.setColumns(10);
		tFLastName.setBounds(285, 251, 170, 30);
		mainFrame.getContentPane().add(tFLastName);

		btnSearch = new JButton("Search");
		btnSearch.setBounds(400, 351, 100, 30);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(storeAccount.searchCustomer(tFFirstName.getText(), tFLastName.getText()) == -1) {
					JOptionPane.showMessageDialog(null, "Customer not found");
				}
				else {
					JOptionPane.showMessageDialog(null, "Customer Found : \n" + data.passengerList.get(storeAccount.searchCustomer(tFFirstName.getText(), tFLastName.getText())).toShowUp());
				}
				tFFirstName.setText("");
				tFLastName.setText("");
			}
		});
		mainFrame.getContentPane().add(btnSearch);

		JLabel bg1 = new JLabel(new ImageIcon(SearchCustomer.class.getResource("/images/SearchCustomer.png")));
		bg1.setBounds(0, 0, 900, 600);
		mainFrame.getContentPane().add(bg1);

	}
}
