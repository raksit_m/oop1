import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Database {
	// Define Time, FlightInfo and Flight
	Time ft1 = new Time(8,0);
	Time tt1 = new Time(10,0);
	Time ft2 = new Time(11,0);
	Time tt2 = new Time(13,0);
	Time ft3 = new Time(14,0);
	Time tt3 = new Time(16,0);

	FlightInfo fInfo1 = new FlightInfo("HH101",ft1,tt1,"Bangkok","HauHin");
	FlightInfo fInfo2 = new FlightInfo("HH102",ft2,tt2,"Bangkok","HauHin");
	FlightInfo fInfo3 = new FlightInfo("HH103",ft3,tt3,"Bangkok","HauHin");
	FlightInfo fInfo4 = new FlightInfo("PY101",ft1,tt1,"Bangkok","Pattaya");
	FlightInfo fInfo5 = new FlightInfo("PY102",ft2,tt2,"Bangkok","Pattaya");
	FlightInfo fInfo6 = new FlightInfo("PY103",ft3,tt3,"Bangkok","Pattaya");
	FlightInfo fInfo7 = new FlightInfo("PK101",ft1,tt1,"Bangkok","Phuket");
	FlightInfo fInfo8 = new FlightInfo("PK102",ft2,tt2,"Bangkok","Phuket");
	FlightInfo fInfo9 = new FlightInfo("PK103",ft3,tt3,"Bangkok","Phuket");
	FlightInfo fInfo10 = new FlightInfo("HY101",ft1,tt1,"Bangkok","HatYai");
	FlightInfo fInfo11 = new FlightInfo("HY102",ft2,tt2,"Bangkok","HatYai");
	FlightInfo fInfo12 = new FlightInfo("HY103",ft3,tt3,"Bangkok","HatYai");
	FlightInfo fInfo13 = new FlightInfo("KC101",ft1,tt1,"Bangkok","KohChang");
	FlightInfo fInfo14 = new FlightInfo("KC102",ft2,tt2,"Bangkok","KohChang");
	FlightInfo fInfo15 = new FlightInfo("KC103",ft3,tt3,"Bangkok","KohChang");

	Flight hh101 = new Flight (new int[] {2,1,2}, fInfo1, 500, new double []{100, 200, 300});
	Flight hh102 = new Flight (new int[] {3,1,1}, fInfo2, 500, new double []{100, 200, 300});
	Flight hh103 = new Flight (new int[] {2,2,1}, fInfo3, 500, new double []{100, 200, 300});
	Flight py101 = new Flight (new int[] {2,1,1}, fInfo4, 400, new double []{100, 200, 300});
	Flight py102 = new Flight (new int[] {2,3,1}, fInfo5, 400, new double []{100, 200, 300});
	Flight py103 = new Flight (new int[] {2,1,2}, fInfo6, 400, new double []{100, 200, 300});
	Flight pk101 = new Flight (new int[] {1,1,2}, fInfo7, 800, new double []{100, 200, 300});
	Flight pk102 = new Flight (new int[] {2,2,2}, fInfo8, 800, new double []{100, 200, 300});
	Flight pk103 = new Flight (new int[] {1,1,2}, fInfo9, 800, new double []{100, 200, 300});
	Flight hy101 = new Flight (new int[] {1,2,2}, fInfo10, 700, new double []{100, 200, 300});
	Flight hy102 = new Flight (new int[] {1,1,1}, fInfo11, 700, new double []{100, 200, 300});
	Flight hy103 = new Flight (new int[] {3,2,2}, fInfo12, 700, new double []{100, 200, 300});
	Flight kc101 = new Flight (new int[] {1,1,3}, fInfo13, 500, new double []{100, 200, 300});
	Flight kc102 = new Flight (new int[] {1,3,2}, fInfo14, 500, new double []{100, 200, 300});
	Flight kc103 = new Flight (new int[] {1,3,1}, fInfo15, 500, new double []{100, 200, 300});

	Flight [] flightList = new Flight [15];  // Containing Flights
	public static ArrayList <Customer> passengerList = new ArrayList <Customer>();

	int [] seatList = new int[15];  // Containing seat left of each flight
	String choice; // Booking, Canceling, Clearing or Searching
	static int indexSelect; // Flight choice
	static ArrayList <int []> numBookingsList = new ArrayList <int []>();
	int [] status = new int [15]; // Can and cannot book

	String temp = "";
	String temp2 = "";
	boolean opening = false;
	public static  double revenue;
	public  double expenses;

	// Constructor
	public Database() {
		// add Flight to each element
		flightList[0] = hh101;
		flightList[1] = hh102;
		flightList[2] = hh103;
		flightList[3] = py101;
		flightList[4] = py102;
		flightList[5] = py103;
		flightList[6] = pk101;
		flightList[7] = pk102;
		flightList[8] = pk103;
		flightList[9] = hy101;
		flightList[10] = hy102;
		flightList[11] = hy103;
		flightList[12] = kc101;
		flightList[13] = kc102;
		flightList[14] = kc103;

		readPassenger();
		readTicketList();
		readBookings();
		readBill();
		readAccountInfo();
		
		initialExpense();

		calculateDiff(); // call method calculateDiff for adding to the seatList

		choice = "";
		indexSelect = 0;
	}
	public void initialExpense() {
		this.expenses = 0;
		for(int i =0;i<flightList.length;i++){
			this.expenses += flightList[i].getFlightExpense();
		}
	}
	public void readPassenger() {
		File file = new File("D://PassengerList.txt");
		if(!file.exists()) {
			try {
				file.createNewFile();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		try {
			Scanner scanner = new Scanner(file);
			while(scanner.hasNext()) {
				String customerStr = scanner.nextLine().replace(" ", "");
				String[] customerArr = customerStr.split(",");
				Customer passenger = new Customer(customerArr[1],customerArr[2],customerArr[3],customerArr[4],customerArr[6]);
				passengerList.add(passenger);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void writePassenger() {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter("D://PassengerList.txt"));
			for(int i = 0; i < passengerList.size(); i++) {
				output.write(passengerList.get(i).toString());
				output.newLine();
			}
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readTicketList() {
		File file = new File("D://TicketList.txt");
		if(!file.exists()) {
			try {
				file.createNewFile();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		try {
			Scanner scanner = new Scanner(file);
			while(scanner.hasNext()) {
				String[] ticketArr = scanner.nextLine().split(",");
				Customer passenger = new Customer(ticketArr[0],ticketArr[1],ticketArr[2],ticketArr[3],ticketArr[12]);
				int index = -1;
				for(int i = 0; i < passengerList.size(); i++) {
					if(passengerList.get(i).equals(passenger.getName(),passenger.getLastname())) {
						index = i;
						break;
					}
				}
				String [] ftInput = ticketArr[5].split(":");
				Time ft = new Time(Integer.parseInt(ftInput[0]),0);
				String [] ttInput = ticketArr[6].split(":");
				Time tt = new Time(Integer.parseInt(ttInput[0]),0);
				FlightInfo fInfo = new FlightInfo(ticketArr[4],ft,tt,ticketArr[7],ticketArr[8]);
				double price = 0;
				if(ticketArr[9].equals("Basic Snorkel - 100 Baht")) {
					price = 100.00;
				}
				else if(ticketArr[9].equals("Average Snorkel - 200 Baht")) {
					price = 200.00;
				}
				else if(ticketArr[9].equals("Professional Snorkel - 300 Baht")) {
					price = 300.00;
				}
				Ticket ticket = new Ticket(passenger,fInfo,ticketArr[9],price);
				passengerList.get(index).ticketList.add(ticket);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void writeTicketList(Customer passenger) {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter("D://TicketList.txt"));
			for(int i = 0; i < passenger.ticketList.size(); i++) {
				output.write(passenger.ticketList.get(i).toString());
				output.newLine();
			}
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readBookings() {
		File file = new File("D://NumBookings.txt");
		numBookingsList.clear();
		if(!file.exists()) {
			try {
				file.createNewFile();
				BufferedWriter output = new BufferedWriter(new FileWriter("D://NumBookings.txt"));
				for(int i = 0; i < 15; i++) {
					output.write("0,0,0");
					output.newLine();
				}
				output.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			Scanner scanner = new Scanner(file);
			while(scanner.hasNext()) {
				String [] inputBook = scanner.nextLine().split(",");
				int [] inputBook2 = new int[3];
				for(int i = 0; i < inputBook2.length; i++) {
					inputBook2[i] = Integer.parseInt(inputBook[i]);
				}
				numBookingsList.add(inputBook2);			
			}
			scanner.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setNumBookings();
	}

	public void setNumBookings() {
		for(int i = 0 ;i < flightList.length;i++){
			flightList[i].getNumBookings()[0] = numBookingsList.get(i)[0];
			flightList[i].getNumBookings()[1] = numBookingsList.get(i)[1];
			flightList[i].getNumBookings()[2] = numBookingsList.get(i)[2];
		}
	}
	public void writeBookings() {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter("D://NumBookings.txt"));
			for(int i = 0; i < flightList.length; i++) {
				for(int j = 0; j < 3; j++) {
					String temp = "" + flightList[i].getNumBookings()[j];
					if(j < 2) {
						temp += ",";
					}
					output.write(temp);
				}
				output.newLine();
			}
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readBill() {
		File file = new File("D://BillList.txt");
		if(!file.exists()) {
			try {
				file.createNewFile();

			} catch(IOException e) {
				e.printStackTrace();
			}		
		}
		try {
			Scanner scanner = new Scanner(file);
			while(scanner.hasNext()) {
				String[] billArr = scanner.nextLine().split(",");
				String [] name = billArr[0].split(" ");
				int search = StoreAccount.searchCustomer(name[1],billArr[1]);
				if(search != -1) {
					Customer passenger = passengerList.get(search);
					String [] ftInput = billArr[3].split(":");
					Time ft = new Time(Integer.parseInt(ftInput[0]),0);
					String [] ttInput = billArr[4].split(":");
					Time tt = new Time(Integer.parseInt(ttInput[0]),0);
					FlightInfo fInfo = new FlightInfo(billArr[2],ft,tt,billArr[5],billArr[6]);
					double price = 0;
					if(billArr[7].equals("Basic Snorkel - 100 Baht")) {
						price = 100.00;
					}
					else if(billArr[7].equals("Average Snorkel - 200 Baht")) {
						price = 200.00;
					}
					else if(billArr[7].equals("Professional Snorkel - 300 Baht")) {
						price = 300.00;
					}
					Ticket ticket = new Ticket(passenger,fInfo,billArr[7],price);
					if(billArr[7].equals("Basic Snorkel - 100 Baht") && passenger.getVoucher().equals("Used")) {
						ticket.setPrice(100.00*0.5);
						passenger.setVoucher("None");
					}
					else if(billArr[7].equals("Average Snorkel - 200 Baht") && passenger.getVoucher().equals("Used")) {
						ticket.setPrice(200.00*0.5);
						passenger.setVoucher("None");
					}
					else if(billArr[7].equals("Professional Snorkel - 300 Baht") && passenger.getVoucher().equals("Used")) {
						ticket.setPrice(300.00*0.5);
						passenger.setVoucher("None");
					}
					Bill bill = new Bill(passenger,ticket);
					passenger.addBill(bill);
				}
			}
			scanner.close();

		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void writeBill() {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter("D://BillList.txt"));
			for(int i = 0; i < passengerList.size(); i++) {
				for(int j = 0; j < passengerList.get(i).billList.size(); j++) {
					output.write(passengerList.get(i).billList.get(j).toString());
					output.newLine();
				}
			}
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readAccountInfo() {
		File file = new File("D://AccountInfo.txt");
		if(!file.exists()) {
			try {
				file.createNewFile();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			Scanner scanner = new Scanner(file);
			while(scanner.hasNext()) {
				String [] info = scanner.nextLine().split(",");
				this.revenue = Double.parseDouble(info[0]);
				this.expenses = Double.parseDouble(info[1]);
			}
			scanner.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void writeAccountInfo() {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter("D://AccountInfo.txt"));
			String AccInfo = this.getRevenue() + "," + this.getExpenses();
			output.write(AccInfo);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void calculateDiff() { // Calculate remaining seats = summation of numSeats - summation of numBookings (for each flight)
		int sumSeats = 0;
		int sumBookings = 0;
		for(int i = 0; i < 15; i++) {
			for(int j = 0; j < 3; j++) {
				sumSeats += flightList[i].getNumSeats()[j];
				sumBookings += flightList[i].getNumBookings()[j];
			}
			seatList[i] = sumSeats - sumBookings; // Calculate then contain in the seatList
			sumSeats = 0; // Reset Values
			sumBookings = 0;

		}
	}

	public void setChoice(String newChoice) {
		this.choice = newChoice;
	}

	public static ArrayList<Customer> getPassengerList() {
		return passengerList;
	}

	public static double getRevenue() {
		return  revenue;
	}
	public double getExpenses() {
		return expenses;
	}
	public  void setRevenue(double revenue) {
		this.revenue = revenue;
	}
	public void setExpenses(double expenses) {
		this.expenses = expenses;
	}
	public double getProfit(){ 
		return revenue-expenses;
	}
	public Flight[] getFlightList() {
		return flightList;
	}
}
