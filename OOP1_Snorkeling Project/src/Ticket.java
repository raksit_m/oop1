public class Ticket {
	private Customer passenger;
	private FlightInfo flightInfo;
	private double price;
	private String paymentStatus;
	private String travelClass;
	
	public Ticket (Customer passenger, FlightInfo flightInfo, String travelClass, double price) {
		this.passenger = passenger;
		this.flightInfo = flightInfo;
		this.travelClass = travelClass;
		this.price = price;
		setPaymentStatus("Charged");
		if(passenger.getVoucher().equals("Used") || passenger.getVoucher().equals("None")) {
		applyDiscount();
		}
	}
	public void applyDiscount () {
		passenger.setDiscountRate(passenger.getMemberClass());
		this.price -= this.price*passenger.getDiscountRate();
	}
	
	public Customer getPassenger() {
		return passenger;
	}
	public FlightInfo getFlightInfo() {
		return flightInfo;
	}
	public double getPrice() {
		return price;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public String getTravelClass() {
		return travelClass;
	}
	public void setPrice(double newPrice) {
		this.price = newPrice;
	}

	public void setPaymentStatus (String newStatus) {
		this.paymentStatus = newStatus;
	}
	public boolean equals(Ticket key) {
		boolean isEquals = false;
		if(this.passenger.equals(key.passenger) && this.flightInfo.equals(key.flightInfo)) {
			isEquals = true;
		}
		return isEquals;
	}
	public String toString () {
		return String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%s,%s",passenger.getName(),passenger.getLastname(),passenger.getGender(),passenger.getMemberClass(),flightInfo.getInfo(),flightInfo.getFromTime().toString(),flightInfo.getToTime().toString(),flightInfo.getFromCity(),flightInfo.getToCity(),getTravelClass(),getPrice(),getPaymentStatus(),passenger.getVoucher());
	}
	public String toShowUp() {
		String [] product = getTravelClass().split("-");
		return String.format("%s , %s , %s , %s , %s , %.2f , %s",passenger.getName(),passenger.getLastname(),passenger.getGender(),passenger.getMemberClass(),product[0],getPrice(),getPaymentStatus());
	}
}
