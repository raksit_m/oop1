import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Login extends JFrame {

	public static Database data;
	public static StoreAccount storeAccount;
	protected JFrame mainFrame;
	private JButton btnPrint;
	private JTextField tFName;
	private JPasswordField passwordField;

	public Login(Database data, StoreAccount storeAccount) {
		this.data = data;
		this.storeAccount = storeAccount;
		initialize();
	}
	public void initialize() {
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setBounds(100, 100, 600, 450);

		ClassLoader loader = this.getClass().getClassLoader();
		mainFrame.getContentPane().setLayout(null);

		btnPrint = new JButton("Print");
		btnPrint.setEnabled(false);
		btnPrint.setBounds(285, 438, 88, 30);
		mainFrame.getContentPane().add(btnPrint);

		tFName = new JTextField();
		tFName.setBounds(210, 118, 200, 30);
		mainFrame.getContentPane().add(tFName);
		tFName.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(210, 177, 200, 30);
		mainFrame.getContentPane().add(passwordField);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(tFName.getText().equals("admin") && passwordField.getText().equals("0000")) {
					JOptionPane.showMessageDialog(null,"Welcome, Admin");
					new MyAccountMenu(data, storeAccount).mainFrame.setVisible(true);
					mainFrame.setVisible(false);
				}

				else {
					JOptionPane.showMessageDialog(null,"Invalid Username or Password");
				}
				tFName.setText("");
				passwordField.setText("");		
			}
		});

		btnLogin.setBounds(250, 259, 97, 30);
		mainFrame.getContentPane().add(btnLogin);

		JLabel bg1 = new JLabel(new ImageIcon(Login.class.getResource("images/Login.png")));
		bg1.setBounds(0, 0, 600, 450);
		mainFrame.getContentPane().add(bg1);

	}
}
