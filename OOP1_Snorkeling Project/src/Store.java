
public class Store {
	private double revenue;
	private double expense;
	
	public Store () {
		this.revenue = 0;
		this.expense = 0;
	}
	public Store (double revenue, double expense) {
		this.revenue = revenue;
		this.expense = expense;
	}
//	public int booking(Flight flight, Customer passenger, String travelClass) {
//		int booking = flight.booking(passenger,travelClass);
//		return booking;
//	}
	public int cancel(Flight flight, Customer passenger) {
		int cancel = flight.cancel(passenger);
		return cancel;
	}
	public int receivePayment(Flight flight, Customer passenger) {
		int receive = -1;
		int index = flight.searchPassenger(passenger);
		if(index != -1) {
			int ticket = passenger.searchTicket(flight.getFlightInfo());
			if(ticket != -1) {
				Ticket found = passenger.ticketList.get(ticket);
				double price = found.getPrice();
				this.revenue += price;
				found.setPaymentStatus("Paid");
				passenger.addBill(new Bill(passenger,found));
				receive = 1;
			}
		}
		return receive;
	}
	public void clearFlight (Flight flight) {
		this.expense += flight.getFlightExpense();
		flight.clear();
	}
	public double getProfit () {
		return this.revenue - this.expense;
	}
	public double getRevenue() {
		return this.revenue;
	}
	public double getExpense() {
		return this.expense;
	}
	public String toString () {
		return String.format("Revenue: %.2f, Expense: %.2f",this.revenue,this.expense);
	}
}
