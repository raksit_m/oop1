import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;

public class MyAccountMenu extends JFrame {

	public Database data;
	public StoreAccount storeAccount;
	protected JFrame mainFrame;

	public MyAccountMenu(Database data, StoreAccount storeAccount) {
		this.data = data;
		this.storeAccount = storeAccount;
		initialize();
	}
	public void initialize() {
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.setBounds(100, 50, 450, 600);

		ClassLoader loader = this.getClass().getClassLoader();
		URL url1 = loader.getResource("images/AccountInfo.png");
		ImageIcon img1 = new ImageIcon(url1);
		mainFrame.getContentPane().setLayout(null);

		JButton btnAccountInfo = new JButton("Account Info");
		btnAccountInfo.setBounds(144, 171, 143, 35);
		btnAccountInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AccountInfo(storeAccount).mainFrame.setVisible(true);
			}
		}
				);
		mainFrame.getContentPane().add(btnAccountInfo);

		JButton btnNewCustomer = new JButton("New Customer");
		btnNewCustomer.setBounds(144, 243, 143, 35);
		btnNewCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewCustomer newcustomer = new NewCustomer(data,storeAccount);
				newcustomer.mainFrame.setVisible(true);
			}
		}
				);
		mainFrame.getContentPane().add(btnNewCustomer);

		JButton btnSearchCustomer = new JButton("Search Customer");
		btnSearchCustomer.setBounds(144, 388, 143, 35);
		btnSearchCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new SearchCustomer(data, storeAccount).mainFrame.setVisible(true);
			}
		}
				);
		mainFrame.getContentPane().add(btnSearchCustomer);

		JButton btnReceivePayment = new JButton("Receive Payment");
		btnReceivePayment.setBounds(144, 451, 143, 35);
		btnReceivePayment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ReceivePayment(data, storeAccount).mainFrame.setVisible(true);
			}
		}
				);
		mainFrame.getContentPane().add(btnReceivePayment);

		JButton btnViewBill = new JButton("View Bill");
		btnViewBill.setBounds(144, 315, 143, 35);
		btnViewBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ViewBill(storeAccount).mainFrame.setVisible(true);
			}
		}
				);
		mainFrame.getContentPane().add(btnViewBill);

		JLabel bg1 = new JLabel(new ImageIcon(MyAccountMenu.class.getResource("/images/My Account.png")));
		bg1.setBounds(0, 0, 450, 600);
		mainFrame.getContentPane().add(bg1);

	}
}
