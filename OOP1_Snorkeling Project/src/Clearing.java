import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Clearing extends JFrame {

	public static Database data;
	public static SelectFlight select;
	private JPanel contentPane;
	protected JFrame new1;
	private JComboBox comboBox_1;
	private JButton btnClearing;
	private int index;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Clearing(Database data, SelectFlight select, final int index) {
		this.data = data;
		this.select = select;
		this.index = index;
		initialize();
	}
	public void initialize() {
		new1 = new JFrame();
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		new1.setBounds(50, 50, 900, 600);

		ClassLoader loader1 = this.getClass().getClassLoader();
		URL urlC = loader1.getResource("images/Clearing.png");
		ImageIcon img1 = new ImageIcon(urlC);
		new1.getContentPane().setLayout(null);

		comboBox_1 = new JComboBox ();
		comboBox_1.setBounds(354, 173, 320, 30);
		for(int i = 0; i < 3; i++) {
			comboBox_1.addItem(data.flightList[index + i].getFlightInfo().toShowUp());
		}
		new1.getContentPane().add(comboBox_1);

		btnClearing = new JButton("Clear");
		btnClearing.setBounds(377, 350, 100, 30);
		btnClearing.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int search = 1;
				boolean check = false;
				Customer passenger = null;
				String output = "Trip Cleared\n";
				for(int i = 0; i < data.passengerList.size(); i++) {
					for(int j = 0; j < data.passengerList.get(i).ticketList.size(); j++) {	
						if(data.passengerList.get(i).ticketList.get(j).getFlightInfo().equals(data.flightList[comboBox_1.getSelectedIndex() + index].getFlightInfo())) {
							 output += data.passengerList.get(i).ticketList.get(j).toShowUp() + "\n";
							if(data.passengerList.get(i).ticketList.get(j).getTravelClass().equals("Basic Snorkel - 100 Baht")) {
								data.passengerList.get(i).ticketList.remove(j);	
								data.flightList[comboBox_1.getSelectedIndex() + index].getNumBookings()[0]--;
								data.numBookingsList.get(comboBox_1.getSelectedIndex())[0]--;
							}
							else if(data.passengerList.get(i).ticketList.get(j).getTravelClass().equals("Average Snorkel - 200 Baht")) {

								data.passengerList.get(i).ticketList.remove(j);
								data.flightList[comboBox_1.getSelectedIndex() + index].getNumBookings()[1]--;
								data.numBookingsList.get(comboBox_1.getSelectedIndex())[1]--;
							}
							else if(data.passengerList.get(i).ticketList.get(j).getTravelClass().equals("Professional Snorkel - 300 Baht")) {
								data.passengerList.get(i).ticketList.remove(j);
								data.flightList[comboBox_1.getSelectedIndex() + index].getNumBookings()[2]--;
								data.numBookingsList.get(comboBox_1.getSelectedIndex())[2]--;
							}
							passenger = data.getPassengerList().get(i);

							i--;
							check = true;
							break;
						}
					}
					if(check)
						data.writeTicketList(passenger);
				}
				
				data.writeBookings();
				
				data.calculateDiff();
				SelectFlight.updateLabel();
				SnorkelLabel.updateLabel();
				select.updateThumbnail();
				JOptionPane.showMessageDialog(null,output);
			}
		});
		new1.getContentPane().add(btnClearing);

		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 900, 600);
		new1.getContentPane().add(bg1);
	}
}
