import java.awt.EventQueue;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;


public class SelectFlight {

	protected static Database data;
	protected static JFrame new1;
	static JLabel lbl1 = new JLabel();
	static JLabel lbl2 = new JLabel();
	static JLabel lbl3 = new JLabel();

	JLabel btn1;
	JLabel btn2;
	JLabel btn3;

	ClassLoader loader;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public SelectFlight(Database data) {
		this.data = data;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		new1 = new JFrame();
		new1.setBounds(100, 100, 600, 300);
		new1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		loader = this.getClass().getClassLoader();
		URL urlC = loader.getResource("images/Select.png");
		URL url2 = loader.getResource("images/Thumbnail_1.png");
		URL url3 = loader.getResource("images/Thumbnail_2.png");
		URL url4 = loader.getResource("images/Thumbnail_3.png");
		ImageIcon img1 = new ImageIcon(urlC);
		ImageIcon thumb1 = new ImageIcon(url2);
		ImageIcon thumb2 = new ImageIcon(url3);
		ImageIcon thumb3 = new ImageIcon(url4);
		new1.getContentPane().setLayout(null);


		lbl1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbl1.setForeground(new Color(255, 255, 153));
		lbl1.setText(Integer.toString(data.seatList[data.indexSelect]) + " Seats Left");
		lbl1.setBounds(58, 226, 121, 14);

		lbl2.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbl2.setForeground(new Color(255, 255, 153));
		lbl2.setText(Integer.toString(data.seatList[data.indexSelect+1]) + " Seats Left");
		lbl2.setBounds(242, 226, 95, 14);

		lbl3.setFont(new Font("Tahoma", Font.BOLD, 13));
		lbl3.setForeground(new Color(255, 255, 153));
		lbl3.setText(Integer.toString(data.seatList[data.indexSelect+2]) + " Seats Left");
		lbl3.setBounds(430, 226, 81, 14);

		new1.getContentPane().add(lbl1);
		new1.getContentPane().add(lbl2);
		new1.getContentPane().add(lbl3);

		btn1 = new JLabel(thumb1);
		btn1.setBounds(25,58,150,150);
		btn1.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				if(data.choice.equals("Booking")) {
					if(data.seatList[data.indexSelect] != 0) {
						Booking book = new Booking(data,getThis(),data.indexSelect);
						book.new1.setVisible(true);
					}
					else {
						JOptionPane.showMessageDialog(null, "No Available Seats !!");
					}
				}
			}
			public void mouseEntered(MouseEvent e) {
				if(data.status[data.indexSelect] == 0) {
					btn1.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_1_Glow.png")));
				}
				else {
					btn1.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_1_Full_Glow.png")));
				}
			}
			public void mouseExited(MouseEvent e) {
				if(data.status[data.indexSelect] == 0) {
					btn1.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_1.png")));
				}
				else {
					btn1.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_1_Full.png")));
				}
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		new1.getContentPane().add(btn1);

		btn2 = new JLabel(thumb2);
		btn2.setBounds(208,57,150,150);
		btn2.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				if(data.choice.equals("Booking")) {
					if(data.seatList[data.indexSelect+1] != 0) {
						Booking book = new Booking(data,getThis(),data.indexSelect+1);
						book.new1.setVisible(true);
					}
					else {
						JOptionPane.showMessageDialog(null, "No Available Seats !!");
					}
				}
			}
			public void mouseEntered(MouseEvent e) {
				if(data.status[data.indexSelect+1] == 0) {
					btn2.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_2_Glow.png")));
				}
				else {
					btn2.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_2_Full_Glow.png")));
				}
			}
			public void mouseExited(MouseEvent e) {
				if(data.status[data.indexSelect+1] == 0) {
					btn2.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_2.png")));
				}
				else {
					btn2.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_2_Full.png")));
				}
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		new1.getContentPane().add(btn2);


		btn3 = new JLabel(thumb3);
		btn3.setBounds(394,56,150,150);
		btn3.addMouseListener(new MouseListener() {
			public void mouseClicked (MouseEvent e) {
				if(data.choice.equals("Booking")) {
					if(data.seatList[data.indexSelect+2] != 0) {
						Booking book = new Booking(data,getThis(),data.indexSelect+2);
						book.new1.setVisible(true);
					}
					else {
						JOptionPane.showMessageDialog(null, "No Available Seats !!");
					}
				}
			}
			public void mouseEntered(MouseEvent e) {	
				if(data.status[data.indexSelect+2] == 0) {
					btn3.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_3_Glow.png")));
				}
				else {
					btn3.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_3_Full_Glow.png")));
				}
			}
			public void mouseExited(MouseEvent e) {
				if(data.status[data.indexSelect+2] == 0) {
					btn3.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_3.png")));
				}
				else {
					btn3.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_3_Full.png")));
				}
			}
			public void mousePressed(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
		new1.getContentPane().add(btn3);	

		updateLabel();
		updateThumbnail();

		JLabel bg1 = new JLabel(img1);
		bg1.setBounds(0, 0, 600, 300);
		new1.getContentPane().add(bg1);
	}
	public static void  updateLabel() {
		lbl1.setText(String.format("%d Seats Left",data.seatList[data.indexSelect]));
		lbl2.setText(String.format("%d Seats Left",data.seatList[data.indexSelect+1]));
		lbl3.setText(String.format("%d Seats Left",data.seatList[data.indexSelect+2]));
		if(data.seatList[data.indexSelect] == 0) {
			lbl1.setText("       FULL");
		}
		if(data.seatList[data.indexSelect+1] == 0) {
			lbl2.setText("       FULL");
		}
		if(data.seatList[data.indexSelect+2] == 0) {
			lbl3.setText("       FULL");
		}
	}
	public SelectFlight getThis() {
		return this;
	}
	public void updateThumbnail() {
		if(data.seatList[data.indexSelect] == 0) {
			btn1.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_1_Full.png")));
			data.status[data.indexSelect] = 1;
		}
		if(data.seatList[data.indexSelect] != 0) {
			btn1.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_1.png")));
			data.status[data.indexSelect] = 0;
		}

		if(data.seatList[data.indexSelect+1] == 0) {
			btn2.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_2_Full.png")));
			data.status[data.indexSelect+1] = 1;
		}

		if(data.seatList[data.indexSelect+1] != 0) {
			btn2.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_2.png")));
			data.status[data.indexSelect+1] = 0;
		}
		if(data.seatList[data.indexSelect+2] == 0) {
			btn3.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_3_Full.png")));
			data.status[data.indexSelect+2] = 1;
		}
		if(data.seatList[data.indexSelect+2] != 0) {
			btn3.setIcon(new ImageIcon(loader.getResource("images/Thumbnail_3.png")));
			data.status[data.indexSelect+2] = 0;
		}
	}
}
