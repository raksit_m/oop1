import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class StoreAccount {

	public static Database data;
	public static SelectFlight select;

	StoreAccount(Database data){
		this.data = data;
		select = new SelectFlight(data);
	}
	//----------------------------------------------------
	// View Bill Methods
	public String viewBill (String name, String lastname){
		String output = "Bill Found\n";
		int index = -1;
		
		for (int i = 0; i < data.passengerList.size(); i++) {
			if(data.passengerList.get(i).equals(name,lastname)) {
				index = i;
				break;
			}
		}
		if(data.passengerList.get(index).billList.isEmpty()) {
			index = -1;
		}
		if(index != -1) {
				for(int j = 0; j < data.passengerList.get(index).billList.size(); j++) {
					output += data.passengerList.get(index).billList.get(j).toString() + "\n";
				}	
		}
		if(index == -1) {
			output = "Passenger / Bill Not Found";
		}
		return output;
	}
	
	public String viewBill(String name, String lastname, int billID) {
		String output = "Bill Found\n";
		int index = -1;
		
		for (int i = 0; i < data.passengerList.size(); i++) {
			if(data.passengerList.get(i).equals(name,lastname)) {
				index = i;
				break;
			}
		}
		if(data.passengerList.get(index).billList.size() == 0) {
			index = -1;
		}
		if(billID >= 1 && billID <= data.passengerList.get(index).billList.size() && index != -1) {
			output += data.passengerList.get(index).billList.get(billID-1).toString() + "\n";
		}
		if(index == -1) {
			output = "Passenger / Bill Not Found";
		}
		return output;
	}
	
	public String viewBill(int billID) {
		for(int i = 0; i < data.passengerList.size(); i++) {
			for(int j = 0; j < data.passengerList.get(i).billList.size(); j++) {
				if(data.passengerList.get(i).billList.get(j).getBillID() == billID) {
					return "Bill Found\n" + data.passengerList.get(i).billList.get(j).toShowUp();
				}
			}
		}
		return "Bill Not Found";
	}
	
	public void printBill (String Billtext ){
		File outfile = new File ("output.txt");
		try {
			FileWriter fw = new FileWriter(outfile);
			PrintWriter pw = new PrintWriter(fw);
			pw.print(Billtext);
			pw.close();
			fw.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	//---------------------------------------------------
	// Search Customer
	public static int searchCustomer(String name, String lastName){
		int index = -1;
		for (int i = 0; i < data.passengerList.size(); i++) {
			if(data.passengerList.get(i).equals(name,lastName)) {
				index = i;
				break;
			}
		}
		return index;

	}
	//----------------------------------------------------------------
	//New Customer
	public void addCustomer(String name, String lastname, String gender, String memberClass, String voucher){
		data.passengerList.add(new Customer(name, lastname, gender, memberClass, voucher));
	}
	public void addCustomer(Customer passenger){
		data.passengerList.add(passenger);
	}
	
	public void exportCustomers(){
		File outfile = new File ("output.txt");
		try {
			FileWriter fw = new FileWriter(outfile);
			PrintWriter pw = new PrintWriter(fw);
			String customers="";
			for (int i = 0; i < data.getPassengerList().size(); i++) {
				customers += data.getPassengerList().get(i).toString() +"\n";			
			}
			pw.print(customers);
			pw.close();
			fw.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	} 
	//-----------------------------------------------------------------
	//receive Payment
	public String receivePayment(int flightnumber, String name ,String lastName ) {
		String output = "Payment Received :\n";
		int i = searchCustomer(name, lastName);
		int count = 0;
		if(i != -1) {
			for(int j = 0; j < data.passengerList.get(i).ticketList.size(); j++) {
				if(data.passengerList.get(i).ticketList.get(j).getFlightInfo().equals(data.flightList[flightnumber].getFlightInfo())){
					data.setRevenue(data.revenue + data.passengerList.get(i).ticketList.get(j).getPrice());
					data.passengerList.get(i).ticketList.get(j).setPaymentStatus("Paid");
					data.passengerList.get(i).billList.add(new Bill(data.passengerList.get(i),data.passengerList.get(i).ticketList.get(j)));
					data.writeBill();
					output += data.passengerList.get(i).ticketList.get(j).toShowUp() + "\n";
					count++;
				}
			}
			for(int j = 0; j < data.passengerList.get(i).ticketList.size(); j++) {
				if(data.passengerList.get(i).ticketList.get(j).getFlightInfo().equals(data.flightList[flightnumber].getFlightInfo())){
					if(data.passengerList.get(i).ticketList.get(0).getTravelClass().equals("Basic Snorkel - 100 Baht")) {
						StoreAccount.this.data.flightList[flightnumber].getNumBookings()[0]--;
						StoreAccount.this.data.numBookingsList.get(flightnumber)[0]--;
					}
					else if(data.passengerList.get(i).ticketList.get(0).getTravelClass().equals("Average Snorkel - 200 Baht")) {
						StoreAccount.this.data.flightList[flightnumber].getNumBookings()[1]--;
						StoreAccount.this.data.numBookingsList.get(flightnumber)[1]--;
					}
					else if(data.passengerList.get(i).ticketList.get(0).getTravelClass().equals("Professional Snorkel - 300 Baht")) {
						StoreAccount.this.data.flightList[flightnumber].getNumBookings()[2]--;
						StoreAccount.this.data.numBookingsList.get(flightnumber)[2]--;
					}
					data.passengerList.get(i).ticketList.remove(j);
					j = -1;
				}
				data.writeTicketList(data.passengerList.get(i));
			}
			
			data.writeBookings();
			
			StoreAccount.this.data.calculateDiff();
			StoreAccount.this.select.updateLabel();
			StoreAccount.this.select.updateThumbnail();
			
		}
		else if(i == -1) {
			output = "Payment Unsuccessful";
		}
		return output;
	}

	//data.getFlightList()[flightnumber].searchPassenger()

	//-----------------------------------------------------------------
	public String toString(){
		return String.format("Revenue: %.2f, Expense: %.2f",data.getRevenue(),data.getExpenses());
	}
	public StoreAccount getStoreAccount() {
		return this;
	}
}






